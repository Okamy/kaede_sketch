/***************************************************************************
 PURPOSE
     RX64M Library for Arduino compatible framework

 TARGET DEVICE
     RX64M

 AUTHOR
     Renesas Solutions Corp.

 ***************************************************************************
 Copyright (C) 2014 Renesas Electronics. All rights reserved.

 This library is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option) any
 later version.

 See file LICENSE.txt for further informations on licensing terms.

****************************************************************************/
#ifndef _IODEFINE_GCC64M_H_
#define _IODEFINE_GCC64M_H_

#include "rx64m/iodefine.h"

#endif/*_IODEFINE_GCC64M_H_*/
