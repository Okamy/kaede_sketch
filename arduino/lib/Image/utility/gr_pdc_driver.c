/*
 * gr_pdc_driver.c
 *
 *  Created on: 2015/08/21
 *      Author: a5034000
 */


#include "gr_pdc_driver.h"
#include "gr_dma_driver.h"
#include "rx64m/util.h"
#include "rx64m/interrupt_handlers.h"

/* This array holds callback functions. */
//typedef void (* pcdfi_callback_t)(void *pdata);

/***********************************************************************************************************************
Private variables and functions
***********************************************************************************************************************/
static bool is_opened = false;      /* indicate whether PDC is opened */
static bool is_initialized = false; /* indicate whether PDC is initialized */

/**
 * Private global function prototypes
 */
static void         callback_receive_data_ready(void);
static void         callback_frame_end(void);
static void         callback_error(void);
static void         error_processing(void);
static void         overrun_error(void);
static void         underrun_error(void);
static void         vertical_line_setting_error(void);
static void         horizontal_byte_setting_error(void);

static void         pdc_set_ports(void);
static pdc_return_t pdc_initialize(pdc_data_cfg_t *p_data_cfg);
static void         pdc_capture_start(void);
static pdc_return_t pdc_change_position_size(pdc_data_cfg_t *p_data_cfg);
static pdc_return_t pdc_int_setting(pdc_step_t step, pdc_data_cfg_t *p_data_cfg);
static pdc_return_t pdc_int_setting_sub(pdc_data_cfg_t *p_data_cfg);
static pdc_return_t pdc_clear_status(pdc_stat_t *p_stat);
static pdc_return_t pdc_get_status(pdc_stat_t *p_stat);

/***********************************************************************************************************************
* Function Name: R_PDC_Open
* Description  : This function is used first to prepare the PDC for use.
*                It makes initial settings to the PDC�fs registers as the first step in initializing the PDC.
* Arguments    : p_data_cfg -
*                    Pointer to PDC settings data structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_OPENED -
*                    R_PDC_Open has already been run.
*                PDC_ERR_INVALID_ARG -
*                    Parameter values in PDC setting information are invalid.
*                PDC_ERR_NULL_PTR -
*                    Argument p_data_cfg is a NULL pointer.
*                PDC_ERR_BUSY -
*                    The PDC has already been locked by another process.
*                PDC_ERR_INTERNAL -
*                    A module internal error was detected.
***********************************************************************************************************************/
pdc_return_t R_PDC_Open(pdc_data_cfg_t *p_data_cfg)
{
    pdc_return_t ret = PDC_SUCCESS;

    /* Used for constructing value to write to PCCR0 register. */
    uint8_t pckdiv_value = 0;

    /* Check that the R_PDC_Open has been executed. */
    if (true == is_opened)
    {
        return PDC_ERR_OPENED;
    }

    /* Make sure that the argument is correct. */
    /* To cast in order to compare the address. There is no problem because the information is not lost even if the
     *  cast. */
    if ((uint32_t)NULL == (uint32_t)p_data_cfg)
    {
        return PDC_ERR_NULL_PTR;
    }

    /* Cancels PDC module stop state. */
    startModule(MstpIdPDC);
    //pdc_module_enable();

    /* Makes initial settings to I/O ports used by the PDC. */
    pdc_set_ports();

    /* Makes settings for interrupts used by the PDC. */
    ret = pdc_int_setting(PDC_INITIALIZE1, p_data_cfg);
    if (PDC_SUCCESS != ret)
    {
        return ret;
    }

    /* Stops PDC receive operation. */
    PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;

    /* Specifies the clock for parallel data transfer clock output (PCKO). */
#if PDC_CFG_PCKO_DIV == 2
    /* Do nothing since PCKDIV bits should be 0. */
#elif PDC_CFG_PCKO_DIV == 4
    pckdiv_value = 0x01;
#elif PDC_CFG_PCKO_DIV == 6
    pckdiv_value = 0x02;
#elif PDC_CFG_PCKO_DIV == 8
    pckdiv_value = 0x03;
#elif PDC_CFG_PCKO_DIV == 10
    pckdiv_value = 0x04;
#elif PDC_CFG_PCKO_DIV == 12
    pckdiv_value = 0x05;
#elif PDC_CFG_PCKO_DIV == 14
    pckdiv_value = 0x06;
#elif PDC_CFG_PCKO_DIV == 16
    pckdiv_value = 0x07;
#else
    #error "Error! Invalid setting for PDC_CFG_PCKO_DIV in r_pdc_rx_config.h"
#endif
    PDC.PCCR0.BIT.PCKDIV = pckdiv_value;

    /* Starts supply of parallel data transfer clock output (PCKO). */
    PDC.PCCR0.BIT.PCKOE = PDC_ENABLE_PCKO_OUTPUT;

    /* PDC module is initialized successfully in R_PDC_Open. */
    is_opened = true;

    return PDC_SUCCESS;
} /* End of function R_PDC_Open() */

/***********************************************************************************************************************
* Function Name: R_PDC_Close
* Description  : Ends operation by the PDC and puts it into the module stop state.
* Arguments    : none
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_NOT_OPEN -
*                    R_PDC_Open has not been run.
***********************************************************************************************************************/
pdc_return_t R_PDC_Close(void)
{
    /* Check that the R_PDC_Open has been executed. */
    if (false == is_opened)
    {
        return PDC_ERR_NOT_OPEN;
    }

    /* Disables interrupts (PCFEI, PCERI, and PCDFI) used by the PDC. */
    IEN(PDC, PCDFI) = 0;
    PDC.PCCR0.BIT.DFIE = 0;
    while (0 != PDC.PCCR0.BIT.DFIE)
    {
        /* Do Nothing */
    }
    IR(PDC, PCDFI) = 0;        /* Interrupt request is cleared. */
    ICU.GENBL0.BIT.EN30 = 0;   /* Interrupt request(GRPBL0.IS30) is also cleared.  */
    ICU.GENBL0.BIT.EN31 = 0;   /* Interrupt request(GRPBL0.IS31) is also cleared.  */

    /* Disables PDC operation.
       The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
    PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;

    /* Stops supply of parallel data transfer clock output (PCKO). */
    PDC.PCCR0.BIT.PCKOE = PDC_DISABLE_PCKO_OUTPUT;

    /* Disables pixel clock input from the image sensor. */
    PDC.PCCR0.BIT.PCKE = PDC_DISABLE_PIXCLK_INPUT;

    /* Stops PDC module. */
    stopModule(MstpIdPDC);
//    pdc_module_disable();

    /* PDC module is not initialized yet */
    is_opened = false;
    is_initialized = false;

    return PDC_SUCCESS;
} /* End of function R_PDC_Close() */

/***********************************************************************************************************************
* Function Name: R_PDC_Create
* Description  : This function makes settings to the PDC registers.
*                It makes initial settings to the PDC�fs registers as the second step in initializing the PDC.
* Arguments    : p_data_cfg -
*                    Pointer to PDC settings data structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_NOT_OPEN -
*                    R_PDC_Open has not been run.
*                PDC_ERR_INVALID_ARG -
*                    Parameter values in PDC setting information are invalid.
*                PDC_ERR_NULL_PTR -
*                    Argument p_data_cfg is a NULL pointer.
***********************************************************************************************************************/
pdc_return_t R_PDC_Create(pdc_data_cfg_t *p_data_cfg)
{
    pdc_return_t      ret = PDC_SUCCESS;

    /* Check that the R_PDC_Open has been executed. */
    if (false == is_opened)
    {
        return PDC_ERR_NOT_OPEN;
    }

    /* Make sure that the argument is correct. */
    /* To cast in order to compare the address. There is no problem because the information is not lost even if the
     *  cast. */
    if ((uint32_t)NULL == (uint32_t)p_data_cfg)
    {
        return PDC_ERR_NULL_PTR;
    }

    /* References the PDC setting information structure specified by the argument and makes initial settings for using
       the PDC. */
    ret = pdc_initialize(p_data_cfg);

    /* PDC module is initialized successfully in R_PDC_Create.  */
    is_initialized = true;

    return ret;
} /* End of function R_PDC_Create() */

/***********************************************************************************************************************
* Function Name: R_PDC_Control
* Description  : This function performs processing according to control codes.
* Arguments    : command -
*                    Control code
*                p_data_cfg -
*                    Pointer to PDC settings data structure
*                p_stat -
*                    Pointer to PDC status structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_NOT_OPEN -
*                    R_PDC_Open has not been run.
*                PDC_ERR_NOT_INITIALIZED -
*                    R_PDC_Create has not been run.
*                PDC_ERR_INVALID_COMMAND -
*                    The argument command is invalid.
*                PDC_ERR_NULL_PTR -
*                    The argument p_data_cfg or p_stat is a NULL pointer.
***********************************************************************************************************************/
pdc_return_t R_PDC_Control( pdc_command_t      command,
                            pdc_data_cfg_t *   p_data_cfg,
                            pdc_stat_t *       p_stat)
{
    pdc_return_t      ret = PDC_SUCCESS;

    /* Check that the R_PDC_Open has been executed. */
    if (false == is_opened)
    {
        return PDC_ERR_NOT_OPEN;
    }

    /* Check that the R_PDC_Create has been executed. */
    if (false == is_initialized)
    {
        return PDC_ERR_NOT_INITIALIZED;
    }

    switch (command)
    {
        case PDC_CMD_CAPTURE_START:
            pdc_capture_start();
            break;

        case PDC_CMD_CHANGE_POS_AND_SIZE:
            ret = pdc_change_position_size(p_data_cfg);
            break;

        case PDC_CMD_STATUS_GET:
            ret = pdc_get_status(p_stat);
            break;

        case PDC_CMD_STATUS_CLR:
            ret = pdc_clear_status(p_stat);
            break;

        case PDC_CMD_SET_INTERRUPT:
            ret = pdc_int_setting(PDC_CHANGE_SETTING, p_data_cfg);
            break;

        case PDC_CMD_DISABLE:
            PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
            break;

        case PDC_CMD_ENABLE:
            PDC.PCCR1.BIT.PCE = PDC_ENABLE_OPERATION;
            break;

        case PDC_CMD_RESET:

            /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
            PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
            PDC.PCCR0.BIT.PRST = PDC_RESET;
            while( PDC_RESET_RELEASE != PDC.PCCR0.BIT.PRST )
            {
                /* Do Nothing */
            }
            break;

        /* Commands not supported */
        default:
            ret = PDC_ERR_INVALID_COMMAND;
            break;
    }
    return ret;
} /* End of function R_PDC_Control() */

/***********************************************************************************************************************
* Function Name: R_PDC_GetFifoAddr
* Description  : This function gets the FIFO address of the PDC.
* Arguments    : none
* Return Values: Address of FIFO (PDC receive data register)
***********************************************************************************************************************/
void * R_PDC_GetFifoAddr(void)
{
    return ((void *)&PDC.PCDR.LONG);
} /* End of function R_PDC_GetFifoAddr() */


/**
 * Private functions
 */
/***********************************************************************************************************************
* Function Name: callback_receive_data_ready
* Description  : Callback function of the received data ready interrupt.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void callback_receive_data_ready(void)
{
    /* User needs to prepare processing for each, if your system need.
       Please edit such to call user function. */
    /* Call user function. */
} /* End of function callback_receive_data_ready() */

/***********************************************************************************************************************
* Function Name: callback_frame_end
* Description  : Callback function of the frame end interrupt.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void callback_frame_end(void)
{
    volatile pdc_return_t       ret_pdc;
    pdc_data_cfg_t              dummy_data;
    pdc_stat_t                  stat_pdc;

    /* Check FIFO empty and UDRF flag. */
    do
    {
        ret_pdc = R_PDC_Control(PDC_CMD_STATUS_GET, &dummy_data, &stat_pdc);
        if (PDC_SUCCESS != ret_pdc)
        {
            /* do something */
        }
        if (true == stat_pdc.pcsr_stat.underrun)
        {
            /* Clear the PCSR.FEF flag to 0. */
            stat_pdc.pcsr_stat.fifo_empty = true;
            ret_pdc = R_PDC_Control(PDC_CMD_STATUS_CLR, &dummy_data, &stat_pdc);
            if (PDC_SUCCESS != ret_pdc)
            {
                /* do something */
            }
            error_processing();
            return;
        }
    }
    while (true != stat_pdc.pcsr_stat.fifo_empty);

    /* After completion of the transfer, clear the PCE bit in the PCCR1 register to 0. */
    ret_pdc = R_PDC_Control(PDC_CMD_DISABLE, &dummy_data, &stat_pdc);
    if (PDC_SUCCESS != ret_pdc)
    {
        /* do something */
    }

    /* Clear the FEF flag in the PCSR register to 0. */
    stat_pdc.pcsr_stat.fifo_empty = true;
    ret_pdc = R_PDC_Control(PDC_CMD_STATUS_CLR, &dummy_data, &stat_pdc);
    if (PDC_SUCCESS != ret_pdc)
    {
        /* do something */
    }

    /* User needs to prepare processing for each, if your system need.
       Please edit such to call user function. */
    /* Call user function. */
#if 0
    ret_dmac = R_DMACA_Control(DMACA_CH0, DMACA_CMD_DISABLE, &p_stat_dmac);
    if (DMACA_SUCCESS != ret_dmac)
    {
        /* do something */
    }
#endif


} /* End of function callback_frame_end() */

/***********************************************************************************************************************
* Function Name: callback_error
* Description  : Callback function of the error interrupt.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void callback_error(void)
{
    /* call function of error processing. */
    error_processing();
} /* End of function callback_error() */

/***********************************************************************************************************************
* Function Name: error_processing
* Description  : error processing.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void error_processing(void)
{
    volatile pdc_return_t       ret_pdc;
    pdc_data_cfg_t              dummy_data;
    pdc_stat_t                  stat_pdc;

    /* Disabling of PDC operation. */
    ret_pdc = R_PDC_Control(PDC_CMD_DISABLE, &dummy_data, &stat_pdc);
    if (PDC_SUCCESS != ret_pdc)
    {
        /* do something */
    }

    /* Disable the DTC or DMAC transfer.Please edit such to call user function. */
    /* Call User Function */

    /* Read the PCSR register. */
    ret_pdc = R_PDC_Control(PDC_CMD_STATUS_GET, &dummy_data, &stat_pdc);
    if (PDC_SUCCESS != ret_pdc)
    {
        /* do something */
    }

    /* Checking the overrun flag.(PCSR.OVRF) */
    if (true == stat_pdc.pcsr_stat.overrun)
    {
        /* call Overrun error processing. */
        overrun_error();

        /* Clear the PCSR.OVRF flag to 0. */
        stat_pdc.pcsr_stat.overrun = true;
        ret_pdc = R_PDC_Control(PDC_CMD_STATUS_CLR, &dummy_data, &stat_pdc);
        if (PDC_SUCCESS != ret_pdc)
        {
            /* do something */
        }
    }

    /* Checking the underrun flag.(PCSR.UDRF) */
    if (true == stat_pdc.pcsr_stat.underrun)
    {
        /* call Underrun error processing. */
        underrun_error();

        /* Clear the PCSR.UDRF flag to 0. */
        stat_pdc.pcsr_stat.underrun = true;
        ret_pdc = R_PDC_Control(PDC_CMD_STATUS_CLR, &dummy_data, &stat_pdc);
        if (PDC_SUCCESS != ret_pdc)
        {
            /* do something */
        }
    }

    /* Checking the verticcal line setting error flag.(PCSR.VERF) */
    if (true == stat_pdc.pcsr_stat.verf_error)
    {
        /* call Vertical line setting error processing. */
        vertical_line_setting_error();

        /* Clear the PCSR.VERF flag to 0. */
        stat_pdc.pcsr_stat.verf_error = true;
        ret_pdc = R_PDC_Control(PDC_CMD_STATUS_CLR, &dummy_data, &stat_pdc);
        if (PDC_SUCCESS != ret_pdc)
        {
            /* do something */
        }
    }

    /* Checking the horizontal byte setting error flag.(PCSR.HERF) */
    if (true == stat_pdc.pcsr_stat.herf_error)
    {
        /* call Horizontal byte setting error processing. */
        horizontal_byte_setting_error();

        /* Clear the PCSR.HERF flag to 0. */
        stat_pdc.pcsr_stat.herf_error = true;
        ret_pdc = R_PDC_Control(PDC_CMD_STATUS_CLR, &dummy_data, &stat_pdc);
        if (PDC_SUCCESS != ret_pdc)
        {
            /* do something */
        }
    }

    /* User needs to prepare processing for each, if your system need.
       Please edit such to call user function. */
    /* Call user function. */
} /* End of function error_processing() */

/***********************************************************************************************************************
* Function Name: overrun_error
* Description  : Overrun error processing.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void overrun_error(void)
{
    /* User needs to prepare processing for each, if your system need.
       Please edit such to call user function. */
    /* Call user function. */
} /* End of function overrun_error() */

/***********************************************************************************************************************
* Function Name: underrun_error
* Description  : Underrun error processing.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void underrun_error(void)
{
    /* User needs to prepare processing for each, if your system need.
       Please edit such to call user function. */
    /* Call user function. */
} /* End of function underrun_error() */

/***********************************************************************************************************************
* Function Name: vertical_line_setting_error
* Description  : Vertical line setting error processing.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void vertical_line_setting_error(void)
{
    /* User needs to prepare processing for each, if your system need.
       Please edit such to call user function. */
    /* Call user function. */
} /* End of function vertical_line_setting_error() */

/***********************************************************************************************************************
* Function Name: horizontal_byte_setting_error
* Description  : Horizontal byte setting error processing.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void horizontal_byte_setting_error(void)
{
    /* User needs to prepare processing for each, if your system need.
       Please edit such to call user function. */
    /* Call user function. */
} /* End of function horizontal_byte_setting_error() */


/***********************************************************************************************************************
* Function Name: pdc_set_ports
* Description  : Makes initial settings to I/O ports used by the PDC.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void pdc_set_ports(void)
{
    MPC.PWPR.BYTE = 0x00;
    MPC.PWPR.BYTE = 0x40;

    /* P15 PIXD0 / P17 PIXD3 */
    PORT1.PMR.BYTE |= 0xAC;
    MPC.P15PFS.BIT.PSEL = 0x1C;
    MPC.P17PFS.BIT.PSEL = 0x1C;

    /* P20 PIXD4 / P21 PIXD5 / P22 PIXD6 / P23 PIXD7
     / P24 PIXCLK / P25 HSYNC */
    PORT2.PMR.BYTE |= 0x3F;
    MPC.P20PFS.BIT.PSEL = 0x1C;
    MPC.P21PFS.BIT.PSEL = 0x1C;
    MPC.P22PFS.BIT.PSEL = 0x1C;
    MPC.P23PFS.BIT.PSEL = 0x1C;
    MPC.P24PFS.BIT.PSEL = 0x1C;
    MPC.P25PFS.BIT.PSEL = 0x1C;

    /* P86 PIXD1 / P87 PIXD2 */
    PORT8.PMR.BYTE |= 0xC0;
    MPC.P86PFS.BIT.PSEL = 0x1C;
    MPC.P87PFS.BIT.PSEL = 0x1C;

    /* P32 VSYNC / P33 PCKO */
    PORT3.PMR.BYTE |= 0x0C;
    MPC.P32PFS.BIT.PSEL = 0x1C;
    MPC.P33PFS.BIT.PSEL = 0x1C;

    MPC.PWPR.BYTE = 0x00;
} /* End of function pdc_set_ports() */

/***********************************************************************************************************************
* Function Name: pdc_int_setting
* Description  : Makes settings for interrupts used by the PDC.
* Arguments    : step -
*                    Which step in setting interrupt
*                p_data_cfg -
*                    Pointer to PDC settings data structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_NOT_OPEN -
*                    R_PDC_Open has not been run.
*                PDC_ERR_INVALID_ARG -
*                    Parameter values in PDC setting information are invalid.
*                PDC_ERR_NULL_PTR -
*                    Argument p_data_cfg is a NULL pointer.
***********************************************************************************************************************/
static pdc_return_t pdc_int_setting(pdc_step_t step, pdc_data_cfg_t *p_data_cfg)
{
    pdc_return_t ret = PDC_SUCCESS;

    /* Make sure that the argument is correct. */
    /* To cast in order to compare the address. There is no problem because the information is not lost even if the
     *  cast. */
    if ((uint32_t)NULL == (uint32_t)p_data_cfg)
    {
        return PDC_ERR_NULL_PTR;
    }

    switch (step)
    {
        case PDC_INITIALIZE1:

            /* Make sure that the argument is correct. */
            if( PDC_IPR_LV_UPPER_LIMIT < p_data_cfg->priority.pcdfi_level )
            {
                return PDC_ERR_INVALID_ARG;
            }

            /* Attention: Interrupt Priority Level Select of GROUPBL0 and
                          Interrupt Request Enable of GROUPBL0 is not set here.
                          Interrupt of GROUPBL0 includes factors other than the PDC.
                          Please set Interrupt Priority Level of GROUPBL0 and
                          Interrupt Request Enable of GROUPBL0 before this process. */
            /* Set the interrupt priority level. */
            IPR(PDC, PCDFI) = p_data_cfg->priority.pcdfi_level;

            /* Set the interrupt setting */
            IR(PDC, PCDFI) = 0;        /* Interrupt request is cleared. */
            IEN(PDC, PCDFI) = p_data_cfg->inticu_req.pcdfi_ien;
            ICU.GENBL0.BIT.EN30 = 0;   /* Interrupt request(GRPBL0.IS30) is also cleared.  */
            ICU.GENBL0.BIT.EN30 = p_data_cfg->inticu_req.pcfei_ien;
            ICU.GENBL0.BIT.EN31 = 0;   /* Interrupt request(GRPBL0.IS31) is also cleared.  */
            ICU.GENBL0.BIT.EN31 = p_data_cfg->inticu_req.pceri_ien;
            IR(ICU, GROUPBL0)  = 0;
            IPR(ICU, GROUPBL0) = 1;
            IEN(ICU, GROUPBL0) = 1;

            break;

        case PDC_INITIALIZE2:

            /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
            PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
            PDC.PCCR0.BIT.DFIE = p_data_cfg->intpdc_req.dfie_ien;
            PDC.PCCR0.BIT.FEIE = p_data_cfg->intpdc_req.feie_ien;
            PDC.PCCR0.BIT.OVIE = p_data_cfg->intpdc_req.ovie_ien;
            PDC.PCCR0.BIT.UDRIE = p_data_cfg->intpdc_req.udrie_ien;
            PDC.PCCR0.BIT.VERIE = p_data_cfg->intpdc_req.verie_ien;
            PDC.PCCR0.BIT.HERIE = p_data_cfg->intpdc_req.herie_ien;
            break;

        case PDC_CHANGE_SETTING:
            ret = pdc_int_setting_sub(p_data_cfg);
            break;

        /* Illegal step is not supported */
        default:
            ret = PDC_ERR_INVALID_ARG;
            break;
    }
    return ret;
} /* End of function pdc_int_setting() */

/***********************************************************************************************************************
* Function Name: pdc_int_setting_sub
* Description  : Makes settings for interrupts used by the PDC.
* Arguments    : p_data_cfg -
*                    Pointer to PDC settings data structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_INVALID_ARG -
*                    Parameter values in PDC setting information are invalid.
*                PDC_ERR_NULL_PTR -
*                    Argument p_data_cfg is a NULL pointer.
***********************************************************************************************************************/
static pdc_return_t pdc_int_setting_sub(pdc_data_cfg_t *p_data_cfg)
{
    /* Make sure that the argument is correct. */
    /* To cast in order to compare the address. There is no problem because the information is not lost even if the
     *  cast. */
    if ((uint32_t)NULL == (uint32_t)p_data_cfg)
    {
        return PDC_ERR_NULL_PTR;
    }

    if (p_data_cfg->iupd_select & PDC_PCDFI_IPR_UPDATE)
    {
        /* Check the argument. When the argument is correct, set the interrupt priority level. */
        if (PDC_IPR_LV_UPPER_LIMIT < p_data_cfg->priority.pcdfi_level)
        {
            return PDC_ERR_INVALID_ARG;
        }
        IPR(PDC, PCDFI) = p_data_cfg->priority.pcdfi_level;
    }

    if (p_data_cfg->iupd_select & PDC_PCFEI_IEN_UPDATE)
    {
        ICU.GENBL0.BIT.EN30 = 0;   /* Interrupt request(GRPBL0.IS30) is also cleared.  */
        ICU.GENBL0.BIT.EN30 = p_data_cfg->inticu_req.pcfei_ien;
    }

    if (p_data_cfg->iupd_select & PDC_PCERI_IEN_UPDATE)
    {
        ICU.GENBL0.BIT.EN31 = 0;   /* Interrupt request(GRPBL0.IS31) is also cleared.  */
        ICU.GENBL0.BIT.EN31 = p_data_cfg->inticu_req.pceri_ien;
    }

    if (p_data_cfg->iupd_select & PDC_PCDFI_IEN_UPDATE)
    {
        IR(PDC, PCDFI) = 0;        /* Interrupt request is cleared. */
        IEN(PDC, PCDFI) = p_data_cfg->inticu_req.pcdfi_ien;
    }

    if (p_data_cfg->iupd_select & PDC_DFIE_IEN_UPDATE)
    {
        /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
        PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
        PDC.PCCR0.BIT.DFIE = p_data_cfg->intpdc_req.dfie_ien;
    }

    if (p_data_cfg->iupd_select & PDC_FEIE_IEN_UPDATE)
    {
        /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
        PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
        PDC.PCCR0.BIT.FEIE = p_data_cfg->intpdc_req.feie_ien;
    }

    if (p_data_cfg->iupd_select & PDC_OVIE_IEN_UPDATE)
    {
        /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
        PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
        PDC.PCCR0.BIT.OVIE = p_data_cfg->intpdc_req.ovie_ien;
    }

    if (p_data_cfg->iupd_select & PDC_UDRIE_IEN_UPDATE)
    {
        /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
        PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
        PDC.PCCR0.BIT.UDRIE = p_data_cfg->intpdc_req.udrie_ien;
    }

    if (p_data_cfg->iupd_select & PDC_VERIE_IEN_UPDATE)
    {
        /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
        PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
        PDC.PCCR0.BIT.VERIE = p_data_cfg->intpdc_req.verie_ien;
    }

    if (p_data_cfg->iupd_select & PDC_HERIE_IEN_UPDATE)
    {
        /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
        PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;
        PDC.PCCR0.BIT.HERIE = p_data_cfg->intpdc_req.herie_ien;
    }
    return PDC_SUCCESS;
} /* End of function pdc_int_setting_sub() */

/***********************************************************************************************************************
* Function Name: pdc_initialize
* Description  : Makes initial settings for using the PDC.
* Arguments    : p_data_cfg -
*                    Pointer to PDC settings data structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_INVALID_ARG -
*                    Argument p_data_cfg is a NULL pointer.
***********************************************************************************************************************/
static pdc_return_t pdc_initialize(pdc_data_cfg_t *p_data_cfg)
{
    pdc_return_t ret = PDC_SUCCESS;
    uint16_t value = 0;

    /* Make sure that the argument is correct. */
    /* To cast in order to compare the address. There is no problem because the information is not lost even if the
     *  cast. */
    if ((uint32_t)NULL == (uint32_t)p_data_cfg)
    {
        return PDC_ERR_NULL_PTR;
    }

    if (PDC_VST_UPPER_LIMIT < p_data_cfg->capture_pos.vst_position)
    {
        return PDC_ERR_INVALID_ARG;
    }

    if (PDC_HST_UPPER_LIMIT < p_data_cfg->capture_pos.hst_position)
    {
        return PDC_ERR_INVALID_ARG;
    }

    if ((PDC_VSZ_LOWER_LIMIT > p_data_cfg->capture_size.vsz_size)
            || (PDC_VSZ_UPPER_LIMIT < p_data_cfg->capture_size.vsz_size))
    {
        return PDC_ERR_INVALID_ARG;
    }

    if ((PDC_HSZ_LOWER_LIMIT > p_data_cfg->capture_size.hsz_size)
            || (PDC_HSZ_UPPER_LIMIT < p_data_cfg->capture_size.hsz_size))
    {
        return PDC_ERR_INVALID_ARG;
    }

    /* The range of values that dealt with in the register is 12-bit.
       Value obtained by adding the 12-bit to 12-bit is 16 bits or less.
       Because the information is not lost within the range to be handled, can be cast. */
    value = (uint16_t)(p_data_cfg->capture_pos.vst_position + p_data_cfg->capture_size.vsz_size);
    if (PDC_VSTVSZ_MIX_UPPER_LIMIT < value)
    {
        return PDC_ERR_INVALID_ARG;
    }

    /* The range of values that dealt with in the register is 12-bit.
       Value obtained by adding the 12-bit to 12-bit is 16 bits or less.
       Because the information is not lost within the range to be handled, can be cast. */
    value = (uint16_t)(p_data_cfg->capture_pos.hst_position + p_data_cfg->capture_size.hsz_size);
    if (PDC_HSTHSZ_MIX_UPPER_LIMIT < value)
    {
        return PDC_ERR_INVALID_ARG;
    }

    /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0.
       The VCR register should be set while the PCE bit in the PCCR1 register is 0.
       The HCR register should be set while the PCE bit in the PCCR1 register is 0. */
    PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;

    /* Enables PIXCLK input. */
    PDC.PCCR0.BIT.PCKE = PDC_ENABLE_PIXCLK_INPUT;

    /* Resets the PDC. */
    PDC.PCCR0.BIT.PRST = PDC_RESET;
    while (PDC_RESET_RELEASE != PDC.PCCR0.BIT.PRST)
    {
        /* Do Nothing */
    }

    /* Sets the vertical and horizontal capture ranges. */
    PDC.VCR.BIT.VST = p_data_cfg->capture_pos.vst_position;
    PDC.HCR.BIT.HST = p_data_cfg->capture_pos.hst_position;

    PDC.VCR.BIT.VSZ = p_data_cfg->capture_size.vsz_size;
    PDC.HCR.BIT.HSZ = p_data_cfg->capture_size.hsz_size;

    /* Sets the VSYNC and HSYNC signal polarity. */
    PDC.PCCR0.BIT.VPS = p_data_cfg->vps_select;
    PDC.PCCR0.BIT.HPS = p_data_cfg->hps_select;

    /* Makes settings for interrupts used by the PDC. */
    ret = pdc_int_setting(PDC_INITIALIZE2, p_data_cfg);
    if (PDC_SUCCESS != ret)
    {
        return ret;
    }

    /* Sets the endianness. */
#if (defined(__BIG) || defined(__BIG_ENDIAN__) || defined(__RX_BIG_ENDIAN__))
    PDC.PCCR0.BIT.EDS = PDC_EDS_BIG_ENDIAN;
#else
    PDC.PCCR0.BIT.EDS = PDC_EDS_LITTLE_ENDIAN;
#endif

    return PDC_SUCCESS;
} /* End of function pdc_initialize() */

/***********************************************************************************************************************
* Function Name: pdc_capture_start
* Description  : After resetting interrupt settings and the PDC, enables PDC receive operation to start data capture.
* Arguments    : none
* Return Values: none
***********************************************************************************************************************/
static void pdc_capture_start(void)
{
    uint32_t value = 0;

    /* The PCCR0 register should only be set while the PCE bit in the PCCR1 register is 0. */
    PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;

    /* Internal interrupt request is cleared. */
    value = PDC.PCCR0.LONG;
    PDC.PCCR0.BIT.DFIE = 0;
    PDC.PCCR0.BIT.FEIE = 0;
    PDC.PCCR0.BIT.OVIE = 0;
    PDC.PCCR0.BIT.UDRIE = 0;
    PDC.PCCR0.BIT.VERIE = 0;
    PDC.PCCR0.BIT.HERIE = 0;
    value &= 0x000007F0;
    PDC.PCCR0.LONG |= value;
    IR(PDC, PCDFI) = 0;            /* Interrupt request is cleared. */
    if (1 == ICU.GENBL0.BIT.EN30)
    {
        ICU.GENBL0.BIT.EN30 = 0;   /* Interrupt request(GRPBL0.IS30) is also cleared.  */
        ICU.GENBL0.BIT.EN30 = 1;
    }
    if (1 == ICU.GENBL0.BIT.EN31)
    {
        ICU.GENBL0.BIT.EN31 = 0;   /* Interrupt request(GRPBL0.IS31) is also cleared.  */
        ICU.GENBL0.BIT.EN31 = 1;
    }

    /* Resets the PDC. */
    PDC.PCCR0.BIT.PRST = PDC_RESET;
    while (PDC_RESET_RELEASE != PDC.PCCR0.BIT.PRST)
    {
        /* Do Nothing */
    }

    /* Enabling of PDC operation. */
    PDC.PCCR1.BIT.PCE = PDC_ENABLE_OPERATION;

} /* End of function pdc_capture_start() */

/***********************************************************************************************************************
* Function Name: pdc_change_position_size
* Description  : Resets the capture start position and size settings.
* Arguments    : p_data_cfg -
*                    Pointer to PDC settings data structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_INVALID_ARG -
*                    Parameter values in PDC setting information are invalid.
*                PDC_ERR_NULL_PTR -
*                    Argument p_data_cfg is a NULL pointer.
***********************************************************************************************************************/
static pdc_return_t pdc_change_position_size(pdc_data_cfg_t *p_data_cfg)
{
    uint16_t value = 0;

    /* Make sure that the argument is correct. */
    /* To cast in order to compare the address. There is no problem because the information is not lost even if the
     *  cast. */
    if ((uint32_t)NULL == (uint32_t)p_data_cfg)
    {
        return PDC_ERR_NULL_PTR;
    }

    if (PDC_VST_UPPER_LIMIT < p_data_cfg->capture_pos.vst_position)
    {
        return PDC_ERR_INVALID_ARG;
    }

    if (PDC_HST_UPPER_LIMIT < p_data_cfg->capture_pos.hst_position)
    {
        return PDC_ERR_INVALID_ARG;
    }

    if ((PDC_VSZ_LOWER_LIMIT > p_data_cfg->capture_size.vsz_size)
            || (PDC_VSZ_UPPER_LIMIT < p_data_cfg->capture_size.vsz_size))
    {
        return PDC_ERR_INVALID_ARG;
    }

    if ((PDC_HSZ_LOWER_LIMIT > p_data_cfg->capture_size.hsz_size)
            || (PDC_HSZ_UPPER_LIMIT < p_data_cfg->capture_size.hsz_size))
    {
        return PDC_ERR_INVALID_ARG;
    }

    /* The range of values that dealt with in the register is 12-bit.
       Value obtained by adding the 12-bit to 12-bit is 16 bits or less.
       Because the information is not lost within the range to be handled, can be cast. */
    value = (uint16_t)(p_data_cfg->capture_pos.vst_position + p_data_cfg->capture_size.vsz_size);
    if (PDC_VSTVSZ_MIX_UPPER_LIMIT < value)
    {
        return PDC_ERR_INVALID_ARG;
    }

    /* The range of values that dealt with in the register is 12-bit.
       Value obtained by adding the 12-bit to 12-bit is 16 bits or less.
       Because the information is not lost within the range to be handled, can be cast. */
    value = (uint16_t)(p_data_cfg->capture_pos.hst_position + p_data_cfg->capture_size.hsz_size);
    if (PDC_HSTHSZ_MIX_UPPER_LIMIT < value)
    {
        return PDC_ERR_INVALID_ARG;
    }

    /* The VCR register should be set while the PCE bit in the PCCR1 register is 0.
       The HCR register should be set while the PCE bit in the PCCR1 register is 0. */
    PDC.PCCR1.BIT.PCE = PDC_DISABLE_OPERATION;

    /* Sets the vertical and horizontal capture ranges. */
    PDC.VCR.BIT.VST = p_data_cfg->capture_pos.vst_position;
    PDC.HCR.BIT.HST = p_data_cfg->capture_pos.hst_position;

    PDC.VCR.BIT.VSZ = p_data_cfg->capture_size.vsz_size;
    PDC.HCR.BIT.HSZ = p_data_cfg->capture_size.hsz_size;

    return PDC_SUCCESS;
} /* End of function pdc_change_position_size() */

/***********************************************************************************************************************
* Function Name: pdc_clear_status
* Description  : Clears PDC status information indicated by argument p_stat.
* Arguments    : p_stat -
*                    Pointer to PDC status structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_NULL_PTR -
*                    The argument p_data_cfg or p_stat is a NULL pointer.
***********************************************************************************************************************/
static pdc_return_t pdc_clear_status(pdc_stat_t *p_stat)
{
    /* Make sure that the argument is correct. */
    /* To cast in order to compare the address. There is no problem because the information is not lost even if the
     *  cast. */
    if ((uint32_t)NULL == (uint32_t)p_stat)
    {
        return PDC_ERR_NULL_PTR;
    }

    /* Clear PDC Status Register(PCSR). */
    if (true == p_stat->pcsr_stat.frame_busy)
    {
        PDC.PCSR.BIT.FBSY = 0;
    }
    if (true == p_stat->pcsr_stat.fifo_empty)
    {
        PDC.PCSR.BIT.FEMPF = 0;
    }
    if (true == p_stat->pcsr_stat.frame_end)
    {
        PDC.PCSR.BIT.FEF = 0;
    }
    if (true == p_stat->pcsr_stat.overrun)
    {
        PDC.PCSR.BIT.OVRF = 0;
    }
    if (true == p_stat->pcsr_stat.underrun)
    {
        PDC.PCSR.BIT.UDRF = 0;
    }
    if (true == p_stat->pcsr_stat.verf_error)
    {
        PDC.PCSR.BIT.VERF = 0;
    }
    if (true == p_stat->pcsr_stat.herf_error)
    {
        PDC.PCSR.BIT.HERF = 0;
    }
    return PDC_SUCCESS;
} /* End of function pdc_clear_status() */

/***********************************************************************************************************************
* Function Name: pdc_get_status
* Description  : Writes PDC status information to the pointer position indicated by argument p_stat.
* Arguments    : p_stat -
*                    Pointer to PDC status structure
* Return Values: PDC_SUCCESS -
*                    Processing finished successfully.
*                PDC_ERR_NULL_PTR -
*                    The argument p_data_cfg or p_stat is a NULL pointer.
***********************************************************************************************************************/
static pdc_return_t pdc_get_status(pdc_stat_t *p_stat)
{
    /* Make sure that the argument is correct. */
    /* To cast in order to compare the address. There is no problem because the information is not lost even if the
     *  cast. */
    if ((uint32_t)NULL == (uint32_t)p_stat)
    {
        return PDC_ERR_NULL_PTR;
    }

    /* Read the PDC Status Register(PCSR). */
    /* Since the value of the register is defined by the BIT, the value to be handled is 1 or 0.
       Because the information is not lost even if the cast to bool, it is okay to cast. */
    p_stat->pcsr_stat.frame_busy = (bool)PDC.PCSR.BIT.FBSY;
    p_stat->pcsr_stat.fifo_empty = (bool)PDC.PCSR.BIT.FEMPF;
    p_stat->pcsr_stat.frame_end = (bool)PDC.PCSR.BIT.FEF;
    p_stat->pcsr_stat.overrun = (bool)PDC.PCSR.BIT.OVRF;
    p_stat->pcsr_stat.underrun = (bool)PDC.PCSR.BIT.UDRF;
    p_stat->pcsr_stat.verf_error = (bool)PDC.PCSR.BIT.VERF;
    p_stat->pcsr_stat.herf_error = (bool)PDC.PCSR.BIT.HERF;

    /* Read the PDC Pin Monitor Register(PCMONR). */
    /* Since the value of the register is defined by the BIT, the value to be handled is 1 or 0.
       Because the information is not lost even if the cast to bool, it is okay to cast. */
    p_stat->pcmonr_stat.vsync = (bool)PDC.PCMONR.BIT.VSYNC;
    p_stat->pcmonr_stat.hsync = (bool)PDC.PCMONR.BIT.HSYNC;

    return PDC_SUCCESS;
} /* End of function pdc_get_status() */

bool isPdcFinished(){
//    return PDC.PCSR.BIT.FBSY;
    return (DMAC0.DMCRB == 0) && (PDC.PCSR.BIT.FEMPF == 1);
}

void int_excep_pdc_pcdfi(){
    callback_receive_data_ready();
}

void int_excep_icu_groupbl0(void){
    if (1 == ICU.GRPBL0.BIT.IS30)
    {
        callback_frame_end();
    }

    if (1 == ICU.GRPBL0.BIT.IS31)
    {
        callback_error();
    }
}

/* End of File */
