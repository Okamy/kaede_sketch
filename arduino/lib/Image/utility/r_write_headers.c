/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/
/***********************************************************************************************************************
* File Name	   : r_write_headers.c
* Description  :
***********************************************************************************************************************/
/**********************************************************************************************************************
* History : DD.MM.YYYY Version  Description
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include "r_jpeg_development.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define _JPEG_MARKER		0xff
#define _JPEG_MARKER_SOI	0xd8
#define _JPEG_MARKER_SOS	0xda
#define _JPEG_MARKER_DQT	0xdb
#define _JPEG_MARKER_SOF0	0xC0
#define _JPEG_MARKER_APP0	0xe0

#define SOF0_LEN0			0x00
#define SOF0_LEN1			0x11
#define SOF0_DATA_PRECISION	0x08
#define SOF0_NUM_COMPONENT	3

#define START_OF_SPECTRAL_SELECTION				0
#define END_OF_SPECTRAL_SELECTION				63
#define SUCCESSIVE_APPROXIMATION_BITS_POSITION	0

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Exported global variables (to be accessed by other files)
***********************************************************************************************************************/

/***********************************************************************************************************************
Private global variables and functions
***********************************************************************************************************************/
#pragma section _jpeg_cmp_S

static const int natural_order[_JPEG_DCTSIZE2] =
{
	0,  1,  8, 16,  9,  2,  3, 10,
	17, 24, 32, 25, 18, 11,  4,  5,
	12, 19, 26, 33, 40, 48, 41, 34,
	27, 20, 13,  6,  7, 14, 21, 28,
	35, 42, 49, 56, 57, 50, 43, 36,
	29, 22, 15, 23, 30, 37, 44, 51,
	58, 59, 52, 45, 38, 31, 39, 46,
	53, 60, 61, 54, 47, 55, 62, 63,
};

int32_t _jpeg_writeSOI(struct _jpeg_working *);
int32_t _jpeg_writeAPP(struct _jpeg_working *);
int32_t _jpeg_writeDQT(struct _jpeg_working *);
int32_t _jpeg_writeSOF0(struct _jpeg_working *);
int32_t _jpeg_writeDHT(struct _jpeg_working *);
int32_t _jpeg_writeSOS(struct _jpeg_working *);
int32_t _jpeg_writeAPP(struct _jpeg_working *);


/***********************************************************************************************************************
* Function Name: _jpeg_write_header
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
int32_t _jpeg_write_header(struct _jpeg_working *wenv)
{
	int32_t res;
	int rst_interval;

	res = _jpeg_writeSOI(wenv);
	if (res != _JPEG_OK)
	{
		return res;
	}
	res = _jpeg_writeAPP(wenv);
	if (res != _JPEG_OK)
	{
		return res;
	}

	if (_jpeg_write_DQT(wenv->encSMB) != 0)
	{
		if (_jpeg_num_QTBL(wenv->encSMB) == 0)
		{
			return _JPEG_ERROR;			/* error! */
		}
		_jpeg_writeDQT(wenv);
	}
	res = _jpeg_writeSOF0(wenv);
	if (res != _JPEG_OK)
	{
		return res;
	}
	res = _jpeg_writeDHT(wenv);
	if (res != _JPEG_OK)
	{
		return res;
	}
	rst_interval = _jpeg_restart_interval(wenv->encFMB);
	if (rst_interval)
	{
		R_jpeg_writeDRI(rst_interval, wenv);
	}
	res = _jpeg_writeSOS(wenv);
	if (res != _JPEG_OK)
	{
		return res;
	}

	return _JPEG_OK;
}


/***********************************************************************************************************************
* Function Name: _jpeg_writeSOI
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
int32_t _jpeg_writeSOI(struct _jpeg_working *wenv)
{
	WRITE_BYTE(_JPEG_MARKER, wenv);
	WRITE_BYTE(_JPEG_MARKER_SOI, wenv);

	return _JPEG_OK;
}


/***********************************************************************************************************************
* Function Name: _jpeg_writeSOF0
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
int32_t _jpeg_writeSOF0(struct _jpeg_working *wenv)
{
	uint8_t *comp, *h_ratio, *v_ratio, *qtbl;
	int i;
	struct component_info *cinfo;
	struct _jpeg_enc_SMB *SMBbase;

	WRITE_BYTE(_JPEG_MARKER, wenv);
	WRITE_BYTE(_JPEG_MARKER_SOF0, wenv);
	WRITE_BYTE(SOF0_LEN0, wenv);
	WRITE_BYTE(SOF0_LEN1, wenv);
	WRITE_BYTE(SOF0_DATA_PRECISION, wenv);
	SMBbase = wenv->encSMB;
	WRITE_BYTE(_jpeg_number_of_lines(SMBbase) >> 8, wenv);
	WRITE_BYTE(_jpeg_number_of_lines(SMBbase) & 0xff, wenv);
	WRITE_BYTE(_jpeg_line_length(SMBbase) >> 8, wenv);
	WRITE_BYTE(_jpeg_line_length(SMBbase) & 0xff, wenv);
	WRITE_BYTE(SOF0_NUM_COMPONENT, wenv);
	cinfo = & component_info(SMBbase);

	comp = cinfo->component_id + 1;
	h_ratio = cinfo->hsample_ratio + 1;
	v_ratio = cinfo->vsample_ratio + 1;
	qtbl = cinfo->quant_tbl_no + 1;
	for (i = SOF0_NUM_COMPONENT; i; --i)
	{
		WRITE_BYTE(*comp, wenv);
		WRITE_BYTE(((*h_ratio) << 4) | (*v_ratio), wenv);
		WRITE_BYTE(*qtbl, wenv);
		++comp, ++h_ratio, ++v_ratio, ++qtbl;
	}
	return _JPEG_OK;
}

/***********************************************************************************************************************
* Function Name: _jpeg_writeSOS
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
int32_t _jpeg_writeSOS(struct _jpeg_working *wenv)
{
	uint8_t *frame, *dc_tbl_no, *ac_tbl_no;
	struct frame_component_info *info;
	int len, i;
	struct _jpeg_enc_SMB *SMBbase;

	WRITE_BYTE(_JPEG_MARKER, wenv);
	WRITE_BYTE(_JPEG_MARKER_SOS, wenv);
	SMBbase = wenv->encSMB;
	len = (_jpeg_frame_num_of_components(SMBbase) << 1) + 6;
	WRITE_BYTE(len >> 8, wenv);
	WRITE_BYTE(len & 0xff, wenv);
	WRITE_BYTE(_jpeg_frame_num_of_components(SMBbase), wenv);
	info = & frame_component_info(SMBbase);
	frame = &(info->component_id[1]);
	dc_tbl_no = &(info->dc_tbl_no[1]);
	ac_tbl_no = &(info->ac_tbl_no[1]);
	for (i = _jpeg_frame_num_of_components(SMBbase); i; --i)
	{
		WRITE_BYTE(*frame, wenv);
		WRITE_BYTE(((*dc_tbl_no) << 4) | (*ac_tbl_no), wenv);
		++frame, ++dc_tbl_no, ++ac_tbl_no;
	}
	WRITE_BYTE(START_OF_SPECTRAL_SELECTION, wenv);
	WRITE_BYTE(END_OF_SPECTRAL_SELECTION, wenv);
	WRITE_BYTE(SUCCESSIVE_APPROXIMATION_BITS_POSITION, wenv);

	return _JPEG_OK;
}


/***********************************************************************************************************************
* Function Name: _jpeg_writeDQT
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
int32_t _jpeg_writeDQT(struct _jpeg_working *wenv)
{
	uint16_t *qtbl_ptr, qval;
	uint8_t *qtbl_prec_ptr;
	int prec, i, n;
	struct _jpeg_enc_SMB *SMBbase;

	SMBbase = wenv->encSMB;
	qtbl_ptr = _jpeg_QTBL(SMBbase);
	qtbl_prec_ptr = _jpeg_precision_QTBL(SMBbase);

	for (i = 0; i < _jpeg_num_QTBL(SMBbase); ++i)
	{
		/* Write DQT */
		WRITE_BYTE(_JPEG_MARKER, wenv);
		WRITE_BYTE(_JPEG_MARKER_DQT, wenv);

		prec = *qtbl_prec_ptr++;
		n = (64 << prec) + 3;
		WRITE_BYTE(n >> 8, wenv);
		WRITE_BYTE(n & 0xff, wenv);
		WRITE_BYTE(i + (prec << 4), wenv);
		for (n = 0; n < _JPEG_DCTSIZE2; ++n)
		{
			qval = qtbl_ptr[natural_order[n]];
			if (prec)
			{
				WRITE_BYTE(qval >> 8, wenv);
			}
			WRITE_BYTE(qval&0xff, wenv);
		}
		qtbl_ptr += (64 << prec);
	}
	return _JPEG_OK;
}


/***********************************************************************************************************************
* Function Name: _jpeg_writeDHT
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
int32_t _jpeg_writeDHT(struct _jpeg_working *wenv)
{
	int len;
	uint8_t *src;
	struct _jpeg_enc_SMC *SMCbase;
	struct _jpeg_enc_FMB *FMBbase;

	SMCbase = wenv->encSMC;
	src = _jpeg_DHT_CODE(SMCbase);
	len = 432;		/* _jpeg_DHT_CODE_END - _jpeg_DHT_CODE */

	FMBbase = wenv->encFMB;
	while (_jpeg_free_in_buffer(FMBbase) <= len)
	{
		memcpy(_jpeg_next_output_byte(FMBbase), src, _jpeg_free_in_buffer(FMBbase));
		_jpeg_next_output_byte(FMBbase) += _jpeg_free_in_buffer(FMBbase);
		src += _jpeg_free_in_buffer(FMBbase);
		len -= _jpeg_free_in_buffer(FMBbase);
		_jpeg_free_in_buffer(FMBbase) = 0;
		(*(wenv->enc_dump_func))(wenv);
	}
	memcpy(_jpeg_next_output_byte(FMBbase), src, len);
	_jpeg_next_output_byte(FMBbase) += len;
	_jpeg_free_in_buffer(FMBbase) -= len;

	return _JPEG_OK;
}


/***********************************************************************************************************************
* Function Name: _jpeg_writeAPP
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
int32_t _jpeg_writeAPP(struct _jpeg_working *wenv)
{
	WRITE_BYTE(_JPEG_MARKER, wenv);
	WRITE_BYTE(_JPEG_MARKER_APP0, wenv);
	WRITE_BYTE(0x00, wenv);
	WRITE_BYTE(0x10, wenv);
	WRITE_BYTE('J', wenv);
	WRITE_BYTE('F', wenv);
	WRITE_BYTE('I', wenv);
	WRITE_BYTE('F', wenv);
	WRITE_BYTE(0x00, wenv);
	WRITE_BYTE(0x01, wenv);
	WRITE_BYTE(0x01, wenv);
	WRITE_BYTE(0x00, wenv);
	WRITE_BYTE(0x00, wenv);	/* Horizontal pixel density */
	WRITE_BYTE(0x01, wenv);
	WRITE_BYTE(0x00, wenv);	/* Vertical pixel density */
	WRITE_BYTE(0x01, wenv);
	WRITE_BYTE(0x00, wenv);	/* Thumbnail pixel counts */
	WRITE_BYTE(0x00, wenv);

	return _JPEG_OK;
}
