/* Copyright (C) 2006-2013 Renesas Solutions Corp. */

#include "r_jpeg_development.h"

#pragma section _jpeg_enc_F


#if defined(R32C)
typedef char	emitsize;
#else
typedef int32_t	emitsize;
#endif
int32_t emit_bits(uint32_t, emitsize, struct _jpeg_working *);

int32_t R_jpeg_flush_bits(struct _jpeg_working *wenv)
{
	int32_t err;
	struct _jpeg_enc_FMB *FMBbase;
	
	err = emit_bits((uint32_t) 0x7F << (32 - 7), 7, wenv);	/* fill any partial byte with ones */
	if(err!=_JPEG_OK)
	{
		return _JPEG_ERROR;
	}
	FMBbase = wenv->encFMB;

	_jpeg_cur_put_buffer(FMBbase) = 0;
	_jpeg_cur_put_bits(FMBbase) = 0;

	return _JPEG_OK;
}


#define _JPEG_MARKER		0xff
#define _JPEG_MARKER_EOI 0xd9

#if 0
#define WRITE_BYTE(c, env)					\
	*_jpeg_next_output_byte(env->encFMB) = (c);			\
	++_jpeg_next_output_byte(env->encFMB);			\
	--_jpeg_free_in_buffer(env->encFMB);			\
	if (_jpeg_free_in_buffer(env->encFMB) == 0) {		\
		(*(env->enc_dump_func))(env);				\
	}
#endif

int32_t R_jpeg_writeEOI(struct _jpeg_working *wenv)
{
	WRITE_BYTE(_JPEG_MARKER, wenv);	/* DRI: 0xFFDD */
	WRITE_BYTE(_JPEG_MARKER_EOI, wenv);
	
	return _JPEG_OK;
}

/*
    Copyright,2006 RENESAS TECHNOLOGY CORPORATION AND
                   RENESAS SOLUTIONS CORPORATION ALL RIGHT RESERVED
 */
