/*
 * gr_dma_driver.c
 *
 *  Created on: 2015/08/22
 *      Author: a5034000
 */

#include "gr_dma_driver.h"
#include "gr_pdc_driver.h"
#include "rx64m/util.h"
#include "rx64m/interrupt_handlers.h"

/* This array holds callback functions. */
void *p_DMACI_Handlers[DMACA_NUM_CHANNELS];
dmaca_transfer_data_cfg_t    g_data_dmac_ch0;
dmaca_transfer_data_cfg_t    g_data_dmac_ch1;

/* Array of address of DMRSRm (m=DMACA channel number) */
volatile uint8_t *g_icu_dmrsr[] =
{
    &(ICU.DMRSR0), &(ICU.DMRSR1), &(ICU.DMRSR2), &(ICU.DMRSR3),
    &(ICU.DMRSR4), &(ICU.DMRSR5), &(ICU.DMRSR6), &(ICU.DMRSR7)
};

/*******************************************************************************
Private variables and functions
*******************************************************************************/
static bool r_dmaca_set_transfer_data(uint8_t channel, dmaca_transfer_data_cfg_t *p_cfg);

/*******************************************************************************
* Function Name: R_DMACA_Open
* Description  : Initializes the DMACA driver and locks the specified channel.
* Arguments    : channel -
*                    Which channel to use
* Return Value : DMACA_SUCCESS -
*                    Successful operation
*                DMACA_ERR_INVALID_CH -
*                    Channel is invalid.
*                DMACA_ERR_BUSY -
*                    Resource has been locked by other process already.
*******************************************************************************/
dmaca_return_t R_DMACA_Open(uint8_t channel)
{
    /* Clear a Activation Request Select Register. */
    ICU_DMRSR(channel)  = 0x00;

    if (0x00 == ICU_DMRSR(channel))
    {
        /* do nothing */
    }

    /* Cancel module stop for DMAC and DTC. */
//    r_dmaca_module_enable();
    startModule(MstpIdDMAC);

    /* Enable the DMAC activation. */
    DMACA_DMAST = DMACA_ACTIVE_ENABLE;

    if (DMACA_ACTIVE_ENABLE == DMACA_DMAST)
    {
        /* do nothing */
    }

    return DMACA_SUCCESS;
}

/*******************************************************************************
* Function Name: R_DMACA_Close
* Description  : Unlocks the specified channel and sets to module stop state.
* Arguments    : channel -
*                    Which channel to use
* Return Value : DMACA_SUCCESS -
*                    Successful operation
*                    Set both DMAC and DTC to module stop state.
*                DMACA_ERR_INVALID_CH -
*                    Channel is invalid.
*                DMACA_SUCCESS_OTHER_CH_BUSY -
*                    Successful operation
*                    Other DMAC channels are locked, so that cannot set to module stop state.
*                DMACA_SUCCESS_DTC_BUSY -
*                    Successful operation
*                    DTC is locked, so that cannot set to module stop state.
*                DMACA_ERR_INTERNAL -
*                    DMACA driver internal error
*******************************************************************************/
dmaca_return_t R_DMACA_Close(uint8_t channel)
{

    /* Clear a Activation Request Select Register. */
    ICU_DMRSR(channel)  = 0x00;

    if (0x00 == ICU_DMRSR(channel))
    {
        /* do nothing */
    }

    /* Disable DMA transfers. */
    DMACA_DMCNT(channel) = DMACA_TRANSFER_DISABLE;
    stopModule(MstpIdDMAC);

    return DMACA_SUCCESS;
}

/*******************************************************************************
* Function Name: R_DMACA_Create
* Description  : Sets registers of the specified channel and the interrupt source.
* Arguments    : channel -
*                    Which channel to use
*                *p_data_cfg -
*                    Pointer to information of transfer data
* Return Value : DMACA_SUCCESS -
*                    Successful operation
*                DMACA_ERR_INVALID_CH -
*                    Channel is invalid.
*                DMACA_ERR_INVALID_ARG -
*                    Parameters are invalid.
*                DMACA_ERR_NULL_PTR -
*                    Argument pointers are NULL.
*******************************************************************************/
dmaca_return_t R_DMACA_Create(uint8_t channel, dmaca_transfer_data_cfg_t *p_data_cfg)
{

    if (false == r_dmaca_set_transfer_data(channel, p_data_cfg))
    { /* Fail to apply configurations for Transfer data. */
        return DMACA_ERR_INVALID_ARG;
    }

    return DMACA_SUCCESS;
}

/*******************************************************************************
* Function Name: R_DMACA_Control
* Description  : Starts/Stops DMACA and checks an interrupt source as DMACA activation.
* Arguments    : channel -
*                    Which channel to use
*                command -
*                Control command
*                *p_stat -
*                    Pointer information of DMACA driver status
*                    When a command is DMACA_CMD_STATUS_GET, casts to void *.
* Return Value : DMACA_SUCCESS -
*                    Successful operation
*                DMACA_ERR_INVALID_CH -
*                    Channel is invalid.
*                DMACA_ERR_INVALID_COMMAND -
*                    Command is invalid.
*                DMACA_ERR_NULL_PTR -
*                    Argument pointers are NULL.
*                DMACA_ERR_SOFTWARE_REQUESTED -
*                    DMA transfer request by software is generated (DMREQ.SWREQ=1b) already.
*                    So that cannot execute command.
*                DMACA_ERR_SOFTWARE_REQUEST_DISABLED -
*                    Transfer Request Source is not Software (DMTMD.DCTG[1:0]=01b).
*                    So that cannot execute command.
*******************************************************************************/
dmaca_return_t R_DMACA_Control(uint8_t channel, dmaca_command_t command, dmaca_stat_t *p_stat)
{
    uint8_t dmaca_interrupt_status = 0x00;
    uint8_t dmaca_soft_req_status  = 0x00;

    switch (command)
    {
        case DMACA_CMD_ENABLE: /* Enable DMA transfer of the specified channel. */
        case DMACA_CMD_RESUME: /* Resume DMA transfer of the specified channel. */
            DMACA_DMCNT(channel) = DMACA_TRANSFER_ENABLE;
            if (DMACA_TRANSFER_ENABLE == DMACA_DMCNT(channel))
            {
                /* do nothing */
            }
        break;

        case DMACA_CMD_DISABLE: /* Disable DMA transfer of the specified channel. */
            DMACA_DMCNT(channel) = DMACA_TRANSFER_DISABLE;
            if (DMACA_TRANSFER_DISABLE == DMACA_DMCNT(channel))
            {
                /* do nothing */
            }
        break;

        case DMACA_CMD_ALL_ENABLE: /* Enable DMAC activation. */
            DMACA_DMAST = DMACA_ACTIVE_ENABLE;
            if (DMACA_ACTIVE_ENABLE == DMACA_DMAST)
            {
                /* do nothing */
            }
        break;

        case DMACA_CMD_ALL_DISABLE: /* Disable DMAC activation. */
            DMACA_DMAST = DMACA_ACTIVE_DISABLE;
            if (DMACA_ACTIVE_DISABLE == DMACA_DMAST)
            {
                /* do nothing */
            }
        break;

        case DMACA_CMD_SOFT_REQ_WITH_AUTO_CLR_REQ: /* DMA transfer by Software using DMREQ.SWREQ bit Auto Clear mode */
            /* Check DMTMD.DCTG[1:0] bits. */
            if (DMACA_TRANSFER_REQUEST_SOFTWARE != (DMACA_DMTMD(channel) & DMACA_REQ_BIT_MASK))
            {
                return DMACA_ERR_SOFTWARE_REQUEST_DISABLED;
            }

            /* Check a DMREQ.CLRS bit. */
            if (0 == (DMACA_DMREQ(channel) & DMACA_CLRS_BIT_MASK))
            {
                /* Check a DMREQ.SWREQ bit. */
                if (0 !=  (DMACA_DMREQ(channel) & DMACA_SWREQ_BIT_MASK))
                {
                    return DMACA_ERR_SOFTWARE_REQUESTED;
                }
            }

            DMACA_DMREQ(channel) = DMACA_SOFT_REQ_WITH_AUTO_CLR_REQ;

            if (DMACA_SOFT_REQ_WITH_AUTO_CLR_REQ == DMACA_DMREQ(channel))
            {
                /* do nothing */
            }
        break;

        case DMACA_CMD_SOFT_REQ_NOT_CLR_REQ: /* DMA transfer by Software */
            /* Check DMTMD.DCTG[1:0] bits. */
            if (DMACA_TRANSFER_REQUEST_SOFTWARE != (DMACA_DMTMD(channel) & DMACA_REQ_BIT_MASK))
            {
                return DMACA_ERR_SOFTWARE_REQUEST_DISABLED;
            }

            DMACA_DMREQ(channel) = DMACA_SOFT_REQ_NOT_CLR_REQ;

            if (DMACA_SOFT_REQ_NOT_CLR_REQ == DMACA_DMREQ(channel))
            {
                /* do nothing */
            }
        break;

        case DMACA_CMD_SOFT_REQ_CLR: /* Clear DMREQ.SWREQ bit. */
            /* Check DMTMD.DCTG[1:0] bits. */
            if (DMACA_TRANSFER_REQUEST_SOFTWARE != (DMACA_DMTMD(channel) & DMACA_REQ_BIT_MASK))
            {
                return DMACA_ERR_SOFTWARE_REQUEST_DISABLED;
            }

            DMACA_DMREQ(channel) &= ~DMACA_SWREQ_BIT_MASK;

            if (0x00 == (DMACA_DMREQ(channel) & DMACA_SWREQ_BIT_MASK))
            {
                /* do nothing */
            }
        break;

        case DMACA_CMD_STATUS_GET:

            dmaca_interrupt_status = DMACA_DMSTS(channel);
            dmaca_soft_req_status = DMACA_DMREQ(channel);

            if (0 == (dmaca_interrupt_status & DMACA_ACT_BIT_MASK)) /* Get the current status of DMACA. */
            {   /* DMACA operation is suspended. */
                p_stat->act_stat = false;
            }
            else
            {  /* DMACA is operating. */
                p_stat->act_stat = true;
            }

            if (0 == (dmaca_interrupt_status & DMACA_DTIF_BIT_MASK)) /* Get the transfer end interrupt flag. */
            {  /* A transfer end interrupt has not been generated. */
                p_stat->dtif_stat = false;
            }
            else
            {  /* A transfer end interrupt has been generated. */
                p_stat->dtif_stat = true;
            }

            if (0 == (dmaca_interrupt_status & DMACA_ESIF_BIT_MASK)) /* Get the escape end interrupt flag. */
            {  /* A transfer escape end interrupt has not been generated. */
                p_stat->esif_stat = false;
            }
            else
            {  /* A transfer escape end interrupt has been generated. */
                p_stat->esif_stat = true;
            }

            if (0 == (dmaca_soft_req_status & DMACA_SWREQ_BIT_MASK)) /* Get the SWREQ flag. */
            {  /* A software transfer is not requested. */
                p_stat->soft_req_stat = false;
            }
            else
            {  /* A software transfer is requested. */
                p_stat->soft_req_stat = true;
            }
        break;

        case DMACA_CMD_ESIF_STATUS_CLR:
            DMACA_DMSTS(channel) = ~((uint8_t)DMACA_ESIF_BIT_MASK);
            if (0x00 == DMACA_DMSTS(channel))
            {
                /* do nothing */
            }
        break;

        case DMACA_CMD_DTIF_STATUS_CLR:
            DMACA_DMSTS(channel) = ~((uint8_t)DMACA_DTIF_BIT_MASK);
            if (0x00 == DMACA_DMSTS(channel))
            {
                /* do nothing */
            }
        break;

        default:
            return DMACA_ERR_INVALID_COMMAND;
        break;
    }

    return DMACA_SUCCESS;
}


/*******************************************************************************
* Function Name: R_DMACA_Int_Enable
* Description  : Enables DMACmI interrupt. (m = 0 to 3, or 74)
* Arguments    : channel -
*                    Which channel to use
*              : priority -
*                    DMACmI interrupt priority level
* Return Value : DMACA_SUCCESS -
*                    Successful operation
*                DMACA_ERR_INVALID_CH -
*                    Channel is invalid.
*******************************************************************************/
dmaca_return_t R_DMACA_Int_Enable(uint8_t channel, uint8_t priority)
{

    /* Set the DMACmI priority level. */
    /* Set the DMACmI interrupt Enable bit. */
    if (DMACA_SUCCESS != r_dmaca_int_enable(channel, priority))
    {
        return DMACA_ERR_INVALID_CH;
    }

    return DMACA_SUCCESS;
}

/*******************************************************************************
* Function Name: R_DMACA_Int_Disable
* Description  : Disables DMACmI interrupt. (m = 0 to 3, or 74)
* Arguments    : channel -
*                    Which channel to use
* Return Value : DMACA_SUCCESS -
*                    Successful operation
*                DMACA_ERR_INVALID_CH -
*                    Channel is invalid.
*******************************************************************************/
dmaca_return_t R_DMACA_Int_Disable(uint8_t channel)
{
    /* Clear the DMACmI interrupt Enable bit. */
    /* Clear the DMACmI priority level. */
    if (DMACA_SUCCESS != r_dmaca_int_disable(channel))
    {
        return DMACA_ERR_INVALID_CH;
    }

    return DMACA_SUCCESS;
}

/*******************************************************************************
* Function Name: r_dmaca_set_transfer_data
* Description  : Applies configurations to Transfer data area.
*                It is an internal function called by R_DMACA_Create().
*                and all arguments are validated in R_DMACA_Create().
* Arguments    : channel -
*                    Which channel to use
*                *p_data_cfg -
*                    Pointer to information of Transfer data
* Return Value : true -
*                    Apply configurations for Transfer data successfully.
*                false -
*                    Fail to apply configurations for Transfer data.
*******************************************************************************/
static bool r_dmaca_set_transfer_data(uint8_t channel, dmaca_transfer_data_cfg_t *p_cfg)
{

    /* Clear DMREQ register. */
    DMACA_DMREQ(channel) = 0x00;

   /* Disable DMA transfers. */
    DMACA_DMCNT(channel) = DMACA_TRANSFER_DISABLE;

    /* Set ICU.DMRSR register. */
    if (DMACA_TRANSFER_REQUEST_PERIPHERAL == p_cfg->request_source)
    {
        ICU_DMRSR(channel) = (uint8_t)p_cfg->act_source;
        if ((uint8_t)p_cfg->act_source == ICU_DMRSR(channel))
        {
            /* do nothing */
        }
    }

    /* Set DMAMD register. */
    DMACA_DMAMD(channel) = (uint16_t)(p_cfg->src_addr_mode | p_cfg->src_addr_repeat_area | p_cfg->des_addr_mode |
                                     p_cfg->des_addr_repeat_area);

    /* Set DMTMD register. */
    DMACA_DMTMD(channel) = (uint16_t)(p_cfg->transfer_mode | p_cfg->repeat_block_side | p_cfg->data_size |
                                     p_cfg->request_source);

    /* Set DMSAR register. */
    DMACA_DMSAR(channel) = p_cfg->p_src_addr;

    /* Set DMDAR register. */
    DMACA_DMDAR(channel) = p_cfg->p_des_addr;

    switch (p_cfg->transfer_mode) /* DMACA transfer mode */
    {
        case DMACA_TRANSFER_MODE_NORMAL: /* Normal transfer mode */
            /* Set DMCRA register. */
            DMACA_DMCRA(channel) = p_cfg->transfer_count;
        break;

        case DMACA_TRANSFER_MODE_REPEAT: /* Repeat transfer mode */
        case DMACA_TRANSFER_MODE_BLOCK: /* Block transfer mode */
            /* Set DMCRA register. */
            DMACA_DMCRA(channel) = (uint32_t)((p_cfg->block_size << 16) | p_cfg->block_size);
            /* Set DMCRB register. */
            DMACA_DMCRB(channel) = (uint16_t)p_cfg->transfer_count;
        break;

        default:
            return false;
        break;
    }

    if (DMACA_CH0 == channel)
    {
        DMAC0.DMOFR = p_cfg->offset_value;
    }

    /* Set DMCSL register. */
    DMACA_DMCSL(channel) = (uint8_t)p_cfg->interrupt_sel;

    /* Set DMINT register */
    DMACA_DMINT(channel) = (uint8_t)(p_cfg->dtie_request | p_cfg->esie_request | p_cfg->rptie_request |
                                     p_cfg->sarie_request | p_cfg->darie_request);

    if ((uint8_t)p_cfg->dtie_request == DMACA_DMINT(channel))
    {
        /* do nothing */
    }

    return true;
}

/*******************************************************************************
* Function Name: r_dmaca_int_enable
* Description  : Enables DMACmI interrupt. (m = 0 to 3, or 74)
* Arguments    : channel -
*                    Which channel to use
*              : priority -
*                    DMACmI interrupt priority level
* Return Value : DMACA_SUCCESS -
*                    DMACmI interrupt is enabled successfully.
*                DMACA_ERR_INVALID_CH -
*                    Channel is invalid.
*******************************************************************************/
dmaca_return_t r_dmaca_int_enable(uint8_t channel, uint8_t priority)
{
    /* Set the DMACmI priority level. */
    /* Set the DMACmI interrupt Enable bit. */
    switch (channel)
    {
        case DMACA_CH0:
            IPR(DMAC, DMAC0I) = priority;
            IEN(DMAC, DMAC0I) = 1;
        break;

        case DMACA_CH1:
            IPR(DMAC, DMAC1I) = priority;
            IEN(DMAC, DMAC1I) = 1;
        break;

        case DMACA_CH2:
            IPR(DMAC, DMAC2I) = priority;
            IEN(DMAC, DMAC2I) = 1;
        break;

        case DMACA_CH3:
            IPR(DMAC, DMAC3I) = priority;
            IEN(DMAC, DMAC3I) = 1;
        break;

        case DMACA_CH4:
        case DMACA_CH5:
        case DMACA_CH6:
        case DMACA_CH7:
            /* The interrupt number is DMAC74I. */
            if (IPR(DMAC, DMAC74I) < priority)
            {
                IPR(DMAC, DMAC74I) = priority;
            }
            IEN(DMAC, DMAC74I) = 1;
        break;

        default:
            /* The channel number is invalid. */
            return DMACA_ERR_INVALID_CH;
        break;
    }

    if (0x00 == IEN(DMAC, DMAC0I))
    {
        /* do nothing */
    }

    return DMACA_SUCCESS;
}

/*******************************************************************************
* Function Name: r_dmaca_int_disable
* Description  : Disables DMACmI interrupt. (m = 0 to 3, or 74)
* Arguments    : channel -
*                    Which channel to use
* Return Value : DMACA_SUCCESS -
*                    DMACmI interrupt is disabled successfully.
*                DMACA_ERR_INVALID_CH -
*                    Channel is invalid.
*******************************************************************************/
dmaca_return_t r_dmaca_int_disable(uint8_t channel)
{

    /* Clear the DMACmI interrupt Enable bit. */
    /* Clear the DMACmI priority level. */
    switch (channel)
    {
        case DMACA_CH0:
            IEN(DMAC, DMAC0I) = 0;
            IPR(DMAC, DMAC0I) = 0;
        break;

        case DMACA_CH1:
            IEN(DMAC, DMAC1I) = 0;
            IPR(DMAC, DMAC1I) = 0;
        break;

        case DMACA_CH2:
            IEN(DMAC, DMAC2I) = 0;
            IPR(DMAC, DMAC2I) = 0;
        break;

        case DMACA_CH3:
            IEN(DMAC, DMAC3I) = 0;
            IPR(DMAC, DMAC3I) = 0;
        break;

        case DMACA_CH4:
            IEN(DMAC, DMAC74I) = 0;
            IPR(DMAC, DMAC74I) = 0;
        break;

        case DMACA_CH5:
            IEN(DMAC, DMAC74I) = 0;
            IPR(DMAC, DMAC74I) = 0;
        break;

        case DMACA_CH6:
            IEN(DMAC, DMAC74I) = 0;
            IPR(DMAC, DMAC74I) = 0;
        break;

        case DMACA_CH7:
            IEN(DMAC, DMAC74I) = 0;
            IPR(DMAC, DMAC74I) = 0;
        break;

        default:
            /* The channel number is invalid. */
            return DMACA_ERR_INVALID_CH;
        break;
    }

    if (0x00 == IPR(DMAC, DMAC0I))
    {
        /* do nothing */
    }

    return DMACA_SUCCESS;
}

void initDmac0ForPdc(){

    g_data_dmac_ch0.des_addr_mode        = DMACA_DES_ADDR_INCR;
    g_data_dmac_ch0.src_addr_mode        = DMACA_SRC_ADDR_FIXED;
    g_data_dmac_ch0.des_addr_repeat_area = DMACA_DES_ADDR_EXT_REP_AREA_NONE;
    g_data_dmac_ch0.src_addr_repeat_area = DMACA_SRC_ADDR_EXT_REP_AREA_NONE;
    g_data_dmac_ch0.transfer_mode        = DMACA_TRANSFER_MODE_BLOCK;
    g_data_dmac_ch0.repeat_block_side    = DMACA_REPEAT_BLOCK_SOURCE;
    g_data_dmac_ch0.data_size            = DMACA_DATA_SIZE_LWORD;
    g_data_dmac_ch0.request_source       = DMACA_TRANSFER_REQUEST_PERIPHERAL;
    g_data_dmac_ch0.block_size           = 0x0008;
    g_data_dmac_ch0.transfer_count       = 0x4b00;
    g_data_dmac_ch0.p_src_addr           = R_PDC_GetFifoAddr();
    g_data_dmac_ch0.p_des_addr           = (uint8_t*)0x8000000;
    g_data_dmac_ch0.offset_value         = 0x00000001;
    g_data_dmac_ch0.dtie_request         = DMACA_TRANSFER_END_INTERRUPT_ENABLE;
    g_data_dmac_ch0.esie_request         = DMACA_TRANSFER_ESCAPE_END_INTERRUPT_DISABLE;
    g_data_dmac_ch0.rptie_request        = DMACA_REPEAT_SIZE_END_INTERRUPT_DISABLE;
    g_data_dmac_ch0.sarie_request        = DMACA_SRC_ADDR_EXT_REP_AREA_OVER_INTERRUPT_DISABLE;
    g_data_dmac_ch0.darie_request        = DMACA_DES_ADDR_EXT_REP_AREA_OVER_INTERRUPT_DISABLE;
    g_data_dmac_ch0.act_source           = IR_PDC_PCDFI;
    g_data_dmac_ch0.interrupt_sel        = DMACA_CLEAR_INTERRUPT_FLAG_BEGINNING_TRANSFER;

    startModule(MstpIdDMAC0);


}

void setDestAdrDmac0ForPdc( uint8_t * destadr )
{
    volatile dmaca_return_t      ret_dmac;
    dmaca_stat_t                 stat_dmac;

    g_data_dmac_ch0.p_des_addr           = destadr;
    ret_dmac = R_DMACA_Create(DMACA_CH0, &g_data_dmac_ch0);
    if (DMACA_SUCCESS != ret_dmac)
    {
        /* do something */
    }

    ret_dmac = R_DMACA_Int_Enable(DMACA_CH0, 8);
    if (DMACA_SUCCESS != ret_dmac)
    {
        /* do something */
    }
    ret_dmac = R_DMACA_Control(DMACA_CH0, DMACA_CMD_ENABLE, &stat_dmac);
    if (DMACA_SUCCESS != ret_dmac)
    {
        /* do something */
    }


}


/***********************************************************************************************************************
* Function Name: dmac0_end_int
* Description  : This function is a callback function when DMAC finishes transmission.
* Arguments    : none
* Return Value : none
***********************************************************************************************************************/
void int_excep_dmac_dmac0i( void )
{
    volatile dmaca_return_t      ret_dmac;
    dmaca_stat_t                 stat_dmac;

    ret_dmac = R_DMACA_Control(DMACA_CH0, DMACA_CMD_STATUS_GET, &stat_dmac);
    if (DMACA_SUCCESS != ret_dmac)
    {
    }
}

