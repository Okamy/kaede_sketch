

#ifndef Mdl_IP_prot
#define Mdl_IP_prot


// マクロ定義
#define MAX_AREA 9      // 分割エリア数
#define MAX_PERSON 5    // 人物検知 検知最大人数
#define MAX_MOVING 5    // 動体検知 検知最大物体数


// 人物検知情報構造体
typedef struct _PersonDetection_Info {
    int flg;            // 0:無し / 1:有り
    float score;        // 一致度
    int sx;             // 矩形始点X
    int sy;             // 矩形始点Y
    int ex;             // 矩形終点X
    int ey;             // 矩形終点Y
} PersonDetection_Info;



// 人物検知結果構造体
typedef struct _PersonDetection_Rslt {
    int p_dct_cnt[MAX_AREA];                        // エリア内にいる人数
    PersonDetection_Info p_dct_inf[MAX_PERSON];     // 検出情報
} PersonDetection_Rslt;




// 動体検知情報構造体
typedef struct _MovingDetection_Info {
    int flg;            // 0:無し / 1:有り
    int area;           // エリアNo
    int sx;             // 矩形始点X
    int sy;             // 矩形始点Y
    int ex;             // 矩形終点X
    int ey;             // 矩形終点Y
} MovingDetection_Info;


// 動体検知結果構造体
typedef struct _MovingDetection_Rslt {
    int p_dct_cnt;      // 検出物体数
    MovingDetection_Info p_dct_inf[MAX_MOVING];     // 検出情報
} MovingDetection_Rslt;






#endif  // Mdl_IP_prot

