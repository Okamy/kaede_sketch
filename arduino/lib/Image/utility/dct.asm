;	Copyright(C) 2008(2009) Renesas Technology Corp. and Renesas Solutions Corp., All rights reserved.
;
;	$Id: //depot/sh_jpeg/Source/1/dct.mar#2 $
;	$Date: 2010/01/31 $






			.section    P_jpeg_enc_F_8,  code, align= 8
			.glb	_R_jpeg_DCT
			
;	Parameter
;			unsigned char **pixels
;			int start_col
;			int qtbl
;			short int *outptr
;			struct _jpeg_working *wenv












_R_jpeg_DCT:
		pushm		r6-r13
		add			#-16,R0,R0
	
		mov.l       r4, 4[r0]		; outptr
		mov.l       0+16+32+4[r0], r4		; wenv

		mov.l       #8, r8
		mov.l       4[r4], r5		; encFMC


		mov.l       [r4], r9			; encFMB

		add         #64, r5, r13
		mov.l       r3, 0[r0]		; qtbl

		mov.l       r9, 8[r0]

		mov.l       R1, r12
		

;	Put 'nop' to word-align at the label 'dimesion1' if necessary
		nop
		nop
		nop

dimesion1:
		mov.l		[r12+], r6
		add			r2, r6
		movu.b		  [r6], r3
		movu.b		7[r6], r4
		movu.b		3[r6], r1
		movu.b		4[r6], r7
		add			#-128, r3
		add			#-128, r4
		add			#-128, r1
		add         #-128, r7
		add			r3, r4,r15
		sub			r4, r3
		shll         #16, r3			; to use MACHI
		add         r1, r7, r4
		sub         r7, r1


		add         r15, r4, r11
		sub         r4, r15
;
;	r3: r3 = r3
;	r1: r1 = r1
;	r7:	r15=r15
;	X6:	r11=r11





		movu.b      1[r6], r5
		movu.b      6[r6],r7
		add         #-128, r5
		add         #-128, r7
		add         r5,r7,r10
		sub         r7, r5
		




		movu.b      2[r6], r7
		movu.b      5[r6], r6

		add         #-128, r7
		add         #-128, r6
		add         r7,r6,r4
		sub         r6, r7
		shll         #16, r7			; to use MACHI




		add         r10, r4, r6
		sub         r4,r10
;	r7 r7=r5
;	r6 r10=r10


		add         r6,r11, r4
		sub         r6, r11
		shll         #2,r4
		shll         #2,r11
		mov.w       r4, [r9]		; 0*2
		mov.w       r11, 32*2[r9]



		mov.l		[r13+], r6
		mov.l		[r13+], r4
		mullo		r6, r5
		machi		r6, r3
		maclo		r4, r1
		machi		r4, r7
		mvfacmi		r6
		shar        #11, r6
		mov.w       r6, 8*2[r9]

		mov.l		[r13+], r6
		mov.l		[r13+], r4
		mullo		r6, r5
		machi		r6, r3
		maclo		r4, r1
		machi		r4, r7
		mvfacmi		r6
		shar        #11, r6
		mov.w       r6, 24*2[r9]

		mov.l		[r13+], r6
		mov.l		[r13+], r4
		mullo		r6, r5
		machi		r6, r3
		maclo		r4, r1
		machi		r4, r7
		mvfacmi		r6
		shar        #11, r6
		mov.w       r6, 40*2[r9]

		mov.l		[r13+], r6
		mov.l		[r13+], r4
		mullo		r6, r5
		machi		r6, r3
		maclo		r4, r1
		machi		r4, r7
		mvfacmi		r6
		shar        #11, r6
		mov.w       r6, 56*2[r9]

		mov.l		[r13+], r6
		mullo		r6, r10
		shlr			#16, r6
		maclo		r6, r15
		mvfacmi		r6
		shar        #11, r6
		mov.w       r6, 16*2[r9]

		mov.l		[r13+], r6
		mullo		r6, r10
		shlr			#16, r6
		maclo		r6, r15
		mvfacmi		r6
		shar        #11, r6
		mov.w       r6, 48*2[r9]
		add         #-40, r13

		add         #2,r9
		sub         #1, r8
		bne         dimesion1






;	r2 = free
;  r13=r13 is active
		mov.l       0[r0], r3
		mov.l       8[r0], r6
		shll        #7, r3
		add         #64*4, r6,r12	; _jpeg_DCTqtbl
		add         r3, r12


		mov.l		#_store_order, r9

		mov.l       #8, r8
		mov.l       4[R0], r14
		
;	Put 'nop' to word-align at the label 'dimesion1' if necessary
dimesion2:




		mov.w		  [r6], r3
		mov.w		7*2[r6], r4
		add			r3, r4,r15
		sub			r4, r3
		shll		#16, r3		; to use MACHI




		mov.w		3*2[r6], r1
		mov.w		4*2[r6], r7
		add         r1, r7, r4
		sub         r7, r1


		add         r15, r4, r11
		sub         r4, r15

;
;	r3: r3 = r3
;	r1: r1 = r1
;	r7:	r15=r15
;	X6:	r11=r11





		mov.w      1*2[r6], r5
		mov.w      6*2[r6],r7
		add         r5,r7,r10
		sub         r7, r5
		





		add         #16,r6,r4
		mov.w      2*2[r6], r7
		mov.w      5*2[r6], r6
;
		mov.l		r4, 12[r0]

;
		add         r7,r6,r4
		sub         r6, r7
		shll		#16, r7			; to use MACHI



		add         r10, r4, r6
		sub         r4,r10
;	r7 r7=r7
;	r6 r10=r10


		add         r6,r11, r4
		sub         r6, r11
		shar         #3,r4
		shar         #3,r11
		mov.w       [r12+], r6
		mullo		r6, r4
		mov.w       [r12+], r6
		racw		#1
		mvfachi		r4
		mullo		r6, r11
		racw		#1
		mvfachi		r11
		movu.b      [r9], r6
		add			r14, r6
		mov.w		r4, [r6]
		movu.b      4[r9], r6
		add         r14, r6
		mov.w       r11, [r6]




		
		add         #16*2, r13
		mov.l		[r13+], r6
		mullo		r6, r10
		shlr			#16, r6
		maclo		r6, r15
		mov.w       [r12+], r2
		mvfachi		r4
		mullo		r2, r4
		racw		#1
		mvfachi		r4
		movu.b      2[r9], r2
		mov.l		[r13+], r6
		add			r14, r2
		mov.w		r4, [r2]
		
		mullo		r6, r10
		shlr			#16, r6
		maclo		r6, r15
		mov.w       [r12+], r2
		mvfachi		r4
		mullo		r2, r4
		racw		#1
		mvfachi		r4
		movu.b      6[r9], r2
		add			#-40, r13
		add			r14, r2
		mov.w		r4, [r2]



		mov.l		[r13+], r6
		mov.l		[r13+], r4
		mullo		r6, r5
		machi		r6, r3
		maclo		r4, r1
		machi		r4, r7
;
		mov.w       [r12+], r4
		mvfachi		r6
		mullo		r4, r6
;
		racw		#1
		mvfachi		r4		; r4
		movu.b      1[r9], r2
		mov.l		[r13+], r6
		add			r14, r2
		mov.w		r4, [r2]
		
		mov.l		[r13+], r4
		mullo		r6, r5
		machi		r6, r3
		maclo		r4, r1
		machi		r4, r7
		mov.w       [r12+], r4
		mvfachi		r6
		mullo		r4, r6
		racw		#1
		mvfachi		r4
		movu.b      3[r9], r2
		mov.l		[r13+], r6
		add			r14, r2
		mov.w		r4, [r2]
		
		mov.l		[r13+], r4
		mullo		r6, r5
		machi		r6, r3
		maclo		r4, r1
		machi		r4, r7
		mov.w       [r12+], r4
		mvfachi		r6
		mullo		r4, r6
		racw		#1
		mvfachi		r4
		movu.b      5[r9], r2
		mov.l		[r13+], r6
		add			r14, r2
		mov.w		r4, [r2]

		mov.l		[r13+], r4
		mullo		r6, r5
		machi		r6, r3
		maclo		r4, r1
		machi		r4, r7
		mov.w       [r12+], r4
		mvfachi		r6
		mullo		r4, r6
		racw		#1
		mvfachi		r4
		movu.b      7[r9], r2
		add         #8, r9
		add			r14, r2
		mov.w		r4, [r2]

		mov.l		12[r0], r6
		add		#-32, r13
		sub         #1, r8
		bne         dimesion2

EXIT:
		rtsd	#16+32, r6-r13
			
            .section	C_jpeg_enc_F_1, ROMDATA
			.glb	_store_order
_store_order:
		.byte		0*2,  2*2,  3*2,  9*2, 10*2, 20*2, 21*2, 35*2, 1*2,  4*2,  8*2, 11*2, 19*2, 22*2, 34*2, 36*2
		.byte		5*2,  7*2, 12*2, 18*2, 23*2, 33*2, 37*2, 48*2, 6*2, 13*2, 17*2, 24*2, 32*2, 38*2, 47*2, 49*2
		.byte		14*2, 16*2, 25*2, 31*2, 39*2, 46*2, 50*2, 57*2, 15*2, 26*2, 30*2, 40*2, 45*2, 51*2, 56*2, 58*2
		.byte		27*2, 29*2, 41*2, 44*2, 52*2, 55*2, 59*2, 62*2, 28*2, 42*2, 43*2, 53*2, 54*2, 60*2, 61*2, 63*2

		.end
