/* Copyright (C) 2006-2013 Renesas Solutions Corp. */

#include "r_jpeg_development.h"


#pragma section _jpeg_enc_F

#ifdef GRKAEDE
#define STORE_IN_ZIGZAG
#define TABLE
#endif
#if defined(R32C)
typedef char	emitsize;
#else
typedef int32_t	emitsize;
#endif
int32_t emit_bits(uint32_t, emitsize, struct _jpeg_working *);

/*""FUNC COMMENT""*************************************************************
 * Outline     : Huffman encode
 *----------------------------------------------------------------------------
 * Include     : #include "SHjpeg.h"
 *                 : #include "inttype.h"
 *----------------------------------------------------------------------------
 * Declaration : int _jpeg_scan_one_block (short int *block, int last_dc_val, int dctbl, int actbl, int comp_index, struct _jpeg_working *wenv)
 *                    int _jpeg_encode_one_block (short int *block, int last_dc_val, int dctbl, int actbl, struct _jpeg_working *wenv)signed short lan_open(void);
 *----------------------------------------------------------------------------
 * Function    : execute Huffman encode of MCU
 *                 : scan mode count up code length at the same time
 *----------------------------------------------------------------------------
 * Argument    :  
 *        short int *block : pointer to MCU
 *        int last_dc_val : last DC coefficient value
 *        int dctbl : No. of  DC coefficient table
 *        int actbl : No. of  AC coefficient table
 *        int comp_index : index of components
 *        struct _jpeg_working *wenv: pointer to working area
 *---------------------------------------------------------------------------- 
 * Return Value: _JPEG_OK  : normal end
 *             : _JPEG_ERROR : error
 *----------------------------------------------------------------------------
 * Notice      :  C model code
 *""FUNC COMMENT END""*********************************************************/
int32_t
#if defined(SCAN_MODE)
R_jpeg_scan_one_block (int16_t *block, int last_dc_val, int dctbl, int actbl, int comp_index, struct _jpeg_working *wenv)
#else
R_jpeg_encode_one_block (int16_t *block, int32_t last_dc_val, int32_t dctbl, int32_t actbl, struct _jpeg_working *wenv)
#endif
{
	int temp, temp2;
	emitsize nbits;
	int k, r, i;
	uint32_t code;
	emitsize size;
	struct _jpeg_enc_FMC *FMCbase;
#if defined(SCAN_MODE)
	struct _jpeg_enc_FMB *FMBbase;
	int fsize;
#endif

	/* Encode the DC coefficient difference per section F.1.2.1 */
	temp = temp2 = block[0] - last_dc_val;

	if (temp < 0) {
		temp = -temp;		/* temp is abs value of input */
		/* For a negative input, want temp2 = bitwise complement of abs(input) */
		/* This code assumes we are on a two's complement machine */
		--temp2;
	}

	FMCbase = wenv->encFMC;
#if defined(TABLE)
	if ((temp >> 8) == 0) {
		nbits = _jpeg_val2nbits(FMCbase)[temp];
	} else {
		nbits = _jpeg_val2nbits(FMCbase)[temp>>8] + 8;
	}
#else
	/* Find the number of bits needed for the magnitude of the coefficient */
	nbits = 0;
	while (temp) {
		++nbits;
		temp >>= 1;
	}
#endif
	code = _jpeg_huffDC(FMCbase)[dctbl*16+nbits];
	size = code >> 24;
#if defined(SCAN_MODE)
	FMBbase = wenv->encFMB;
	_jpeg_dc_size(FMBbase)[comp_index] += (size + nbits);
#else
	code <<= 8;
	/* Emit the Huffman-coded symbol for the number of bits */
	if (emit_bits(code, size, wenv)!=_JPEG_OK) {
		return _JPEG_ERROR;
	}

	/* Emit that number of bits of the value, if positive, */
	/* or the complement of its magnitude, if negative. */
	if (nbits) {			/* emit_bits rejects calls with size 0 */
		if (emit_bits(((uint32_t) temp2 << (32-nbits)), nbits, wenv)!=_JPEG_OK) {
			return _JPEG_ERROR;
		}
	}
#endif

	/* Encode the AC coefficients per section F.1.2.2 */
	r = 0;			/* r = run length of zeros */
#if defined(SCAN_MODE)
	fsize = 0;
#endif  
	for (k=1; k<_JPEG_DCTSIZE2; k++) {
#if defined(STORE_IN_ZIGZAG)	/* fast version */
		temp = block[k];
#else
		temp = block[(int)(_jpeg_natural_order(FMCbase)[k])];
#endif
		if (temp == 0) {
			++r;
		} else {
			/* if run length > 15, must emit special run-length-16 codes (0xF0) */
			while (r > 15) {
				code = _jpeg_huffAC(FMCbase)[actbl*256+0xF0];
				size = code >> 24;
#if defined(SCAN_MODE)
				fsize += size;
#else
				code <<= 8;
				if (emit_bits(code, size, wenv)!=_JPEG_OK) {
					return _JPEG_ERROR;
				}
#endif
				r -= 16;
			}
			
			temp2 = temp;
			if (temp < 0) {
				temp = -temp;		/* temp is abs value of input */
				/* This code assumes we are on a two's complement machine */
				--temp2;
			}
      
#if defined(TABLE)
			if ((temp >> 8) == 0) {
					nbits = _jpeg_val2nbits(FMCbase)[temp];
			} else {
				nbits = _jpeg_val2nbits(FMCbase)[temp>>8] + 8;
			}
#else
			/* Find the number of bits needed for the magnitude of the coefficient */
			nbits = 1;		/* there must be at least one 1 bit */
			while ((temp >>= 1)) {
				++nbits;
			}
#endif
			/* Emit Huffman symbol for run length / number of bits */
			i = (r << 4) + nbits;
			code = _jpeg_huffAC(FMCbase)[actbl*256+i];
			size = code >> 24;
#if defined(SCAN_MODE)
			fsize += (size+nbits);
#else
			code <<= 8;
			if (emit_bits(code, size, wenv)!=_JPEG_OK) {
				return _JPEG_ERROR;
			}
			/* Emit that number of bits of the value, if positive, */
			/* or the complement of its magnitude, if negative. */
			if (emit_bits(((uint32_t)temp2 << (32-nbits)), nbits, wenv)!=_JPEG_OK) {
				return _JPEG_ERROR;
			}
#endif
			r = 0;
		}
	}
	
	/* If the last coef(s) were zero, emit an end-of-block code */
	if (r > 0) {
		code = _jpeg_huffAC(FMCbase)[actbl*256+0];
		size = code >> 24;
#if defined(SCAN_MODE)
		fsize += size;
#else
		code <<= 8;
		if (emit_bits(code, size, wenv)!=_JPEG_OK) {
			return _JPEG_ERROR;
		}
#endif
	}

#if defined(SCAN_MODE)
	_jpeg_ac_size(FMBbase)[comp_index] += fsize;
#endif
	
	return _JPEG_OK;
}


#if !defined(SCAN_MODE)
/*
    Emit some bits; return _JPEG_OK if successful, _JPEG_ERROR if must suspend
    This function is supposed to be implemented as a macro, 
    because symbol "emit_bits" is not global.
 */
/* This routine is heavily used, so it's worth coding tightly. */
int32_t emit_bits(uint32_t code, emitsize size, struct _jpeg_working *wenv)
{
	struct _jpeg_enc_FMB *FMBbase;

	FMBbase = wenv->encFMB;

	/* if size is 0, caller used an invalid Huffman table entry */
	if (size == 0) {
		return _JPEG_ERROR;
	}

	code >>= _jpeg_cur_put_bits(FMBbase);
	_jpeg_cur_put_bits(FMBbase) += size;
	_jpeg_cur_put_buffer(FMBbase) |= code;

	while (_jpeg_cur_put_bits(FMBbase) >= 8) {
		uint8_t c = (uint8_t) ((_jpeg_cur_put_buffer(FMBbase) >> 24) & 0xFF);

		WRITE_BYTE(c, wenv);
//		*_jpeg_next_output_byte(FMBbase) = c;
//		++_jpeg_next_output_byte(FMBbase);
//		--_jpeg_free_in_buffer(FMBbase);
//		if (_jpeg_free_in_buffer(FMBbase) == 0) {
//			(*(wenv->enc_dump_func))(wenv);
//		}
		if (c == (uint8_t)0xFF) {		/* need to stuff a zero byte? */
			WRITE_BYTE(0, wenv);
//			*_jpeg_next_output_byte(FMBbase) = (uint8_t)0;
//			++_jpeg_next_output_byte(FMBbase);
//			--_jpeg_free_in_buffer(FMBbase);
//			if (_jpeg_free_in_buffer(FMBbase) == 0) {
//				(*(wenv->enc_dump_func))(wenv);
//			}
		}
		_jpeg_cur_put_buffer(FMBbase) <<= 8;
		_jpeg_cur_put_bits(FMBbase) -= 8;
	}

	return _JPEG_OK;
}
#endif

/*
        Copyright (C) 2006 Renesas Technology Corp. All Rights Reserved
        AND Renesas Solutions Corp. All Rights Reserved
 */
