/* Copyright (C) 2006-2013 Renesas Solutions Corp. */

#include "r_jpeg_development.h"



#pragma section _jpeg_enc_S


void R_jpeg_add_quant_table (int32_t which_tbl, const uint16_t *basic_table, int32_t quality, struct _jpeg_working *wenv)
{
	natural_int_t i;
	int16_t *qp, *tmpp, *pw;
	int32_t *pco;
	uint16_t *uqp;
//	int16_t temp;
	uint16_t tempu;
	uint8_t *posp;
//	uint16_t prec = 0;
	int16_t scale_factor;
	struct _jpeg_enc_FMB *FMBbase;
//	struct _jpeg_enc_FMC *FMCbase;
	struct _jpeg_enc_SMB *SMBbase;
	struct _jpeg_enc_SMC *SMCbase;
	
	FMBbase = wenv->encFMB;
//	FMCbase = wenv->encFMC;
	SMBbase = wenv->encSMB;
	SMCbase = wenv->encSMC;
	++_jpeg_num_QTBL(SMBbase);
	uqp = _jpeg_QTBL(SMBbase) + _JPEG_DCTSIZE2 * which_tbl;
#if 0
	if (quality < 0) {
		for (i=_JPEG_DCTSIZE2, pw=(int16_t *)_jpeg_work(FMBbase), pco=(int32_t *)_jpeg_quant_coeff(SMCbase); i; --i) {
			temp = (int32_t) *uqp++;
			if (temp > 255)
				prec = 1;
			*pw++ = *pco++ / temp;
		}
		_jpeg_precision_QTBL(SMBbase)[which_tbl] = prec;
		tmpp = _jpeg_DCTqtbl(FMBbase) + _JPEG_DCTSIZE2 * which_tbl;
		for (i=_JPEG_DCTSIZE, pw=(int16_t *)_jpeg_work(FMBbase), posp=(uint8_t *)_jpeg_quant_pos(SMCbase); i ; --i) {
			qp = tmpp + (int32_t)*posp++;
			*qp = *pw++;
			*(qp+8) = *pw++;
			*(qp+16) = *pw++;
			*(qp+24) = *pw++;
			*(qp+32) = *pw++;
			*(qp+40) = *pw++;
			*(qp+48) = *pw++;
			*(qp+56) = *pw++;
		}
		return;
	}
#endif
/*
 * Convert a user-specified quality rating to a percentage scaling factor
 * for an underlying quantization table, using our recommended scaling curve.
 * The input 'quality' factor should be 0 (terrible) to 128 (very good).
 */
	/* Safety limit on quality factor.  Convert 0 to 1 to avoid zero divide. */
	if (quality <= 0) {
		quality = 1;
	}
	if (quality > 128) {
		quality = 128;
	}

/*
 * The basic table is used as-is (scaling 128) for a quality of 64.
 * Qualities 64..128 are converted to scaling percentage 256 - 2*Q;
 * note that at Q=128 the scaling is 0, which will cause _jpeg_add_quant_table
 * to make all the table entries 1 (hence, minimum quantization loss).
 * Qualities 1..64 are converted to scaling percentage 8192/Q.
 */
	if (quality < 64) {
		scale_factor = 8192 / quality;
	} else {
		scale_factor = 256 - quality * 2;
	}

	for (i=0, pw=(int16_t *)_jpeg_work(FMBbase), pco=(int32_t *)_jpeg_quant_coeff(SMCbase); i<_JPEG_DCTSIZE2; ++i) {
		tempu = ((uint16_t) basic_table[i] * scale_factor + 64) / 128;
     
		if (tempu <= 0)	 {
			tempu = 1;
		}
//		if (tempu > 32767) {
//			tempu = 32767; 
//		}
		if (tempu > 255) {
			tempu = 255;
		}
//		if (tempu > 255) {
//			prec = 1;
//		}
		*uqp++ = tempu;
		*pw++ = (*pco++ / tempu) >> 16;
	}

//	_jpeg_precision_QTBL(SMBbase)[which_tbl] = prec;
	_jpeg_precision_QTBL(SMBbase)[which_tbl] = 0;
	tmpp = _jpeg_DCTqtbl(FMBbase) + _JPEG_DCTSIZE2 * which_tbl;
	for (i=_JPEG_DCTSIZE, pw=(int16_t *)_jpeg_work(FMBbase), posp=(uint8_t *)_jpeg_quant_pos(SMCbase); i; --i) {
		qp = tmpp + *posp++;
		*qp = *pw++;
		*(qp+8) = *pw++;
		*(qp+16) = *pw++;
		*(qp+24) = *pw++;
		*(qp+32) = *pw++;
		*(qp+40) = *pw++;
		*(qp+48) = *pw++;
		*(qp+56) = *pw++;
	}
}
