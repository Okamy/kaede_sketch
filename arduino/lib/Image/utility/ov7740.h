/*
 * ov7740.h
 *
 *  Created on: 2015/08/30
 *      Author: a5034000
 */

#ifndef OV7740_H_
#define OV7740_H_

#define IMG_SENSOR_WIDTH   640
#define IMG_SENSOR_HEIGHT  480

#ifndef OV7740CONFIG_INITIAL
#define OV7740CONFIG_EXTERN extern
#else
#define OV7740CONFIG_EXTERN
#endif

/*******************************************************************************
Includes   <System Includes> , "Project Includes"
*******************************************************************************/

/******************************************************************************
Macro definitions
******************************************************************************/

/*******************************************************************************
Typedef definitions
*******************************************************************************/
#ifdef USE_IMG_PROC
#define     IMPARAMETER_MODE        0x00 // VGA YCbCr422
#else
#define     IMPARAMETER_MODE        0x01 // QVGA YCbCr422
#endif

/******************************************************************************
Exported global variables
******************************************************************************/
OV7740CONFIG_EXTERN const unsigned char change_reg[ 0xFF ][ 2 ] =
{
#if  IMPARAMETER_MODE==0x00
// [OV7740] VGA input -> VGA output (YUV:422)
    {0x80, 0x7f}, // [6]: black pixel correction
                  // [5]: white pixel correction
                  // [4]: AWB
                  // [3]: gamma
                  // [2]: AWB gain
                  // [1]: LENC
                  // [0]: ISP
    {0x81, 0x3f}, // [5]: SDE
                  // [4]: UV adjust
                  // [3]: YUV444to422
                  // [2]: UV average
                  // [1]: CMX
                  // [0]: CIP
    {0x12, 0x80}, // [7]: software reset
    {0x13, 0xff}, // [7]: Faster AEC correction
                  // [6]: frame drop function
                  // [5]: banding filter
                  // [4]: AEC below banding value
                  // [3]: Tp level exposure
                  // [2]: AGC auto
                  // [1]: AWB control
                  // [0]: Exposure auto
    {0x11, 0x01}, // [5..0]: Clock divider (1/(N+1))
    {0x55, 0x42}, // reserved
    {0x12, 0x00}, // [6]: vertical skip mode
                  // [5]: CC656 mode
                  // [0]: output raw data RGB mode
    {0xd5, 0x10}, // [4]: zoom out mode
    {0x0c, 0xd2}, // [7]: vertical flip
                  // [6]: mirror
                  // [4]: (0:UYVY)  (1:YUYV)
                  // [3]: 0 : [Y9,Y8,..,Y0]
                  // [2..1]: Max expoture (frame length - N*2)
                  // [0]: array color bar
    {0x0d, 0x34}, // reserved

//  mirror_en -> must be adjusted
    {0x16, 0x01}, // [1:0]AHSTART : Sensor Horizontal Output Start Point [1:0]
                  // [2]AHSTART  : Sensor Vertical Output Start Point
                  // [4:3]AHSIZE : Sensor Horizontal Output Size
                  // [5]AVSIZE   : Sensor Vertical Output Size
    {0x17, 0x24}, // AHSTART : Sensor Horizontal Output Start Point [9..2]

    {0x18, 0xa0}, // AHSIZE : Sensor Horizontal Output Size [9..2]
    {0x19, 0x03}, // AVSTART : Sensor Vertical Output Start Point [9..2]
    {0x1a, 0xf0}, // AVSIZE : Sensor Vertical Output Size [9..2]
    {0x1b, 0x85}, // PSHFT : pixel shift
    {0x1e, 0x13}, // [0] : AEC max increasing step has no limit
    {0x22, 0x03}, // reserved
    {0x29, 0x17}, // Horizontal tp counter end point LSB
    {0x2b, 0xf8}, // row counter end point LSB
    {0x2c, 0x01}, // row counter end point MSB
    {0x33, 0xc4}, // reserved
    {0x3a, 0xb4}, // reserved
    {0x36, 0x3f}, // reserved
    {0x04, 0x60}, // reserved
    {0x27, 0x80}, // [7]: black sun cancellation
    {0x3d, 0x0f}, // reserved
    {0x3e, 0x82}, // reserved
    {0x3f, 0x40}, // reserved
    {0x40, 0x7f}, // reserved
    {0x41, 0x6a}, // reserved
    {0x42, 0x29}, // reserved
    {0x44, 0xe5}, // reserved
    {0x45, 0x41}, // reserved
    {0x47, 0x42}, // reserved
    {0x48, 0x00}, // reserved
    {0x49, 0x61}, // reserved
    {0x4a, 0xa1}, // reserved
    {0x4b, 0x46}, // reserved
    {0x4c, 0x18}, // reserved
    {0x4d, 0x50}, // reserved
    {0x4e, 0x13}, // reserved
    {0x64, 0x00}, // reserved
    {0x67, 0x88}, // [5..0]: BLC target
    {0x68, 0x1a}, // reserved
    {0x14, 0x38}, // [6..4] AGC gain ceiling x2^(N+1)
    {0x24, 0x3c}, // WPT : luminance signal high range for AEC/AGC operation
    {0x25, 0x30}, // BPT : luminance signal low range for AEC/AGC operation
    {0x26, 0x72}, // VPT : effective only AEC/AGC fast mode
    {0x50, 0x97}, // LSBs of banding starting step for 50[Hz] light source
    {0x51, 0x7e}, // LSBs of banding starting step for 60[Hz] light source
    {0x52, 0x00}, // [7..6]: MSBs of banding starting step for 60[Hz] light source
                  // [5..4]: MSBs of banding starting step for 50[Hz] light source
    {0x53, 0x00}, // reserved
    {0x20, 0x00}, // reserved
    {0x21, 0x23}, // reserved
    {0x38, 0x11}, // [3..0]: Monitor[3:0] I2C registers sub address control bits
    {0xe9, 0x00}, // YAVG BLK THRE : threshold for blackness
    {0x56, 0x55}, // 16 zone Y average select 00:not selected,01:x1,10:x2,11:x4
                  // [7..6]/[5..4]/[3..2]/[1..0] each zone(4�`1)
    {0x57, 0xff}, // [7..6]/[5..4]/[3..2]/[1..0] each zone(8�`5)
    {0x58, 0xff}, // [7..6]/[5..4]/[3..2]/[1..0] each zone(12�`9)
    {0x59, 0xff}, // [7..6]/[5..4]/[3..2]/[1..0] each zone(16�`13)
    {0x5f, 0x04}, // reserved
    {0x13, 0xff}, // [7]: Faster AEC correction
                  // [6]: frame drop function
                  // [5]: banding filter
                  // [4]: AEC below banding value
                  // [3]: Tp level exposure
                  // [2]: AGC auto
                  // [1]: AWB control
                  // [0]: Exposure auto
    {0x80, 0x7f}, // [6]: black pixel correction
                  // [5]: white pixel correction
                  // [4]: AWB
                  // [3]: gamma
                  // [2]: AWB gain
                  // [1]: LENC
                  // [0]: ISP
    {0x81, 0x3f}, // [5]: SDE
                  // [4]: UV adjust
                  // [3]: YUV444to422
                  // [2]: UV average
                  // [1]: CMX
                  // [0]: CIP
    {0x83, 0x03}, // [5]: 0:normal output/1:no output
                  // [4]: Y average inputs 0:RAW/1:YUV422
                  // [3]: Y average after (0:AWB gain,1:WBC)
                  // [2]: RAW after (0:WBC/1:cip raw)
                  // [1]: AWB2 after (0:gamma input/1:gamma output)
                  // [0]: First line (0:GR/1:BG)
    {0x38, 0x11}, // [3..0]: Monitor[3:0] I2C registers sub address control bits
    {0x84, 0x70}, // [7]: CMX bias
                  // [6]: gamma bias
                  // [5]: AWB bias
                  // [4]: LENC bias
                  // [3]: CMX offset
                  // [2]: gamma offset
                  // [1]: AWB offset
                  // [0]: LEC offset
    {0x85, 0x00}, // AGC OFFSET
    {0x86, 0x03}, // [4..0]: base 1 for y_edge_mt auto value
    {0x87, 0x01}, // [4..0]: base 2 for y_edge_mt auto value
    {0x88, 0x05}, // [6]: gain sel enable
                  // [5..4]: gain sel
                  // [3..2]: gain for DNS threshold
                  // [1..0]: value for edge enhancement
    {0x89, 0x30}, // [5]: LENC bias plus
                  // [4]: enable data round
                  // [3..2]: vertical skip
                  // [1..0]: horizontal skip
    {0x8d, 0x40}, // [6..0]: LENC RED A1
    {0x8e, 0x00}, // [7..0]: LENC RED B1
    {0x8f, 0x33}, // [7..4]: LENC RED B2
                  // [3..0]: LENC RED A2
    {0x93, 0x28}, // [6..0]: LENC GREEN A1
    {0x94, 0x20}, // [7..0]: LENC GREEN B1
    {0x95, 0x33}, // [7..4]: LENC GREEN B2
                  // [3..0]: LENC GREEN A2
    {0x99, 0x30}, // [6..0]: LENC BLUE A1
    {0x9a, 0x14}, // [7..0]: LENC BLUE B1
    {0x9b, 0x33}, // [7..4]: LENC BLUE B2
                  // [3..0]: LENC BLUE A2
    {0x9c, 0x08}, // gamma curve YS1
    {0x9d, 0x12}, // gamma curve YS2
    {0x9e, 0x23}, // gamma curve YS3
    {0x9f, 0x45}, // gamma curve YS4
    {0xa0, 0x55}, // gamma curve YS5
    {0xa1, 0x64}, // gamma curve YS6
    {0xa2, 0x72}, // gamma curve YS7
    {0xa3, 0x7f}, // gamma curve YS8
    {0xa4, 0x8b}, // gamma curve YS9
    {0xa5, 0x95}, // gamma curve YS10
    {0xa6, 0xa7}, // gamma curve YS11
    {0xa7, 0xb5}, // gamma curve YS12
    {0xa8, 0xcb}, // gamma curve YS13
    {0xa9, 0xdd}, // gamma curve YS14
    {0xaa, 0xec}, // gamma curve YS15
    {0xab, 0x1a}, // slope in gamma curve YSLP15
    {0xce, 0x78}, // Color Matrix CMX M1
    {0xcf, 0x6e}, // Color Matrix CMX M2
    {0xd0, 0x0a}, // Color Matrix CMX M3
    {0xd1, 0x0c}, // Color Matrix CMX M4
    {0xd2, 0x84}, // Color Matrix CMX M5
    {0xd3, 0x90}, // Color Matrix CMX M6
    {0xd4, 0x1e}, // [6]: Color Matrix db
                  // [5..0]: Sign for Color Matrix
    {0x5a, 0x24}, // reserved
    {0x5b, 0x1f}, // reserved
    {0x5c, 0x88}, // reserved
    {0x5d, 0x60}, // reserved
    {0xac, 0x6e}, // reserved
    {0xbe, 0xff}, // reserved
    {0xbf, 0x00}, // reserved
    {0x70, 0x00}, // reserved
    {0x71, 0x34}, // reserved
    {0x74, 0x28}, // reserved
    {0x75, 0x98}, // reserved
    {0x76, 0x00}, // reserved
    {0x77, 0x08}, // reserved
    {0x78, 0x01}, // reserved
    {0x79, 0xc2}, // reserved
    {0x7d, 0x02}, // reserved
    {0x7a, 0x9c}, // reserved
    {0x7b, 0x40}, // reserved
    {0xec, 0x02}, // [7]: banding filter for 50Hz (0:manual/1:auto)
                  // [6]: banding filter step for (0:60Hz/1:50Hz)
    {0x7c, 0x0c}, // reserved
    {0x69, 0x08}, // [3]: auto BLC will be initiated for 64 frames
    {0x69, 0x00},
//VGA input -> VGA output
    {0x31, 0xa0}, // HOUTSIZE : DSP H output size 8 MSB [9..2] 0xA0*4=0x280(640)
    {0x32, 0xf0}, // VOUTSIZE : DSP V output size 8 MSB [8..1] 0xF0*2=0x1E0(480)
                  // LSB : refer to address 0x34.(default:0)
    {0x82, 0x32}, // [5]: Y average
                  // [4]: FIFO enable
                  // [3]: vertical scale
                  // [2]: horizontal scale
                  // [1]: window cropping
                  // [0]: smooth scale

//add auto adjust function off
    {0x13, 0xf8}, // [7]: Faster AEC correction
                  // [6]: frame drop function
                  // [5]: banding filter
                  // [4]: AEC below banding value
                  // [3]: Tp level exposure
                  // [2]: AGC auto
                  // [1]: AWB control
                  // [0]: Exposure auto

    { 0xFF, 0xFF } // termination

#else

    {0x80, 0x7f}, // [6]: black pixel correction
                  // [5]: white pixel correction
                  // [4]: AWB
                  // [3]: gamma
                  // [2]: AWB gain
                  // [1]: LENC
                  // [0]: ISP
    {0x81, 0x3f}, // [5]: SDE
                  // [4]: UV adjust
                  // [3]: YUV444to422
                  // [2]: UV average
                  // [1]: CMX
    // [0]: CIP
    {0x12, 0x80}, // [7]: software reset
    {0x13, 0xff}, // [7]: Faster AEC correction
                  // [6]: frame drop function
                  // [5]: banding filter
                  // [4]: AEC below banding value
                  // [3]: Tp level exposure
                  // [2]: AGC auto
                  // [1]: AWB control
                   // [0]: Exposure auto
    {0x11, 0x01}, // [5..0]: Clock divider (1/(N+1))
    {0x55, 0x42}, // reserved
    {0x12, 0x00}, // [6]: vertical skip mode
                  // [5]: CC656 mode
                  // [0]: output raw data RGB mode
    {0xd5, 0x10}, // [4]: zoom out mode

//  reg 0x0c data 0x12->0xc2 [4]0:UYVY [6]1:mirror_en [7]1:flip_en
    {0x0c, 0xc2}, // [7]: vertical flip
                  // [6]: mirror
                  // [4]: (0:UYVY)  (1:YUYV)
                  // [3]: 0 : [Y9,Y8,..,Y0]
                  // [2..1]: Max expoture (frame length - N*2)
                  // [0]: array color bar
    {0x0d, 0x34}, // reserved

//  mirror_en -> must be adjusted
    {0x16, 0x01}, // [1:0]AHSTART : Sensor Horizontal Output Start Point [1:0]
                  // [2]AHSTART  : Sensor Vertical Output Start Point
                  // [4:3]AHSIZE : Sensor Horizontal Output Size
                  // [5]AVSIZE   : Sensor Vertical Output Size
    {0x17, 0x4c}, // AHSTART : Sensor Horizontal Output Start Point [9..2]

    {0x18, 0x50}, // AHSIZE : Sensor Horizontal Output Size [9..2]
    {0x19, 0x53}, // AVSTART : Sensor Vertical Output Start Point [8..1]
    {0x1a, 0x78}, // AVSIZE : Sensor Vertical Output Size [9.1]
    {0x1b, 0x85}, // PSHFT : pixel shift
    {0x1e, 0x13}, // [0] : AEC max increasing step has no limit
    {0x22, 0x03}, // reserved
    {0x29, 0x17}, // Horizontal tp counter end point LSB
    {0x2b, 0xf8}, // row counter end point LSB
    {0x2c, 0x01}, // row counter end point MSB
    {0x31, 0x50}, // HOUTSIZE : DSP H output size 8 MSB [9..2] 0x50*4=0x140(320)
    {0x32, 0x78}, // VOUTSIZE : DSP V output size 8 MSB [8..1] 0x78*2=0xF0(240)
                  // LSB : refer to address 0x34.(default:0)
    {0x33, 0xc4}, // reserved
    {0x3a, 0xb4}, // reserved
    {0x36, 0x3f}, // reserved
    {0x04, 0x60}, // reserved
    {0x27, 0x80}, // [7]: black sun cancellation
    {0x3d, 0x0f}, // reserved
    {0x3e, 0x82}, // reserved
    {0x3f, 0x40}, // reserved
    {0x40, 0x7f}, // reserved
    {0x41, 0x6a}, // reserved
    {0x42, 0x29}, // reserved
    {0x44, 0xe5}, // reserved
    {0x45, 0x41}, // reserved
    {0x47, 0x42}, // reserved
    {0x48, 0x00}, // reserved
    {0x49, 0x61}, // reserved
    {0x4a, 0xa1}, // reserved
    {0x4b, 0x46}, // reserved
    {0x4c, 0x18}, // reserved
    {0x4d, 0x50}, // reserved
    {0x4e, 0x13}, // reserved
    {0x64, 0x00}, // reserved
    {0x67, 0x88}, // [5..0]: BLC target
    {0x68, 0x1a}, // reserved
    {0x14, 0x38}, // [6..4] AGC gain ceiling x2^(N+1)
    {0x24, 0x3c}, // WPT : luminance signal high range for AEC/AGC operation
    {0x25, 0x30}, // BPT : luminance signal low range for AEC/AGC operation
    {0x26, 0x72}, // VPT : effective only AEC/AGC fast mode
    {0x50, 0x97}, // LSBs of banding starting step for 50[Hz] light source
    {0x51, 0x7e}, // LSBs of banding starting step for 60[Hz] light source
    {0x52, 0x00}, // [7..6]: MSBs of banding starting step for 60[Hz] light source
                  // [5..4]: MSBs of banding starting step for 50[Hz] light source
    {0x53, 0x00}, // reserved
    {0x20, 0x00}, // reserved
    {0x21, 0x23}, // reserved
    {0x38, 0x11}, // [3..0]: Monitor[3:0] I2C registers sub address control bits
    {0xe9, 0x00}, // YAVG BLK THRE : threshold for blackness
    {0x56, 0x55}, // 16 zone Y average select 00:not selected,01:x1,10:x2,11:x4
                  // [7..6]/[5..4]/[3..2]/[1..0] each zone(4�`1)
    {0x57, 0xff}, // [7..6]/[5..4]/[3..2]/[1..0] each zone(8�`5)
    {0x58, 0xff}, // [7..6]/[5..4]/[3..2]/[1..0] each zone(12�`9)
    {0x59, 0xff}, // [7..6]/[5..4]/[3..2]/[1..0] each zone(16�`13)
    {0x5f, 0x04}, // reserved
    {0x13, 0xff}, // [7]: Faster AEC correction
                  // [6]: frame drop function
                  // [5]: banding filter
                  // [4]: AEC below banding value
                  // [3]: Tp level exposure
                  // [2]: AGC auto
                  // [1]: AWB control
                  // [0]: Exposure auto
    {0x80, 0x7f}, // [6]: black pixel correction
                  // [5]: white pixel correction
                  // [4]: AWB
                  // [3]: gamma
                  // [2]: AWB gain
                  // [1]: LENC
                  // [0]: ISP
    {0x81, 0x3f}, // [5]: SDE
                  // [4]: UV adjust
                  // [3]: YUV444to422
                  // [2]: UV average
                  // [1]: CMX
                  // [0]: CIP
    {0x82, 0x3e}, // [5]: Y average
                  // [4]: FIFO enable
                  // [3]: vertical scale
                  // [2]: horizontal scale
                  // [1]: window cropping
                  // [0]: smooth scale
    {0x83, 0x03}, // [5]: 0:normal output/1:no output
                  // [4]: Y average inputs 0:RAW/1:YUV422
                  // [3]: Y average after (0:AWB gain,1:WBC)
                  // [2]: RAW after (0:WBC/1:cip raw)
                  // [1]: AWB2 after (0:gamma input/1:gamma output)
                  // [0]: First line (0:GR/1:BG)
    {0x38, 0x11}, // [3..0]: Monitor[3:0] I2C registers sub address control bits
    {0x84, 0x70}, // [7]: CMX bias
                  // [6]: gamma bias
                  // [5]: AWB bias
                  // [4]: LENC bias
                  // [3]: CMX offset
                  // [2]: gamma offset
                  // [1]: AWB offset
                  // [0]: LEC offset
    {0x85, 0x00}, // AGC OFFSET
    {0x86, 0x03}, // [4..0]: base 1 for y_edge_mt auto value
    {0x87, 0x01}, // [4..0]: base 2 for y_edge_mt auto value
    {0x88, 0x05}, // [6]: gain sel enable
                  // [5..4]: gain sel
                  // [3..2]: gain for DNS threshold
                  // [1..0]: value for edge enhancement
    {0x89, 0x30}, // [5]: LENC bias plus
                  // [4]: enable data round
                  // [3..2]: vertical skip
                  // [1..0]: horizontal skip
    {0x8d, 0x40}, // [6..0]: LENC RED A1
    {0x8e, 0x00}, // [7..0]: LENC RED B1
    {0x8f, 0x33}, // [7..4]: LENC RED B2
                  // [3..0]: LENC RED A2
    {0x93, 0x28}, // [6..0]: LENC GREEN A1
    {0x94, 0x20}, // [7..0]: LENC GREEN B1
    {0x95, 0x33}, // [7..4]: LENC GREEN B2
                  // [3..0]: LENC GREEN A2
    {0x99, 0x30}, // [6..0]: LENC BLUE A1
    {0x9a, 0x14}, // [7..0]: LENC BLUE B1
    {0x9b, 0x33}, // [7..4]: LENC BLUE B2
                  // [3..0]: LENC BLUE A2
    {0x9c, 0x08}, // gamma curve YS1
    {0x9d, 0x12}, // gamma curve YS2
    {0x9e, 0x23}, // gamma curve YS3
    {0x9f, 0x45}, // gamma curve YS4
    {0xa0, 0x55}, // gamma curve YS5
    {0xa1, 0x64}, // gamma curve YS6
    {0xa2, 0x72}, // gamma curve YS7
    {0xa3, 0x7f}, // gamma curve YS8
    {0xa4, 0x8b}, // gamma curve YS9
    {0xa5, 0x95}, // gamma curve YS10
    {0xa6, 0xa7}, // gamma curve YS11
    {0xa7, 0xb5}, // gamma curve YS12
    {0xa8, 0xcb}, // gamma curve YS13
    {0xa9, 0xdd}, // gamma curve YS14
    {0xaa, 0xec}, // gamma curve YS15
    {0xab, 0x1a}, // slope in gamma curve YSLP15
    {0xce, 0x78}, // Color Matrix CMX M1
    {0xcf, 0x6e}, // Color Matrix CMX M2
    {0xd0, 0x0a}, // Color Matrix CMX M3
    {0xd1, 0x0c}, // Color Matrix CMX M4
    {0xd2, 0x84}, // Color Matrix CMX M5
    {0xd3, 0x90}, // Color Matrix CMX M6
    {0xd4, 0x1e}, // [6]: Color Matrix db
                  // [5..0]: Sign for Color Matrix
    {0x5a, 0x24}, // reserved
    {0x5b, 0x1f}, // reserved
    {0x5c, 0x88}, // reserved
    {0x5d, 0x60}, // reserved
    {0xac, 0x6e}, // reserved
    {0xbe, 0xff}, // reserved
    {0xbf, 0x00}, // reserved
    {0x70, 0x00}, // reserved
    {0x71, 0x34}, // reserved
    {0x74, 0x28}, // reserved
    {0x75, 0x98}, // reserved
    {0x76, 0x00}, // reserved
    {0x77, 0x08}, // reserved
    {0x78, 0x01}, // reserved
    {0x79, 0xc2}, // reserved
    {0x7d, 0x02}, // reserved
    {0x7a, 0x9c}, // reserved
    {0x7b, 0x40}, // reserved
    {0xec, 0x02}, // [7]: banding filter for 50Hz (0:manual/1:auto)
                  // [6]: banding filter step for (0:60Hz/1:50Hz)
    {0x7c, 0x0c}, // reserved
    {0x69, 0x08}, // [3]: auto BLC will be initiated for 64 frames
    {0x69, 0x00},

//add auto adjust function off
    {0x13, 0xf8}, // [7]: Faster AEC correction
                  // [6]: frame drop function
                  // [5]: banding filter
                  // [4]: AEC below banding value
                  // [3]: Tp level exposure
                  // [2]: AGC auto
                  // [1]: AWB control
                  // [0]: Exposure auto

    { 0xFF, 0xFF } // termination

#endif
};



#endif /* OV7740_H_ */
