/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/
/***********************************************************************************************************************
* File Name	   : r_readSampling.c
* Description  :
***********************************************************************************************************************/
/**********************************************************************************************************************
* History : DD.MM.YYYY Version  Description
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include "r_jpeg.h"
#include "r_compress_jpege.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define GET_R(a)	((a >> 8) & 0xF8)
#define GET_G(a)	((a >> 3) & 0xFC)
#define GET_B(a)	((a << 3) & 0xF8)
#define RGB565_to_RGB888(b, a)	\
	a[0] = GET_R(b);	\
	a[1] = GET_G(b);	\
	a[2] = GET_B(b);

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Exported global variables (to be accessed by other files)
***********************************************************************************************************************/
extern uint8_t Y_buf[4*_JPEG_DCTSIZE2];
extern uint8_t Cb_buf[_JPEG_DCTSIZE2];
extern uint8_t Cr_buf[_JPEG_DCTSIZE2];

extern void r_rgb2ycc(const uint8_t *rgb, uint8_t *ycc);

/***********************************************************************************************************************
Private global variables and functions
***********************************************************************************************************************/

#ifndef GRKAEDE
#pragma section _jpeg_cmp_F
#endif
/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
/* [RGB888] -> YY..., Cb..., Cr... */
int16_t readSamplingMCU_RGB888_YCbCr422(const uint8_t *read_pt, uint16_t line_byte)
{
	natural_int_t i, j;
	const uint8_t *line;
	uint8_t ycbcr0[3], ycbcr1[3];
	uint8_t *Y_pt, *Cb_pt, *Cr_pt;

	for (i = 0; i < _JPEG_DCTSIZE; i++)
	{
		line = read_pt;
		read_pt += line_byte;

		Y_pt = &Y_buf[i*(_JPEG_DCTSIZE*2)];
		Cb_pt = &Cb_buf[i*(_JPEG_DCTSIZE)];
		Cr_pt = &Cr_buf[i*(_JPEG_DCTSIZE)];

		for (j = 0; j < (_JPEG_DCTSIZE*2); j += 2)
		{
			r_rgb2ycc(line, ycbcr0);
			line += 3;
			r_rgb2ycc(line, ycbcr1);
			line += 3;
			*Y_pt++ = ycbcr0[0];
			*Y_pt++ = ycbcr1[0];
			*Cb_pt++ = (ycbcr0[1] + ycbcr1[1] + 1) / 2;
			*Cr_pt++ = (ycbcr0[2] + ycbcr0[2] + 1) / 2;
		}
	}
	return COMPRESS_JPEGE_OK;
}

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
/* [RGB888] -> YYYY..., Cb..., Cr... */
int16_t readSamplingMCU_RGB888_YCbCr420(const uint8_t *read_pt, uint16_t line_byte)
{
	natural_int_t i, j;
	const uint8_t *line1;
	const uint8_t *line2;
	uint8_t ycbcr0[3], ycbcr1[3];
	uint8_t ycbcr2[3], ycbcr3[3];
	uint8_t *Y_pt1, *Y_pt2, *Cb_pt, *Cr_pt;

	for (i = 0; i < _JPEG_DCTSIZE; i++)
	{
		line1 = read_pt;				/* odd line */
		read_pt += line_byte;
		line2 = read_pt;				/* even line */
		read_pt += line_byte;

		Y_pt1 = &Y_buf[(i*2)*(_JPEG_DCTSIZE*2)];
		Y_pt2 = &Y_buf[(i*2+1)*(_JPEG_DCTSIZE*2)];
		Cb_pt = &Cb_buf[i*(_JPEG_DCTSIZE)];
		Cr_pt = &Cr_buf[i*(_JPEG_DCTSIZE)];

		for (j = 0; j < (_JPEG_DCTSIZE*2); j += 2)
		{
			r_rgb2ycc(line1, ycbcr0);
			line1 += 3;
			r_rgb2ycc(line1, ycbcr1);
			line1 += 3;

			r_rgb2ycc(line2, ycbcr2);
			line2 += 3;
			r_rgb2ycc(line2, ycbcr3);
			line2 += 3;

			*Y_pt1++ = ycbcr0[0];
			*Y_pt1++ = ycbcr1[0];
			*Y_pt2++ = ycbcr2[0];
			*Y_pt2++ = ycbcr3[0];
			*Cb_pt++ = (ycbcr0[1] + ycbcr1[1] + ycbcr2[1] + ycbcr3[1] + 2) / 4;
			*Cr_pt++ = (ycbcr0[2] + ycbcr1[2] + ycbcr2[2] + ycbcr3[2] + 2) / 4;
		}
	}
	return COMPRESS_JPEGE_OK;
}

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
/* [RGB565] -> YY..., Cb..., Cr... */
int16_t readSamplingMCU_RGB565_YCbCr422(const uint8_t *read_pt, uint16_t line_byte)
{
	natural_int_t i, j;
	const uint16_t *line;
	uint8_t ycbcr0[3], ycbcr1[3];
	uint8_t *Y_pt, *Cb_pt, *Cr_pt;
	uint16_t rgb565;
	uint8_t rgb888[3];

	for (i = 0; i < _JPEG_DCTSIZE; i++)
	{
		line = (const uint16_t *)read_pt;
		read_pt += line_byte;

		Y_pt = &Y_buf[i*(_JPEG_DCTSIZE*2)];
		Cb_pt = &Cb_buf[i*(_JPEG_DCTSIZE)];
		Cr_pt = &Cr_buf[i*(_JPEG_DCTSIZE)];

		for (j = 0; j < (_JPEG_DCTSIZE*2); j += 2)
		{
			rgb565 = *line++;
			RGB565_to_RGB888(rgb565, rgb888);
			r_rgb2ycc(rgb888, ycbcr0);
			rgb565 = *line++;
			RGB565_to_RGB888(rgb565, rgb888);
			r_rgb2ycc(rgb888, ycbcr1);

			*Y_pt++ = ycbcr0[0];
			*Y_pt++ = ycbcr1[0];
			*Cb_pt++ = (ycbcr0[1] + ycbcr1[1] + 1) / 2;
			*Cr_pt++ = (ycbcr0[2] + ycbcr0[2] + 1) / 2;
		}
	}
	return COMPRESS_JPEGE_OK;
}

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
/* [RGB565] -> YYYY..., Cb..., Cr... */
int16_t readSamplingMCU_RGB565_YCbCr420(const uint8_t *read_pt, uint16_t line_byte)
{
	natural_int_t i, j;
	const uint16_t *line1;
	const uint16_t *line2;
	uint8_t ycbcr0[3], ycbcr1[3];
	uint8_t ycbcr2[3], ycbcr3[3];
	uint8_t *Y_pt1, *Y_pt2, *Cb_pt, *Cr_pt;
	uint16_t rgb565;
	uint8_t rgb888[3];

	for (i = 0; i < _JPEG_DCTSIZE; i++)
	{
		line1 = (const uint16_t *)read_pt;				/* odd line */
		read_pt += line_byte;
		line2 = (const uint16_t *)read_pt;				/* even line */
		read_pt += line_byte;

		Y_pt1 = &Y_buf[(i*2)*(_JPEG_DCTSIZE*2)];
		Y_pt2 = &Y_buf[(i*2+1)*(_JPEG_DCTSIZE*2)];
		Cb_pt = &Cb_buf[i*(_JPEG_DCTSIZE)];
		Cr_pt = &Cr_buf[i*(_JPEG_DCTSIZE)];

		for (j = 0; j < (_JPEG_DCTSIZE*2); j += 2)
		{
			rgb565 = *line1++;
			RGB565_to_RGB888(rgb565, rgb888);
			r_rgb2ycc(rgb888, ycbcr0);
			rgb565 = *line1++;
			RGB565_to_RGB888(rgb565, rgb888);
			r_rgb2ycc(rgb888, ycbcr1);

			rgb565 = *line2++;
			RGB565_to_RGB888(rgb565, rgb888);
			r_rgb2ycc(rgb888, ycbcr2);
			rgb565 = *line2++;
			RGB565_to_RGB888(rgb565, rgb888);
			r_rgb2ycc(rgb888, ycbcr3);

			*Y_pt1++ = ycbcr0[0];
			*Y_pt1++ = ycbcr1[0];
			*Y_pt2++ = ycbcr2[0];
			*Y_pt2++ = ycbcr3[0];
			*Cb_pt++ = (ycbcr0[1] + ycbcr1[1] + ycbcr2[1] + ycbcr3[1] + 2) / 4;
			*Cr_pt++ = (ycbcr0[2] + ycbcr1[2] + ycbcr2[2] + ycbcr3[2] + 2) / 4;
		}
	}
	return COMPRESS_JPEGE_OK;
}

uint8_t chg_bit_asign( uint8_t hibyte, uint8_t lobyte )
{
	uint16_t ret,tmp;

	tmp = (hibyte << 8 | lobyte );
	
	ret = (tmp & 0x7FF) >> 3;
	
	return ret;
}

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
/* YYCbCrYYCbCr... -> YYYY..., CbCb..., CrCr... */
int16_t readSamplingMCU_YCbCr422(const uint8_t *read_pt, uint16_t line_byte)
{
	natural_int_t i, j;
	const uint8_t *line;
	uint8_t *Y_pt, *Cb_pt, *Cr_pt;
#ifdef IMG_SENSOR_USE_OV7670
			uint8_t tmpY;
#endif

	for (i = 0; i < _JPEG_DCTSIZE; i++)
	{
		line = read_pt;
		read_pt += line_byte;

		Y_pt = &Y_buf[i*(_JPEG_DCTSIZE*2)];
		Cb_pt = &Cb_buf[i*(_JPEG_DCTSIZE)];
		Cr_pt = &Cr_buf[i*(_JPEG_DCTSIZE)];

		{
			for (j = 0; j < (_JPEG_DCTSIZE*2); j += 2)
			{
#if defined(GRKAEDE) || defined(IMG_SENSOR_USE_OV7740)
				/* YUYV�`�� 4byte little endian */
				*Cb_pt++ = *line++;
				*Y_pt++ = *line++;
				*Cr_pt++ = *line++;
				*Y_pt++ = *line++;
#elif defined IMG_SENSOR_USE_OV7670
				/* UYVY�`���w�� */
				tmpY = *line;line++;
				*Y_pt++ = chg_bit_asign(*line, tmpY);
				*Cb_pt++ = (*line & 0xF8); line++;
				tmpY = *line;line++;
				*Y_pt++ = chg_bit_asign(*line, tmpY);
				*Cr_pt++ = (*line & 0xF8); line++;
#else	/* original */
				*Y_pt++ = *line++;
				*Y_pt++ = *line++;
				*Cb_pt++ = *line++;
				*Cr_pt++ = *line++;
#endif	/* 1 */
			}
		}
	}
	return COMPRESS_JPEGE_OK;
}
