/*
 * gr_pdc_driver.h
 *
 *  Created on: 2015/08/21
 *      Author: a5034000
 */

#ifndef GR_PDC_DRIVER_H_
#define GR_PDC_DRIVER_H_

#include <Arduino.h>  // only for typedef

#define USE_IMG_PROC

/* Version Number of API. */
#define PDC_RX_VERSION_MAJOR  (1)
#define PDC_RX_VERSION_MINOR  (1)

/* Selects the active sense of the VSYNC signal. */
#define PDC_VSYNC_SIGNAL_POLARITY_HIGH    (0)
#define PDC_VSYNC_SIGNAL_POLARITY_LOW     (1)

/* Selects the active sense of the HSYNC signal. */
#define PDC_HSYNC_SIGNAL_POLARITY_HIGH    (0)
#define PDC_HSYNC_SIGNAL_POLARITY_LOW     (1)

/* Select the interrupt set to be updated. */
#define PDC_PCDFI_IPR_UPDATE              (0x0001)
#define PDC_PCFEI_IEN_UPDATE              (0x0002)
#define PDC_PCERI_IEN_UPDATE              (0x0004)
#define PDC_PCDFI_IEN_UPDATE              (0x0008)
#define PDC_DFIE_IEN_UPDATE               (0x0010)
#define PDC_FEIE_IEN_UPDATE               (0x0020)
#define PDC_OVIE_IEN_UPDATE               (0x0040)
#define PDC_UDRIE_IEN_UPDATE              (0x0080)
#define PDC_VERIE_IEN_UPDATE              (0x0100)
#define PDC_HERIE_IEN_UPDATE              (0x0200)
#define PDC_ALL_INT_UPDATE                (0x03FF)

/* VSYNC */
#define VSYNC_ACTIVE_L    (1)        /* VSYNC=ACTIVE LOW ->PDC.PCCR0.BIT.VPS */
#define VSYNC_ACTIVE_H    (0)        /* VSYNC=ACTIVE HIGH */
#define VSYNC_ACTIVE VSYNC_ACTIVE_L

/* HSYNC */
#define HSYNC_ACTIVE_L    (1)        /* HSYNC=ACTIVE LOW ->PDC.PCCR0.BIT.HPS */
#define HSYNC_ACTIVE_H    (0)        /* HSYNC=ACTIVE HIGH */
#define HSYNC_ACTIVE HSYNC_ACTIVE_H

/* memory transfer modlue select */
#define TRANS_DTC         (1)        /* TRANSFER=DTC */
#define TRANS_DMAC        (2)        /* TRANSFER=DMAC */
#define TRANS_MEMORY TRANS_DMAC
/* When DTC is chosen. "PCKO <= 20MHz" is specification.
   please, "r_bsp_config.h:BSP_CFG_PCKB_DIV" and "r_pdc_rx_config.h:PDC_CFG_PCKO_DIV" are checked */

// interrupt priority
#if TRANS_MEMORY == TRANS_DMAC
#define PDC_INT_PRI_DMAC  (1)
#else
#define PDC_INT_PRI_DTC   (7)
#endif

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/
/* R_PDC_Control control codes */
typedef enum e_pdc_command
{
    PDC_CMD_CAPTURE_START = 0,                    /* Start PDC capture */
    PDC_CMD_CHANGE_POS_AND_SIZE,                  /* Change PDC capture position and capture size */
    PDC_CMD_STATUS_GET,                           /* Get PDC status */
    PDC_CMD_STATUS_CLR,                           /* Clear PDC status */
    PDC_CMD_SET_INTERRUPT,                        /* PDC interrupt setting */
    PDC_CMD_DISABLE,                              /* Disable PDC receive operation */
    PDC_CMD_ENABLE,                               /* Enable PDC receive operation */
    PDC_CMD_RESET                                 /* PDC reset */
} pdc_command_t;

/* Function return values */
typedef enum e_pdc_return                         /* PDC API error codes */
{
    PDC_SUCCESS = 0,                              /* Processing finished successfully. */
    PDC_ERR_OPENED,                               /* PDC module initialized. Initialization function
                                                     R_PDC_Open has been run. */
    PDC_ERR_NOT_OPEN,                             /* PDC module uninitialized. R_PDC_Open has not been run. */
    PDC_ERR_NOT_INITIALIZED,                      /* PDC module uninitialized. R_PDC_Create has not been run. */
    PDC_ERR_INVALID_ARG,                          /* Invalid argument input. */
    PDC_ERR_INVALID_COMMAND,                      /* Command is invalid. */
    PDC_ERR_INVALID_INTERRUPT_SRC,                /* Specified interrupt source is invalid. */
    PDC_ERR_INVALID_HANDLER_ADDR,                 /* Argument function address input is invalid.
                                                     Registration of the previously registered function
                                                     has been canceled. */
    PDC_ERR_NULL_PTR,                             /* Argument pointer value was NULL. */
    PDC_ERR_BUSY,                                 /* PDC resources is in use by another process. */
    PDC_ERR_INTERNAL                              /* Module internal error is detected. */
} pdc_return_t;

/* R_PDC_IntWrite interrupt sources */
typedef enum e_pdc_interrupt_source
{
    PDC_INTERRUPT_PCDFI = 0,                      /* Receive data-ready interrupt */
    PDC_INTERRUPT_PCFEI,                          /* Frame-end interrupt */
    PDC_INTERRUPT_PCERI,                          /* Error interrupt */
} pdc_intsrc_t;

/* Interrupt priority level control */
typedef struct st_pdc_int_priority_data_cfg
{
    uint8_t   pcdfi_level;                        /* PCDFI interrupt priority level */
} pdc_ipr_dcfg_t;

/* Interrupt controller (ICUA) PDC interrupt enable/disable */
typedef struct st_pdc_inticu_data_cfg
{
    bool    pcfei_ien;                            /* Frame-end interrupt enabled */
    bool    pceri_ien;                            /* Error interrupt enabled */
    bool    pcdfi_ien;                            /* Receive data-ready interrupt enabled */
} pdc_inticu_dcfg_t;

/* PDC interrupt enable/disable */
typedef struct st_pdc_intpdc_data_cfg
{
    bool    dfie_ien;                             /* Receive data-ready interrupt enabled */
    bool    feie_ien;                             /* Frame-end interrupt enabled */
    bool    ovie_ien;                             /* Overrun interrupt enabled */
    bool    udrie_ien;                            /* Underrun interrupt enabled */
    bool    verie_ien;                            /* Vertical line count setting error interrupt enabled */
    bool    herie_ien;                            /* Horizontal byte count setting error interrupt enabled */
} pdc_intpdc_dcfg_t;

/* Capture position specification */
typedef struct st_pdc_position_data_cfg
{
    uint16_t   vst_position;                      /* Vertical capture start line position */
    uint16_t   hst_position;                      /* Horizontal capture start byte position */
} pdc_pos_dcfg_t;

/* Capture size specification */
typedef struct st_pdc_size_data_cfg
{
    uint16_t   vsz_size;                          /* Vertical capture size */
    uint16_t   hsz_size;                          /* Horizontal capture size */
} pdc_size_dcfg_t;

/* PDC settings */
typedef struct st_pdc_data_cfg
{
    uint16_t                iupd_select;          /* Interrupt setting update select */
    pdc_ipr_dcfg_t          priority;             /* Interrupt priority level */
    pdc_inticu_dcfg_t       inticu_req;           /* ICU interrupt setting */
    pdc_intpdc_dcfg_t       intpdc_req;           /* PDC interrupt setting */
    bool                    vps_select;           /* VSYNC signal polarity setting */
    bool                    hps_select;           /* HSYNC signal polarity setting */
    pdc_pos_dcfg_t          capture_pos;          /* Capture position setting */
    pdc_size_dcfg_t         capture_size;         /* Capture size setting */
} pdc_data_cfg_t;

/* Copy of PDC status register (PCSR) */
typedef struct st_pdc_pcsr_stat
{
    bool    frame_busy;                           /* PDC operating status (FBSY flag) */
    bool    fifo_empty;                           /* FIFO status (FEMPF flag) */
    bool    frame_end;                            /* Frame-end (FEF flag) */
    bool    overrun;                              /* Overrun (OVRF flag) */
    bool    underrun;                             /* Underrun (UDRF flag) */
    bool    verf_error;                           /* Vertical line count setting error (VERF flag) */
    bool    herf_error;                           /* Horizontal byte count setting error (HERF flag) */
} pdc_pcsr_stat_t;

/* Copy of PDC pin monitor status register (PCMONR) */
typedef struct st_pdc_pcmonr_stat
{
    bool    vsync;                                /* VSYNC signal status (VSYNC flag) */
    bool    hsync;                                /* HSYNC signal status (HSYNC flag) */
} pdc_pcmonr_stat_t;

/* PDC status */
typedef struct st_pdc_stat
{
    pdc_pcsr_stat_t     pcsr_stat;                /* PDC status register(PCSR) information */
    pdc_pcmonr_stat_t   pcmonr_stat;              /* PDC pin monitor status (PCMONR) information */
} pdc_stat_t;


/* Defines for the PCE bit. (PDC Operation Enable) */
#define PDC_DISABLE_OPERATION       (0)
#define PDC_ENABLE_OPERATION        (1)

/* Defines for the PCKE bit. (PIXCLK Input Enable) */
#define PDC_DISABLE_PIXCLK_INPUT    (0)
#define PDC_ENABLE_PIXCLK_INPUT     (1)

/* Defines for the PRST bit. (PDC Reset) */
#define PDC_RESET_RELEASE           (0)
#define PDC_RESET                   (1)

/* Defines for the PCKOE bit. (PCKO Output Enable) */
#define PDC_DISABLE_PCKO_OUTPUT     (0)
#define PDC_ENABLE_PCKO_OUTPUT      (1)

/* Defines for value of the VST bit. (Vertical Capture Start Line Position) */
#define PDC_VST_UPPER_LIMIT         (0x0FFE)

/* Defines for value of the HST bit. (Horizontal Capture Start Byte Position) */
#define PDC_HST_UPPER_LIMIT         (0x0FFB)

/* Defines for value of the VSZ bit. (Vertical Capture Size) */
#define PDC_VSZ_LOWER_LIMIT         (0x0001)
#define PDC_VSZ_UPPER_LIMIT         (0x0FFF)

/* Defines for value of the HSZ bit. (Horizontal Capture Size) */
#define PDC_HSZ_LOWER_LIMIT         (0x0004)
#define PDC_HSZ_UPPER_LIMIT         (0x0FFF)

/* Defines for mix value of the VST bit and VSZ bit. */
#define PDC_VSTVSZ_MIX_UPPER_LIMIT  (0x0FFF)
#define PDC_HSTHSZ_MIX_UPPER_LIMIT  (0x0FFF)

/* Defines for the EDS bit. (Endian Select) */
#define PDC_EDS_LITTLE_ENDIAN       (0)
#define PDC_EDS_BIG_ENDIAN          (1)

/* Defines for value of IPR bit. (Interrupt Priority Level) */
#define PDC_IPR_LV_UPPER_LIMIT      (0x0F)

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/
/* Defines for interrupt setup step */
typedef enum e_pdc_setup_step
{
    PDC_INITIALIZE1 = 0,                      /* Procedure 1, which is run by within R_PDC_Open. */
    PDC_INITIALIZE2,                          /* Procedure 2, which is run by within R_PDC_Create. */
    PDC_CHANGE_SETTING                        /* Procedure, which is run by within R_PDC_Control. */
} pdc_step_t;


/* PCKO Frequency Division Ratio Select. (PCKDIV)
   Available divisors = /2, /4, /6, /8, /10, /12, /14, /16
*/
#define PDC_CFG_PCKO_DIV                (6)

pdc_return_t R_PDC_Open(pdc_data_cfg_t *p_data_cfg);
pdc_return_t R_PDC_Close(void);
pdc_return_t R_PDC_Create(pdc_data_cfg_t *p_data_cfg);
pdc_return_t R_PDC_Control(pdc_command_t  command, pdc_data_cfg_t *p_data_cfg, pdc_stat_t *p_stat);
void *       R_PDC_GetFifoAddr(void);

bool isPdcFinished();

#endif /* ARDUINO_LIB_IMAGE_UTILITY_GR_PDC_DRIVER_H_ */
