/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/
/***********************************************************************************************************************
* File Name	   : r_rgb2ycc.c
* Description  : Converts from RGB to YCbCr
***********************************************************************************************************************/
/**********************************************************************************************************************
* History : DD.MM.YYYY Version  Description
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include "r_stdint.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#define NUM_COEFF	(8)

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/
#pragma section _jpeg_cmp_F

typedef union
{
	int16_t word[NUM_COEFF];
	int32_t	longWord[NUM_COEFF/2];
} CoeffData;

/***********************************************************************************************************************
Exported global variables (to be accessed by other files)
***********************************************************************************************************************/

#ifdef GRKAEDE
#define __LIT
#endif
/***********************************************************************************************************************
Private global variables and functions
***********************************************************************************************************************/
const CoeffData coeffData =
{
#ifdef __LIT
	0x4b23,		// +0.58700
	0x2646,		// +0.29900
	0x0e98,		// + 0.11400
	0xea68,		// -0.16874
	0xd59a,		// - 0.33126
	0x4000,		// + 0.50000
	0xf599,		// - 0.08131
	0xca69,		// - 0.41869
#else
	0x2646,		// +0.29900
	0x4b23,		// +0.58700
	0xea68,		// -0.16874
	0x0e98,		// + 0.11400
	0x4000,		// + 0.50000
	0xd59a,		// - 0.33126
	0xca69,		// - 0.41869
	0xf599,		// - 0.08131
#endif
};



#pragma section _jpeg_cmp_F
/***********************************************************************************************************************
* Function Name: r_rgb2ycc
* Description  : Converts from RGB to YCbCr
* Arguments    : uint8_t *rgb : RGB data
*              : uint8_t *ycc : YCbCr data
* Return Value : count -
* Return Value : none
***********************************************************************************************************************/
/*
 *	Y  =  0.29900 * R + 0.58700 * G + 0.11400 * B
 *	Cb = -0.16874 * R - 0.33126 * G + 0.50000 * B  + CENTERJSAMPLE
 *	Cr =  0.50000 * R - 0.41869 * G - 0.08131 * B  + CENTERJSAMPLE
 */
#pragma inline_asm r_rgb2ycc
void r_rgb2ycc(const uint8_t *rgb, uint8_t *ycc)
{
//	asm("push.l	r6");
//	asm("push.l	r7");

//; register r1: rgb
//; register r2: ycc

	asm("movu.b	[r1+], r3"); //	;	r
	asm("movu.b	[r1+], r4"); //	;	g
	asm("movu.b	[r1], r5"); //	;	b
	asm("mov.l	#_coeffData, r1");

	/*	Y = 0.29900 * R + 0.58700 * G + 0.11400 * B	*/
	asm("mov.l	[r1+], r6");
	asm("mov.l	[r1+], r7");
	asm("mullo	r6, r4");
	asm("shlr	#16, r6");//		; Shift into lower 16 bits to use coefficient in upper 16 bits
	asm("maclo	r6, r3");
	asm("maclo	r7, r5");
	asm("shlr	#16, r7");//		; -0.16874 is placed in lower 16 bits. This coefficient is used to compute Cb.
	asm("racw	#1");
	asm("mvfachi	r6");
	asm("min  	#000000FFH, r6");//		; Saturate calculation
	asm("max     #00H,r6");
	asm("mov.b   r6, [r2+]");

	/*	Cb = -0.16874 * R - 0.33126 * G + 0.50000 * B + 128	*/
	asm("mov.l	[r1+], r6");
	asm("mullo	r7, r3");
	asm("maclo	r6, r4");
	asm("shlr	#16, r6");//		; +0.50000 is placed in lower 16 bits. This coefficient is used to compute Cr.
	asm("maclo	r6, r5");
	asm("racw	#1");
	asm("mvfachi	r7");
	asm("add		#128, r7");
	asm("min  	#000000FFH, r7");//	; Saturate calculation
	asm("max     #00H, r7");
	asm("mov.b   r7, [r2+]");
	asm("mov.l	[r1+], r7");

	/*	Cr = 0.50000 * R - 0.41869 * G - 0.08131 * B + 128	*/
	asm("mullo	r6, r3");
	asm("maclo	r7, r5");
	asm("shlr	#16, r7");
	asm("maclo	r7, r4");
	asm("racw	#1");
	asm("mvfachi	r7");
	asm("add		#128, r7");
	asm("min  	#000000FFH, r7");//		; Saturate calculation
	asm("max     #00H, r7");
	asm("mov.b   r7, [r2]");

//	asm("pop	r7");
//	asm("pop	r6");
}
