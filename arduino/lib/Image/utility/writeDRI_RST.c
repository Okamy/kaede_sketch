/* Copyright (C) 2006-2013 Renesas Solutions Corp. */

#include "r_jpeg_development.h"


#pragma section _jpeg_enc_F


#define _JPEG_MARKER		0xff

#if 0
#define WRITE_BYTE(c, env)					\
	*_jpeg_next_output_byte(env->encFMB) = (c);			\
	++_jpeg_next_output_byte(env->encFMB);			\
	--_jpeg_free_in_buffer(env->encFMB);			\
	if (_jpeg_free_in_buffer(env->encFMB) == 0) {		\
			(*(env->enc_dump_func))(env);				\
	}
#endif

#define _JPEG_MARKER_DRI	0xdd

int32_t R_jpeg_writeDRI(int32_t ri,  struct _jpeg_working *wenv)
{
	natural_int_t n;

	WRITE_BYTE(_JPEG_MARKER, wenv);	/* DRI: 0xFFDD */
	WRITE_BYTE(_JPEG_MARKER_DRI, wenv);
	WRITE_BYTE(0x00, wenv);		/* Lr: 0x0004 */
	WRITE_BYTE(0x04, wenv);
	n = (ri >> 8) & 255;
	WRITE_BYTE(n, wenv);		/* Ri: 0x00XX */
	n = ri & 255;
	WRITE_BYTE(n, wenv);
	
	return _JPEG_OK;
}
#undef _JPEG_MARKER_DRI

int32_t R_jpeg_writeRST(int32_t m, struct _jpeg_working *wenv)
{
	natural_int_t n;

	R_jpeg_flush_bits(wenv);

	/* RSTm: 0xFFDx, x=0,1,...,7 */
	WRITE_BYTE(_JPEG_MARKER, wenv);		/* write 0xFF */

	n = 0xd0 | m;
	WRITE_BYTE(n, wenv);			/* write 0xDm */

	return _JPEG_OK;
}
