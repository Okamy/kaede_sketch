/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2013 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************/
/***********************************************************************************************************************
* File Name	   : r_compress_jpege.c
* Description  :
***********************************************************************************************************************/
/**********************************************************************************************************************
* History : DD.MM.YYYY Version  Description
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
#include "r_jpeg_development.h"
#include "r_compress_jpege.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Exported global variables (to be accessed by other files)
***********************************************************************************************************************/
//#pragma section _jpeg_cmp_F
#include "r_std_quant.h"

struct _jpeg_working working;
struct _jpeg_enc_FMB encFMB;
struct _jpeg_enc_SMB encSMB;

/* work */
int16_t MCU_data[_JPEG_DCTSIZE2];

uint8_t Y_buf[4*_JPEG_DCTSIZE2];
uint8_t Cb_buf[_JPEG_DCTSIZE2];
uint8_t Cr_buf[_JPEG_DCTSIZE2];

extern uint8_t * R_jpeg_write_out_buffer(int32_t *rest);
extern int32_t _jpeg_write_header(struct _jpeg_working *wenv);

extern int16_t readSamplingMCU_RGB888_YCbCr422(const uint8_t *read_pt, uint16_t line_byte);
extern int16_t readSamplingMCU_RGB888_YCbCr420(const uint8_t *read_pt, uint16_t line_byte);
extern int16_t readSamplingMCU_RGB565_YCbCr422(const uint8_t *read_pt, uint16_t line_byte);
extern int16_t readSamplingMCU_RGB565_YCbCr420(const uint8_t *read_pt, uint16_t line_byte);
extern int16_t readSamplingMCU_YCbCr422(const uint8_t *read_pt, uint16_t line_byte);

/***********************************************************************************************************************
Private global variables and functions
***********************************************************************************************************************/
static void _jpeg_init(struct r_jpeg_encode_t *setting, struct _jpeg_working *wenv);
static int16_t encode_jpeg422(struct r_jpeg_encode_t *setting, struct _jpeg_working *work);
static int16_t encode_jpeg420(struct r_jpeg_encode_t *setting, struct _jpeg_working *work);
static void initYCbCr422(uint8_t *Y_data[], uint8_t *Cb_data[], uint8_t *Cr_data[]);
static void initYCbCr420(uint8_t *Y_data[], uint8_t *Cb_data[], uint8_t *Cr_data[]);

static void R_jpeg_dump_buffer(struct _jpeg_working *wenv);


//#pragma section _jpeg_cmp_F
/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
int32_t R_compress_jpeg(struct r_jpeg_encode_t *setting)
{
	int16_t ret;

//	_jpeg_file_size = 0;

#if 0	// ��Ń`�F�b�N����̂ŕs�v
	switch (setting->input_format)
	{
		case JPEGE_IN_RGB565:
		case JPEGE_IN_RGB888:
			break;
		case JPEGE_IN_YCC422:
			if (setting->output_format != JPEGE_OUT_YCC422)
			{
				return COMPRESS_JPEGE_ERROR_ARG;
			}
			break;
//	case JPEGE_IN_YCC420:
//		if(setting->output_format != JPEGE_OUT_YCC420)
//		{
//			return COMPRESS_JPEGE_ERROR_ARG;
//		}
//		break;
		default:
			return COMPRESS_JPEGE_ERROR_ARG;
	}
#endif

	if (setting->quality > 128)
	{
		return COMPRESS_JPEGE_ERROR_ARG;
	}

	working.encFMB = &encFMB;
	working.encSMB = &encSMB;
	working.encFMC = (struct _jpeg_enc_FMC *) & _jpeg_enc_fmcData;
	working.encSMC = (struct _jpeg_enc_SMC *) & _jpeg_enc_smcData;
	working.enc_dump_func = R_jpeg_dump_buffer;
	_jpeg_init(setting, &working);

	switch (setting->output_format)
	{
		case JPEGE_OUT_YCC422:
			/* only 16n x 8m */
			if (((setting->width & 0x000F) != 0) || ((setting->height & 0x0007) != 0))
			{
				return COMPRESS_JPEGE_ERROR_ARG;
			}
			ret = encode_jpeg422(setting, &working);
			if (ret < 0)
			{
				return ret;
			}

			break;
		case JPEGE_OUT_YCC420:
			/* only 16n x 16m */
			if (((setting->width & 0x000F) != 0) || ((setting->height & 0x000F) != 0))
			{
				return COMPRESS_JPEGE_ERROR_ARG;
			}
			ret = encode_jpeg420(setting, &working);
			if (ret < 0)
			{
				return ret;
			}

			break;
		default:
			return COMPRESS_JPEGE_ERROR_ARG;
	}

	return COMPRESS_JPEGE_OK;
//	return ((setting->outbuff_size) - _jpeg_free_in_buffer(working.encFMB));
}


/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
static void _jpeg_init(struct r_jpeg_encode_t *setting, struct _jpeg_working *wenv)
{
	int8_t *hsample_ratio, *vsample_ratio, *quant_tbl_no;
	int8_t *component_id, *dc_tbl_no, *ac_tbl_no;
	struct component_info *cinfo;
	struct frame_component_info *finfo;
	struct _jpeg_enc_FMB *FMBbase;
	struct _jpeg_enc_SMB *SMBbase;

	SMBbase = wenv->encSMB;
	FMBbase = wenv->encFMB;
	cinfo = &(component_info(SMBbase));
	_jpeg_num_QTBL(SMBbase) = 0;
	_jpeg_next_output_byte(FMBbase) = (uint8_t *)(setting->outbuff);
	_jpeg_free_in_buffer(FMBbase) = (setting->outbuff_size);

	_jpeg_restart_interval(FMBbase) = setting->restart_interval;
//	_jpeg_thinning_mode(FMBbase) = 0;

	_jpeg_write_DQT(SMBbase) = (int8_t)1;
	_jpeg_write_DHT(SMBbase) = (int8_t)1;

	component_id = (int8_t *)(cinfo->component_id);
	hsample_ratio = (int8_t *)(cinfo->hsample_ratio);
	vsample_ratio = (int8_t *)(cinfo->vsample_ratio);
	quant_tbl_no = (int8_t *)(cinfo->quant_tbl_no);
	switch (setting->output_format)
	{
		case JPEGE_OUT_YCC422:
			component_id[0] = 0;
			component_id[1] = 1;
			component_id[2] = 2;
			component_id[3] = 3;
			hsample_ratio[0] = 0;
			hsample_ratio[1] = 2;
			hsample_ratio[2] = 1;
			hsample_ratio[3] = 1;
			vsample_ratio[0] = 0;
			vsample_ratio[1] = 1;
			vsample_ratio[2] = 1;
			vsample_ratio[3] = 1;
			quant_tbl_no[0] = 0;
			quant_tbl_no[1] = 0;
			quant_tbl_no[2] = 1;
			quant_tbl_no[3] = 1;
			break;
		case JPEGE_OUT_YCC420:
		default:
			component_id[0] = 0;
			component_id[1] = 1;
			component_id[2] = 2;
			component_id[3] = 3;
			hsample_ratio[0] = 0;
			hsample_ratio[1] = 2;
			hsample_ratio[2] = 1;
			hsample_ratio[3] = 1;
			vsample_ratio[0] = 0;
			vsample_ratio[1] = 2;
			vsample_ratio[2] = 1;
			vsample_ratio[3] = 1;
			quant_tbl_no[0] = 0;
			quant_tbl_no[1] = 0;
			quant_tbl_no[2] = 1;
			quant_tbl_no[3] = 1;
			break;
	}

	/*
		 init Frame Information
	 */
	_jpeg_frame_num_of_components(SMBbase) = 3;
	finfo = &(frame_component_info(SMBbase));
	component_id = (int8_t *)(finfo->component_id);
	dc_tbl_no = (int8_t *)(finfo->dc_tbl_no);
	ac_tbl_no = (int8_t *)(finfo->ac_tbl_no);
	component_id[0] = 0;
	component_id[1] = 1;
	component_id[2] = 2;
	component_id[3] = 3;
	dc_tbl_no[0] = 0;
	dc_tbl_no[1] = 0;
	dc_tbl_no[2] = 1;
	dc_tbl_no[3] = 1;
	ac_tbl_no[0] = 0;
	ac_tbl_no[1] = 0;
	ac_tbl_no[2] = 1;
	ac_tbl_no[3] = 1;

	_jpeg_cur_put_buffer(FMBbase) = _jpeg_cur_put_bits(FMBbase) = 0;
}


/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
static int16_t encode_jpeg422(struct r_jpeg_encode_t *setting, struct _jpeg_working *work)
{
	int32_t ret;
	uint8_t *Y_data[_JPEG_DCTSIZE];
	uint8_t *Cb_data[_JPEG_DCTSIZE];
	uint8_t *Cr_data[_JPEG_DCTSIZE];

	int16_t col;
	int16_t Y_last_dc_val, Cb_last_dc_val, Cr_last_dc_val;
	struct _jpeg_enc_FMB *pencFMB;
	struct _jpeg_enc_SMB *pencSMB;
	natural_int_t next_marker_num, MCU_count, restart_interval;
	uint16_t width, height, line_byte;
	int16_t quality;

	const uint8_t *pNextInput0, *pNextInput;
	int32_t add_NextInput;
	int16_t (*call_pt)(const uint8_t *, uint16_t);


	switch (setting->input_format)
	{
		case JPEGE_IN_RGB888:
			add_NextInput = (_JPEG_DCTSIZE * 2) * 3;
			call_pt = readSamplingMCU_RGB888_YCbCr422;
			break;
		case JPEGE_IN_RGB565:
			add_NextInput = (_JPEG_DCTSIZE * 2) * 2;
			call_pt = readSamplingMCU_RGB565_YCbCr422;
			break;
		case JPEGE_IN_YCC422:
			add_NextInput = (_JPEG_DCTSIZE * 2) * 2;
			call_pt = readSamplingMCU_YCbCr422;
			break;
		default:
			return COMPRESS_JPEGE_ERROR_ARG;
	}

	pNextInput0 = setting->input_addr;

	width = setting->width;
	height = setting->height;
	line_byte = setting->input_line_byte;
	quality = setting->quality;
	pencSMB = work->encSMB;
	pencFMB = work->encFMB;

	_jpeg_line_length(pencSMB) = width;
	_jpeg_number_of_lines(pencSMB) = height;

	next_marker_num = 0;
	MCU_count = restart_interval = _jpeg_restart_interval(pencFMB);       /* Restart Interval */

	R_jpeg_add_quant_table(0, _jpeg_std_luminance_quant_tbl, quality, work);
	R_jpeg_add_quant_table(1, _jpeg_std_chrominance_quant_tbl, quality, work);

	ret = _jpeg_write_header(work);
	if (ret != _JPEG_OK)
	{
		return COMPRESS_JPEGE_ERROR_WRITE;
	}

	height >>= 3;	/* number of MCU per line */
	Y_last_dc_val = Cb_last_dc_val = Cr_last_dc_val = 0;

	initYCbCr422(Y_data, Cb_data, Cr_data);

	while (height--)
	{
		col = _jpeg_line_length(pencSMB) >> 4;	/* WIDTH columns / 16 columns */

		pNextInput = pNextInput0;
		pNextInput0 += (line_byte * _JPEG_DCTSIZE);

		while (col--)
		{
			if (restart_interval)
			{
				if (MCU_count == 0)
				{
					ret = R_jpeg_writeRST(next_marker_num, work);
					if (ret != _JPEG_OK)
					{
						return COMPRESS_JPEGE_ERROR_WRITE;
					}
					next_marker_num = (next_marker_num + 1) & 7;
					MCU_count = restart_interval - 1;
					Y_last_dc_val = Cb_last_dc_val = Cr_last_dc_val = 0;
				}
				else
				{
					--MCU_count;
				}
			}
			call_pt(pNextInput, setting->input_line_byte);

			R_jpeg_DCT(Y_data, 0, 0, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Y_last_dc_val, 0, 0, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Y_last_dc_val = *MCU_data;

			R_jpeg_DCT(Y_data, _JPEG_DCTSIZE, 0, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Y_last_dc_val, 0, 0, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Y_last_dc_val = *MCU_data;

			R_jpeg_DCT(Cb_data, 0, 1, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Cb_last_dc_val, 1, 1, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Cb_last_dc_val = *MCU_data;

			R_jpeg_DCT(Cr_data, 0, 1, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Cr_last_dc_val, 1, 1, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Cr_last_dc_val = *MCU_data;
			pNextInput += add_NextInput;
		}
	}
	ret = R_jpeg_flush_bits(work);
	if (ret != _JPEG_OK)
	{
		return COMPRESS_JPEGE_ERROR_WRITE;
	}
	ret = R_jpeg_writeEOI(work);
	if (ret != _JPEG_OK)
	{
		return COMPRESS_JPEGE_ERROR_WRITE;
	}
	R_jpeg_write_out_buffer(&_jpeg_free_in_buffer(pencFMB));
	return COMPRESS_JPEGE_OK;
}

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
static int16_t encode_jpeg420(struct r_jpeg_encode_t *setting, struct _jpeg_working *work)
{
	int32_t ret;
	uint8_t *Y_data[_JPEG_DCTSIZE * 2];
	uint8_t *Cb_data[_JPEG_DCTSIZE];
	uint8_t *Cr_data[_JPEG_DCTSIZE];

	int16_t col;
	int16_t Y_last_dc_val, Cb_last_dc_val, Cr_last_dc_val;
	struct _jpeg_enc_FMB *pencFMB;
	struct _jpeg_enc_SMB *pencSMB;
	natural_int_t next_marker_num, MCU_count, restart_interval;
	uint16_t width, height, line_byte;
	int16_t quality;

	const uint8_t *pNextInput0, *pNextInput;
	int32_t add_NextInput;
	int16_t (*call_pt)(const uint8_t *, uint16_t);


	switch (setting->input_format)
	{
		case JPEGE_IN_RGB888:
			add_NextInput = (_JPEG_DCTSIZE * 2) * 3;
			call_pt = readSamplingMCU_RGB888_YCbCr420;
			break;
		case JPEGE_IN_RGB565:
			add_NextInput = (_JPEG_DCTSIZE * 2) * 2;
			call_pt = readSamplingMCU_RGB565_YCbCr420;
			break;
		case JPEGE_IN_YCC422:
		default:
			return COMPRESS_JPEGE_ERROR_ARG;
	}

	pNextInput0 = setting->input_addr;

	width = setting->width;
	height = setting->height;
	line_byte = setting->input_line_byte;
	quality = setting->quality;
	pencSMB = work->encSMB;
	pencFMB = work->encFMB;

	_jpeg_line_length(pencSMB) = width;
	_jpeg_number_of_lines(pencSMB) = height;

	next_marker_num = 0;
	MCU_count = restart_interval = _jpeg_restart_interval(pencFMB);       /* Restart Interval */

	R_jpeg_add_quant_table(0, _jpeg_std_luminance_quant_tbl, quality, work);
	R_jpeg_add_quant_table(1, _jpeg_std_chrominance_quant_tbl, quality, work);

	ret = _jpeg_write_header(work);
	if (ret != _JPEG_OK)
	{
		return COMPRESS_JPEGE_ERROR_WRITE;
	}

	height >>= 4;	/* number of MCU per line */
	Y_last_dc_val = Cb_last_dc_val = Cr_last_dc_val = 0;

	initYCbCr420(Y_data, Cb_data, Cr_data);

	while (height--)
	{
		col = _jpeg_line_length(pencSMB) >> 4;	/* WIDTH columns / 16 columns */

		pNextInput = pNextInput0;
		pNextInput0 += (line_byte * _JPEG_DCTSIZE * 2);

		while (col--)
		{
			if (restart_interval)
			{
				if (MCU_count == 0)
				{
					ret = R_jpeg_writeRST(next_marker_num, work);
					if (ret != _JPEG_OK)
					{
						return COMPRESS_JPEGE_ERROR_WRITE;
					}
					next_marker_num = (next_marker_num + 1) & 7;
					MCU_count = restart_interval - 1;
					Y_last_dc_val = Cb_last_dc_val = Cr_last_dc_val = 0;
				}
				else
				{
					--MCU_count;
				}
			}
			call_pt(pNextInput, setting->input_line_byte);

			R_jpeg_DCT(Y_data, 0, 0, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Y_last_dc_val, 0, 0, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Y_last_dc_val = *MCU_data;

			R_jpeg_DCT(Y_data, _JPEG_DCTSIZE, 0, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Y_last_dc_val, 0, 0, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Y_last_dc_val = *MCU_data;

			R_jpeg_DCT(&Y_data[_JPEG_DCTSIZE], 0, 0, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Y_last_dc_val, 0, 0, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Y_last_dc_val = *MCU_data;

			R_jpeg_DCT(&Y_data[_JPEG_DCTSIZE], _JPEG_DCTSIZE, 0, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Y_last_dc_val, 0, 0, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Y_last_dc_val = *MCU_data;

			R_jpeg_DCT(Cb_data, 0, 1, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Cb_last_dc_val, 1, 1, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Cb_last_dc_val = *MCU_data;

			R_jpeg_DCT(Cr_data, 0, 1, MCU_data, work);
			ret = R_jpeg_encode_one_block(MCU_data, Cr_last_dc_val, 1, 1, work);
			if (ret != _JPEG_OK)
			{
				return COMPRESS_JPEGE_ERROR_WRITE;
			}
			Cr_last_dc_val = *MCU_data;
			pNextInput += add_NextInput;
		}
	}

	ret = R_jpeg_flush_bits(work);
	if (ret != _JPEG_OK)
	{
		return COMPRESS_JPEGE_ERROR_WRITE;
	}
	ret = R_jpeg_writeEOI(work);
	if (ret != _JPEG_OK)
	{
		return COMPRESS_JPEGE_ERROR_WRITE;
	}
//	_jpeg_next_output_byte(pencFMB) = R_jpeg_write_out_buffer(&_jpeg_free_in_buffer(pencFMB));
	R_jpeg_write_out_buffer(&_jpeg_free_in_buffer(pencFMB));
	return COMPRESS_JPEGE_OK;
}

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
static void initYCbCr422(uint8_t *Y_data[], uint8_t *Cb_data[], uint8_t *Cr_data[])
{
	Y_data[0] = &Y_buf[0*_JPEG_DCTSIZE];
	Y_data[1] = &Y_buf[2*_JPEG_DCTSIZE];
	Y_data[2] = &Y_buf[4*_JPEG_DCTSIZE];
	Y_data[3] = &Y_buf[6*_JPEG_DCTSIZE];
	Y_data[4] = &Y_buf[8*_JPEG_DCTSIZE];
	Y_data[5] = &Y_buf[10*_JPEG_DCTSIZE];
	Y_data[6] = &Y_buf[12*_JPEG_DCTSIZE];
	Y_data[7] = &Y_buf[14*_JPEG_DCTSIZE];

	Cb_data[0] = &Cb_buf[0*_JPEG_DCTSIZE];
	Cb_data[1] = &Cb_buf[1*_JPEG_DCTSIZE];
	Cb_data[2] = &Cb_buf[2*_JPEG_DCTSIZE];
	Cb_data[3] = &Cb_buf[3*_JPEG_DCTSIZE];
	Cb_data[4] = &Cb_buf[4*_JPEG_DCTSIZE];
	Cb_data[5] = &Cb_buf[5*_JPEG_DCTSIZE];
	Cb_data[6] = &Cb_buf[6*_JPEG_DCTSIZE];
	Cb_data[7] = &Cb_buf[7*_JPEG_DCTSIZE];

	Cr_data[0] = &Cr_buf[0*_JPEG_DCTSIZE];
	Cr_data[1] = &Cr_buf[1*_JPEG_DCTSIZE];
	Cr_data[2] = &Cr_buf[2*_JPEG_DCTSIZE];
	Cr_data[3] = &Cr_buf[3*_JPEG_DCTSIZE];
	Cr_data[4] = &Cr_buf[4*_JPEG_DCTSIZE];
	Cr_data[5] = &Cr_buf[5*_JPEG_DCTSIZE];
	Cr_data[6] = &Cr_buf[6*_JPEG_DCTSIZE];
	Cr_data[7] = &Cr_buf[7*_JPEG_DCTSIZE];
}

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
static void initYCbCr420(uint8_t *Y_data[], uint8_t *Cb_data[], uint8_t *Cr_data[])
{
	Y_data[0] = &Y_buf[0*_JPEG_DCTSIZE];
	Y_data[1] = &Y_buf[2*_JPEG_DCTSIZE];
	Y_data[2] = &Y_buf[4*_JPEG_DCTSIZE];
	Y_data[3] = &Y_buf[6*_JPEG_DCTSIZE];
	Y_data[4] = &Y_buf[8*_JPEG_DCTSIZE];
	Y_data[5] = &Y_buf[10*_JPEG_DCTSIZE];
	Y_data[6] = &Y_buf[12*_JPEG_DCTSIZE];
	Y_data[7] = &Y_buf[14*_JPEG_DCTSIZE];
	Y_data[8] = &Y_buf[16*_JPEG_DCTSIZE];
	Y_data[9] = &Y_buf[18*_JPEG_DCTSIZE];
	Y_data[10] = &Y_buf[20*_JPEG_DCTSIZE];
	Y_data[11] = &Y_buf[22*_JPEG_DCTSIZE];
	Y_data[12] = &Y_buf[24*_JPEG_DCTSIZE];
	Y_data[13] = &Y_buf[26*_JPEG_DCTSIZE];
	Y_data[14] = &Y_buf[28*_JPEG_DCTSIZE];
	Y_data[15] = &Y_buf[30*_JPEG_DCTSIZE];

	Cb_data[0] = &Cb_buf[0*_JPEG_DCTSIZE];
	Cb_data[1] = &Cb_buf[1*_JPEG_DCTSIZE];
	Cb_data[2] = &Cb_buf[2*_JPEG_DCTSIZE];
	Cb_data[3] = &Cb_buf[3*_JPEG_DCTSIZE];
	Cb_data[4] = &Cb_buf[4*_JPEG_DCTSIZE];
	Cb_data[5] = &Cb_buf[5*_JPEG_DCTSIZE];
	Cb_data[6] = &Cb_buf[6*_JPEG_DCTSIZE];
	Cb_data[7] = &Cb_buf[7*_JPEG_DCTSIZE];

	Cr_data[0] = &Cr_buf[0*_JPEG_DCTSIZE];
	Cr_data[1] = &Cr_buf[1*_JPEG_DCTSIZE];
	Cr_data[2] = &Cr_buf[2*_JPEG_DCTSIZE];
	Cr_data[3] = &Cr_buf[3*_JPEG_DCTSIZE];
	Cr_data[4] = &Cr_buf[4*_JPEG_DCTSIZE];
	Cr_data[5] = &Cr_buf[5*_JPEG_DCTSIZE];
	Cr_data[6] = &Cr_buf[6*_JPEG_DCTSIZE];
	Cr_data[7] = &Cr_buf[7*_JPEG_DCTSIZE];
}

/***********************************************************************************************************************
* Function Name:
* Description  :
* Arguments    :
* Return Value :
***********************************************************************************************************************/
/* user defined function */
static void R_jpeg_dump_buffer(struct _jpeg_working *wenv)
{
	struct _jpeg_enc_FMB *FMBbase;

	FMBbase = wenv->encFMB;
	_jpeg_next_output_byte(FMBbase) = R_jpeg_write_out_buffer(&_jpeg_free_in_buffer(FMBbase));
}
