


#include "Mdl_IP_prot.h"



// ライブラリ関数 ジャンプテーブル構造体
typedef struct _jmp_table {
    void (*Mdl_IP_Init)( unsigned char *wk_adr );
    int (*Mdl_IP_PersonDetection)( unsigned char *in_img, PersonDetection_Rslt *rslt, unsigned char *out_img );
    int (*Mdl_IP_PersonDetection_ParamChg)( int kind, int val );
    int (*Mdl_IP_MovingDetection)( unsigned char *in_img1, unsigned char *in_img2, unsigned char *in_img3, MovingDetection_Rslt *rslt, unsigned char *out_img );
    int (*Mdl_IP_MovingDetection_ParamChg)( int kind, int val );
    int (*Mdl_IP_ImgRevise)( unsigned char *in_img, unsigned char *out_img );
    int (*Mdl_IP_ImgRevise_ParamChg)( int kind, int val );
} JMP_TABLE;

#define P_JMP_TABLE ((JMP_TABLE *)0xFFC00200)       // ライブラリ関数 ジャンプテーブル構造体の割付アドレス



// ライブラリ関数
void Mdl_IP_Init( unsigned char *wk_adr );                  // 初期化処理

int Mdl_IP_PersonDetection( unsigned char *in_img, PersonDetection_Rslt *rslt, unsigned char *out_img );    // 人物検知処理
int Mdl_IP_PersonDetection_ParamChg( int kind, int val );   // 人物検知処理のパラメータ変更

int Mdl_IP_MovingDetection( unsigned char *in_img1, unsigned char *in_img2, unsigned char *in_img3, MovingDetection_Rslt *rslt, unsigned char *out_img );   // 動体検知処理
int Mdl_IP_MovingDetection_ParamChg( int kind, int val );   // 動体検知処理のパラメータ変更

int Mdl_IP_ImgRevise( unsigned char *in_img, unsigned char *out_img );      // 歪み補正処理
int Mdl_IP_ImgRevise_ParamChg( int kind, int val );         // 歪み補正処理のパラメータ変更



















