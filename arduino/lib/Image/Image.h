/*
  Image.h - Image processing library for GR-KAEDE.
  Copyright (c) 2015 Renesas Electronics.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef Image_h
#define Image_h
#include "rx64m/typedefine.h"
extern "C"{
#include "utility/Mdl_IP_ex.h"
}

/******************************************************************************
Macro definitions
******************************************************************************/
#define IMG_BUF_BASE_ADDR 0x8000000 // SDRAM

#define IMG_YCBCR422         (0)
#define IMG_RGB888           (1)
#define IMG_RGB565           (2)
#define IMG_INPUT_FORMAT     (IMG_YCBCR422)

#if ( IMG_INPUT_FORMAT == VIDEO_YCBCR422 || IMG_INPUT_FORMAT == VIDEO_RGB565 )
#define IMG_SIZE_PER_PIC      (2u)
#else
#define IMG_SIZE_PER_PIC      (4u)
#endif

#define IMG_SENSOR_USE_OV7740
#define IMG_SENSOR_SLV_ADDRESS    0x42       // Slave address (bit0 = 0. (R/W bit) )
#define IMG_SIZE_VGA_WIDTH   640
#define IMG_SIZE_VGA_HEIGHT  480
#define IMG_SIZE_QVGA_WIDTH   320
#define IMG_SIZE_QVGA_HEIGHT  240
#define IMG_SIZE_QQVGA_WIDTH   320
#define IMG_SIZE_QQVGA_HEIGHT  240

#define IMG_SIZE_BUF_VGA (IMG_SIZE_VGA_WIDTH * IMG_SIZE_VGA_HEIGHT * IMG_SIZE_PER_PIC)
#define IMG_SIZE_BUF_QVGA (IMG_SIZE_QVGA_WIDTH * IMG_SIZE_QVGA_HEIGHT * IMG_SIZE_PER_PIC)
#define IMG_SIZE_BUF_QQVGA (IMG_SIZE_QQVGA_WIDTH * IMG_SIZE_QQVGA_HEIGHT * IMG_SIZE_PER_PIC)
#define IMG_SIZE_BUF_JPG  (100*1024) // 100KByte
#define JPEG_QUALITY 128
#define JPEG_OUTPUT_FMT    JPEGE_OUT_YCC422


class Image{
    public:
        typedef enum {
            ALL = 0,
            PERSON_UPPERLEFT,
            PERSON_UPPER,
            PERSON_UPPERRIGHT,
            PERSON_LEFT,
            PERSON_CENTER,
            PERSON_RIGHT,
            PERSON_LOWERLEFT,
            PERSON_LOWER,
            PERSON_LOWERRIGHT
        } person_detect_area_t;

        typedef enum {
            MOVING_NOAREA = 0,
            MOVING_UPPERLEFT,
            MOVING_UPPER,
            MOVING_UPPERRIGHT,
            MOVING_LEFT,
            MOVING_CENTER,
            MOVING_RIGHT,
            MOVING_LOWERLEFT,
            MOVING_LOWER,
            MOVING_LOWERRIGHT
        } moving_detect_area_t;

        Image();
        void begin();
        void captureStart();
        bool isCaptureFinished();
        uint8_t* createRgb565();
        uint8_t* correctRgb565();
        uint8_t* createGrayImage();
        uint8_t* createJpg();
        uint8_t* getCreatedRgb565();
        uint8_t* getCreatedGrayImage();
        uint8_t* getCreatedJpg();
        int32_t getCreatedJpgSize();
        bool personDetection();
        uint8_t getPersonNumber(person_detect_area_t area = ALL);
        bool movingDetection(uint8_t* before_gray_image1, uint8_t* before_gray_image2);
        uint8_t getMovingNumber();
        uint8_t getMovingArea(uint8_t moving);
    private:
        uint8_t* detect_workaddr;
        uint8_t* bufaddr;
        uint8_t* raw_rgbaddr;
        uint8_t* corrected_rgbaddr;
        uint8_t* grayaddr;
        uint8_t* jpgaddr;
        bool use_corrected_rgb565;
        bool done_create_rgb565;
        bool done_correct_rgb565;
        bool done_convert_gray;
        bool done_create_jpg;
        int32_t jpgsize;
        PersonDetection_Rslt g_pdct_rslt;
        MovingDetection_Rslt g_mdct_rslt;
        static uint8_t img_no;

};

#endif /* Image_h */
