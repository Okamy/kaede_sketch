/*
  Image.cpp - Image processing library for GR-KAEDE.
  Copyright (c) 2015 Renesas Electronics.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "Wire.h"
#include "Image.h"
extern "C" {
#include "utility/gr_pdc_driver.h"
#include "utility/gr_dma_driver.h"
#include "utility/r_compress_jpege.h"
#define  OV7740CONFIG_INITIAL
#include "utility/ov7740.h"
}


static void pdc_receive( uint8_t * write_buff_addr  );
static void jpeg_compress_init( uint8_t* buf_in, uint8_t* buf_jpeg );

static void YUV422_to_RGB565( unsigned char *in_adr, unsigned char *out_adr, int w, int h );
static void RGB565_to_Y( unsigned char *in_adr, unsigned char *out_adr, int w, int h );

static pdc_data_cfg_t    p_data_pdc;
static r_jpeg_encode_t cmp_jpeg_info;
static int32_t jpeg_output_size     = 0; // jpeg size afrer compress processing

bool g_flag_finished_image_begin = false;
uint8_t* g_buf_jpeg;

static void imageProcessingInit(void){
    if (g_flag_finished_image_begin == 1){
        return;
    }
    initialize_sdram();
    // pdc initialize

    // iic setting for a camera
    Wire3.begin();
    // ToDo ------------------------------------------//
    // SSCL2 and SSDA2 are shared both IO24/IO26 and IO30/IO31
    // Below setting affects both of that.
    // Configure SCI0's SCL, SDA pins for peripheral functions.
    pinMode(31, OUTPUT_OPENDRAIN);
    pinMode(30, OUTPUT_OPENDRAIN);
    BSET(portPullupControlRegister(digitalPinToPort(31)), digitalPinToBit(31)); //SDA pull-up on
    BSET(portModeRegister(digitalPinToPort(31)), digitalPinToBit(31)); //SDA
    BSET(portPullupControlRegister(digitalPinToPort(30)), digitalPinToBit(30)); //SCL pull-up on
    BSET(portModeRegister(digitalPinToPort(30)), digitalPinToBit(30)); //SCL
    assignPinFunction(31, 0b01010, 0, 0);
    assignPinFunction(30, 0b01010, 0, 0);
    //-----------------------------------------------//

    // PDC setting step1
    p_data_pdc.priority.pcdfi_level = PDC_INT_PRI_DMAC;
    p_data_pdc.inticu_req.pcdfi_ien = true;
    p_data_pdc.inticu_req.pcfei_ien = true;
    p_data_pdc.inticu_req.pceri_ien = true;

    R_PDC_Open(&p_data_pdc);

    // DMA setting
    R_DMACA_Open(DMACA_CH0);
    initDmac0ForPdc();

    // camera setting
    for ( int i = 0; i < 0xFF; i++ )
    {
        if ( change_reg[i][0] == 0xFF )
        {
            break;
        }

LBL_resetting:

        Wire3.beginTransmission(IMG_SENSOR_SLV_ADDRESS >> 1);
        Wire3.write(change_reg[i][0]);
        Wire3.write(change_reg[i][1]);
        Wire3.endTransmission();
        delay(20);

        if ( change_reg[i][0] == 0x12 )
        {
            continue;
        }
        Wire3.beginTransmission(IMG_SENSOR_SLV_ADDRESS >> 1);
        Wire3.write(change_reg[i][0]);
        Wire3.endTransmission(false);
        Wire3.requestFrom(IMG_SENSOR_SLV_ADDRESS >> 1, 1);
        if ( Wire3.read() != change_reg[i][1] )
        {
            //err++;
            goto LBL_resetting;
        }
    }

    /* initialize step2 of pdc */  // need to pclk from Image sensor
    p_data_pdc.intpdc_req.dfie_ien      = true;
    p_data_pdc.intpdc_req.feie_ien      = true;
    p_data_pdc.intpdc_req.ovie_ien      = true;
    p_data_pdc.intpdc_req.udrie_ien     = true;
    p_data_pdc.intpdc_req.verie_ien     = true;
    p_data_pdc.intpdc_req.herie_ien     = true;
#if VSYNC_ACTIVE == VSYNC_ACTIVE_L
    p_data_pdc.vps_select               = PDC_VSYNC_SIGNAL_POLARITY_LOW;
#else
    p_data_pdc.vps_select               = PDC_VSYNC_SIGNAL_POLARITY_HIGH;
#endif
#if HSYNC_ACTIVE == HSYNC_ACTIVE_H
    p_data_pdc.hps_select               = PDC_HSYNC_SIGNAL_POLARITY_HIGH;
#else
    p_data_pdc.hps_select               = PDC_HSYNC_SIGNAL_POLARITY_LOW;
#endif
    p_data_pdc.capture_pos.vst_position = 0;
    p_data_pdc.capture_pos.hst_position = 0;
    p_data_pdc.capture_size.vsz_size    = IMG_SENSOR_HEIGHT;
    p_data_pdc.capture_size.hsz_size    = IMG_SENSOR_WIDTH * 2;  //16bit(4:2:2)

    R_PDC_Create(&p_data_pdc);
}

#if 0
/***********************************************************************************************************************
* Function Name: R_dmac_ch1_init
* Description  : dmac ch1 initialize
* Arguments    : unsigned char continue_flag:forwarding address initial flag(0:initialize,1:continue)
* Return Value : none
***********************************************************************************************************************/
void R_dmac_ch1_init( uint8_t continue_flag )
{
    volatile dmaca_return_t      ret_dmac;
    dmaca_stat_t                 stat_dmac;

    g_data_dmac_ch1.des_addr_mode        = DMACA_DES_ADDR_INCR;
    g_data_dmac_ch1.src_addr_mode        = DMACA_SRC_ADDR_INCR;
    g_data_dmac_ch1.des_addr_repeat_area = DMACA_DES_ADDR_EXT_REP_AREA_NONE;
    g_data_dmac_ch1.src_addr_repeat_area = DMACA_SRC_ADDR_EXT_REP_AREA_NONE;
    g_data_dmac_ch1.transfer_mode        = DMACA_TRANSFER_MODE_BLOCK;
    g_data_dmac_ch1.repeat_block_side    = DMACA_REPEAT_BLOCK_DISABLE;
    g_data_dmac_ch1.data_size            = DMACA_DATA_SIZE_LWORD;
    g_data_dmac_ch1.request_source       = DMACA_TRANSFER_REQUEST_SOFTWARE;//DMACA_TRANSFER_REQUEST_PERIPHERAL;
    g_data_dmac_ch1.block_size           = JPEG_OUTPUT_AREA_SIZE / 1024; // 1K=1024byte
    g_data_dmac_ch1.transfer_count       = (1024 / 4);                   // DMACA_DATA_SIZE_LWORD = 32bit
    g_data_dmac_ch1.p_src_addr           = (void *)buf_jpeg;
    g_data_dmac_ch1.p_des_addr           = (void *)buf_image;
    g_data_dmac_ch1.offset_value         = 0x00000000;
    g_data_dmac_ch1.dtie_request         = DMACA_TRANSFER_END_INTERRUPT_ENABLE;
    g_data_dmac_ch1.esie_request         = DMACA_TRANSFER_ESCAPE_END_INTERRUPT_DISABLE;
    g_data_dmac_ch1.rptie_request        = DMACA_REPEAT_SIZE_END_INTERRUPT_DISABLE;
    g_data_dmac_ch1.sarie_request        = DMACA_SRC_ADDR_EXT_REP_AREA_OVER_INTERRUPT_DISABLE;
    g_data_dmac_ch1.darie_request        = DMACA_DES_ADDR_EXT_REP_AREA_OVER_INTERRUPT_DISABLE;
    g_data_dmac_ch1.act_source           = IR_DMAC_DMAC1I;
    g_data_dmac_ch1.interrupt_sel        = DMACA_CLEAR_INTERRUPT_FLAG_BEGINNING_TRANSFER;

    ret_dmac = R_DMACA_Create(DMACA_CH1, &g_data_dmac_ch1);
    if (DMACA_SUCCESS != ret_dmac)
    {
        /* do something */
    }

    ret_dmac = R_DMACA_Int_Enable(DMACA_CH1, 8);
    if (DMACA_SUCCESS != ret_dmac)
    {
        /* do something */
    }

    ret_dmac = R_DMACA_Control(DMACA_CH1, DMACA_CMD_ENABLE, &stat_dmac);
    if (DMACA_SUCCESS != ret_dmac)
    {
        /* do something */
    }

    ret_dmac = R_DMACA_Control(DMACA_CH1, DMACA_CMD_SOFT_REQ_NOT_CLR_REQ, &stat_dmac);
    if (DMACA_SUCCESS != ret_dmac)
    {
        /* do something */
    }

}

#endif

void jpeg_compress_init( uint8_t* buf_in, uint8_t* buf_jpeg ){

    /* JPEG Setting */
    cmp_jpeg_info.width             = IMG_SIZE_QVGA_WIDTH;
    cmp_jpeg_info.height            = IMG_SIZE_QVGA_HEIGHT;
    cmp_jpeg_info.quality           = JPEG_QUALITY;               /* 1-128(128:high quality) */
    cmp_jpeg_info.restart_interval  = 0;                          /* temp */

    /* input */
    cmp_jpeg_info.input_addr        = buf_in;    /* input address(original picture) */
    cmp_jpeg_info.input_line_byte   = 320*2;//IMG_SENSOR_OUTPUT_WIDTH * 2;    /* line number(input size(WIDTH * 16bit(4:2:2))) */
#ifdef USE_IMG_PROC
    cmp_jpeg_info.input_format      = JPEGE_IN_RGB565;                /* input format */
#else // USE_IMG_PROC
    cmp_jpeg_info.input_format      = JPEGE_IN_YCC422;                /* input format */
#endif // USE_IMG_PROC

#ifdef JPEG_AREA_MULTI
    cmp_jpeg_info.outbuff           = buf_jpeg; /* output buffer address(internal RAM) */
#else
    cmp_jpeg_info.outbuff           = (uint8_t *)buf_jpeg;                                 /* output buffer address(internal RAM) */
#endif // JPEG_AREA_MULTI
    cmp_jpeg_info.outbuff_size      = IMG_SIZE_BUF_JPG;                                /* output buffer size(limite size) */
    cmp_jpeg_info.output_format     = JPEG_OUTPUT_FMT;                                      /* output format                   */

}

void pdc_receive( uint8_t * write_buff_addr  )
{
    pdc_return_t       ret_pdc;
    pdc_data_cfg_t     p_data_pdc;
    pdc_stat_t         p_stat_pdc;

    setDestAdrDmac0ForPdc( write_buff_addr  );

    ret_pdc = R_PDC_Control(PDC_CMD_CAPTURE_START, &p_data_pdc, &p_stat_pdc);
    if (PDC_SUCCESS == ret_pdc)
    {
        /* do something */
    }
}


#define CLIP( val, min, max )   { if( val < min ){ val =  min ;}else if( val > max ){ val = max;} }

#define YUV2RGB( y,u,v,r,g,b )  {   r = y + (1435*v)/1024; \
                                    g = y - ( 352*u + 731*v )/1024; \
                                    b = y + (1814*u)/(1024); \
                                    CLIP( r, 0, 255 );\
                                    CLIP( g, 0, 255 );\
                                    CLIP( b, 0, 255 );\
}

#define GET_B( dt2, dt1 )   ( ((dt2)<<3)&0xf8 )
#define GET_G( dt2, dt1 )   ((((dt1)<<5)&0xe0 ) | ( ((dt2)>>3)&0x1C ) )
#define GET_R( dt2, dt1 )   ( ((dt1)&0xf8 ) )

#define RGB2Y( r,g,b,y )    {   y = ((306*r)/1024) + ((601*g)/1024) + ((116*b)/1024) ; \
                                CLIP( y, 0, 255 );\
}


void YUV422_to_RGB565( unsigned char *in_adr, unsigned char *out_adr, int w, int h)
{
    int x, y;
    unsigned char dt_y1, dt_u, dt_v;//,dt_y2;
    int R1,G1,B1;
    int u, v;
    unsigned char R, G, B;
    unsigned char *pInW = in_adr;
    unsigned char *pOutW= out_adr;

    for( y = 0 ; y < h; y+=2 ){
        for( x = 0; x < w; x+=2 ){

            // Y0,U,Y1,V
            dt_y1 = *pInW++;
            dt_u  = *pInW++;
            *pInW++;
            dt_v  = *pInW++;

            u = dt_u -128;
            v = dt_v -128;

            YUV2RGB( dt_y1,u,v, R1, G1, B1 );

            R = (unsigned char)R1;
            G = (unsigned char)G1;
            B = (unsigned char)B1;
            *pOutW++ = ( ( (G<<3)&0xe0 ) | ( (B>>3)&0x1f ) );
            *pOutW++ = ( ( R&0xF8      ) | ( (G>>5)&0x07 ) );

        }
        pInW+=w*2;
    }
}



void RGB565_to_Y( unsigned char *in_adr, unsigned char *out_adr, int w, int h )
{
    int x, y;
    int dt_y;
    unsigned char dth;
    unsigned char dtl;
    int R, G, B;
    unsigned char *pInW = in_adr;
    unsigned char *pOutW= out_adr;


    for( y = 0 ; y < h; y++ ){
        for( x = 0; x < w; x++ ){
            dth = *pInW++;
            dtl = *pInW++;

            B = GET_B( dth, dtl );
            G = GET_G( dth, dtl );
            R = GET_R( dth, dtl );

            RGB2Y( R, G, B, dt_y );

            *pOutW++ = (unsigned char)dt_y;
        }
    }
}


/******************************************************************************
* Function Name : R_jpeg_write_out_buffer
* Description   : This function is a callback function when the compressor finishes.
* Argument      : none
* Return Value  : none
******************************************************************************/
uint8_t *R_jpeg_write_out_buffer( int32_t *rest )
{
#if 0 // move to main.c
#ifndef JPEG_AREA_MULTI
    dmaca_stat_t                 stat_dmac;

    R_DMACA_Control(DMACA_CH1, DMACA_CMD_SOFT_REQ_NOT_CLR_REQ, &stat_dmac);
#endif
#endif

    noInterrupts();

    /* size = jpeg area size - output size(now) */
    jpeg_output_size = (int32_t)IMG_SIZE_BUF_JPG - *rest;    // jpeg size update

    interrupts();


#ifdef USE_MMC_IMG_OUTPUT

#ifdef USE_MMC_IMG_OUTPUT_JPEG
    R_mmc_write( cmp_jpeg_info.outbuff, jpeg_output_size, MMC_FILE_EXTENSTION_TYPE_JPEG );
#endif // USE_MMC_IMG_OUTPUT_JPEG

#ifdef USE_MMC_IMG_OUTPUT_PROCESSION
    R_mmc_write( (uint8_t *)cmp_jpeg_info.input_addr, IMG_SESOR_OUTOUT_PIXEL_SIZE_INTERNAL, MMC_FILE_EXTENSTION_TYPE_PROCESSION );
#endif // USE_MMC_IMG_OUTPUT_PROCESSION

#endif // USE_MMC_IMG_OUTPUT

    return g_buf_jpeg;
}

/****************** end of static functions ******************************/
uint8_t Image::img_no = 0;

Image::Image(){
    //0x8000000
    // detect_work (675K)   : IMG_SIZE_BUF_QVGA * 4 + IMG_SIZE_BUF_QQVGA * 2
    //------------------------------------------
    //------------------------------------------
    // bufaddr    (600K)     : IMG_SIZE_BUF_VGA
    // r_rgbaddr  (600K)     : IMG_SIZE_BUF_QVGA(need VGA size for correction)
    // c_rgbaddr  (150K)     : IMG_SIZE_BUF_QVGA
    // grayaddr   (75K)      : IMG_SIZE_BUF_QVGA / 2
    // jpgaddr    (100K)     : IMG_SIZE_BUF_JPG
    //-----------------------------------------

    detect_workaddr = (uint8_t*)IMG_BUF_BASE_ADDR;
    uint8_t* offset = (uint8_t *)((IMG_SIZE_BUF_VGA*2 + IMG_SIZE_BUF_QVGA + IMG_SIZE_BUF_QVGA/2 + IMG_SIZE_BUF_JPG) * img_no \
            + IMG_SIZE_BUF_QVGA * 4 + IMG_SIZE_BUF_QQVGA * 2 + detect_workaddr);

    bufaddr = offset;
    raw_rgbaddr = (uint8_t*)(bufaddr + IMG_SIZE_BUF_VGA);
    corrected_rgbaddr = (uint8_t*)(raw_rgbaddr + IMG_SIZE_BUF_VGA);
    grayaddr = (uint8_t*)(corrected_rgbaddr + IMG_SIZE_BUF_QVGA);
    jpgaddr = (uint8_t*)(grayaddr + IMG_SIZE_BUF_QVGA/2);
    use_corrected_rgb565 = false;
    done_create_rgb565 = false;
    done_correct_rgb565 = false;
    done_convert_gray = false;
    done_create_jpg = false;
    jpgsize = 0;

    ++img_no;
}

void Image::begin(){
    imageProcessingInit();
    // 画像処理ミドルウェア初期化
//    Mdl_IP_Init( detect_workaddr );
}

void Image::captureStart(){
    pdc_receive(bufaddr);
    done_create_rgb565 = false;
    done_correct_rgb565 = false;
    done_convert_gray = false;
    done_create_jpg = false;
    jpgsize = 0;

}

bool Image::isCaptureFinished(){
    return isPdcFinished();
}

uint8_t* Image::createRgb565(){
    YUV422_to_RGB565( bufaddr, raw_rgbaddr, IMG_SIZE_VGA_WIDTH, IMG_SIZE_VGA_HEIGHT );
    done_create_rgb565 = true;
    if(use_corrected_rgb565){
        return correctRgb565();
    }
    return raw_rgbaddr;
}

uint8_t* Image::correctRgb565(){
    if(!done_correct_rgb565){
        if(Mdl_IP_ImgRevise(raw_rgbaddr, corrected_rgbaddr) == 0){
            done_correct_rgb565 = true;
        }
    }
    return corrected_rgbaddr;
}

uint8_t* Image::createGrayImage(){

    if(use_corrected_rgb565){
        if(!done_correct_rgb565){
            createRgb565();
        }
        RGB565_to_Y(corrected_rgbaddr, grayaddr, IMG_SIZE_QVGA_WIDTH, IMG_SIZE_QVGA_HEIGHT);
    } else {
        if(!done_create_rgb565){
            createRgb565();
        }
        RGB565_to_Y(raw_rgbaddr, grayaddr, IMG_SIZE_QVGA_WIDTH, IMG_SIZE_QVGA_HEIGHT);
    }

    done_convert_gray = true;
    return grayaddr;
}

uint8_t* Image::createJpg(){
    if(use_corrected_rgb565){
        if(!done_correct_rgb565){
            createRgb565();
        }
        jpeg_compress_init(corrected_rgbaddr, jpgaddr);
    } else {
        if(!done_create_rgb565){
            createRgb565();
        }
        jpeg_compress_init(raw_rgbaddr, jpgaddr);
    }

/*
    int32_t ret = R_compress_jpeg( &cmp_jpeg_info );    // compress exec.
    if ( ret != COMPRESS_JPEGE_OK )
    {
    }
*/
    noInterrupts(); //TODO : Webサーバー使用時にEIだと何回も失敗することがある。
    while(R_compress_jpeg( &cmp_jpeg_info) != COMPRESS_JPEGE_OK);
    interrupts();
    done_create_jpg = true;
    jpgsize = jpeg_output_size;


    return jpgaddr;
}


uint8_t* Image::getCreatedRgb565(){
    uint8_t* ret;
    if(use_corrected_rgb565){
        if(done_correct_rgb565){
            ret = corrected_rgbaddr;
        } else {
            ret = NULL;
        }
    } else {
        if(done_create_rgb565){
            ret = raw_rgbaddr;
        } else {
            ret = NULL;
        }
    }
    return ret;
}
uint8_t* Image::getCreatedGrayImage(){
    uint8_t* ret;
    if(done_convert_gray){
        ret = grayaddr;
    } else {
        ret = NULL;
    }
    return ret;
}
uint8_t* Image::getCreatedJpg(){
    uint8_t* ret;
    if(done_create_jpg){
        ret = jpgaddr;
    } else {
        ret = NULL;
    }
    return ret;
}

int32_t Image::getCreatedJpgSize(){
    int32_t ret;
    if(done_create_jpg){
        ret = jpgsize;
    } else {
        ret = 0;
    }
    return ret;
}

bool Image::personDetection(){
    bool result;
    // 画像処理ミドルウェア初期化
    Mdl_IP_Init( detect_workaddr );

    if(!done_convert_gray){
        createGrayImage();
    }
    memset( &g_pdct_rslt, 0x0, sizeof( PersonDetection_Rslt ) );
    if(use_corrected_rgb565){
        result = (bool)Mdl_IP_PersonDetection( grayaddr, &g_pdct_rslt, corrected_rgbaddr);
    } else {
        result = (bool)Mdl_IP_PersonDetection( grayaddr, &g_pdct_rslt, raw_rgbaddr);
    }
    return result;
}

uint8_t Image::getPersonNumber(person_detect_area_t area){
    uint8_t ret;
    if(area == ALL){
        ret = 0;
        for(int i = 0; i < MAX_AREA; i++){
            ret += g_pdct_rslt.p_dct_cnt[i];
        }
    } else {
        ret = g_pdct_rslt.p_dct_cnt[area-1];
    }
    return ret;
}

bool Image::movingDetection(uint8_t* before_gray_image1, uint8_t* before_gray_image2){
    bool result;
    // 画像処理ミドルウェア初期化
    Mdl_IP_Init( detect_workaddr );
    if(!done_convert_gray){
        createGrayImage();
    }
    memset( &g_mdct_rslt, 0x0, sizeof( MovingDetection_Rslt ) );
    if(use_corrected_rgb565){
        result = (bool)Mdl_IP_MovingDetection(before_gray_image1, before_gray_image2, grayaddr, &g_mdct_rslt, corrected_rgbaddr);
    } else {
        result = (bool)Mdl_IP_MovingDetection(before_gray_image1, before_gray_image2, grayaddr, &g_mdct_rslt, raw_rgbaddr);
    }
    return result;
}

uint8_t Image::getMovingNumber(){
    return g_mdct_rslt.p_dct_cnt;
}

uint8_t Image::getMovingArea(uint8_t moving){
    return g_mdct_rslt.p_dct_inf[moving].area;
}
