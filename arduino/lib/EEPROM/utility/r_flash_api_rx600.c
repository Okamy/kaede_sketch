/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name	   : r_flash_api_rx600.c
* Version	   : 1.00
* Device 	   : RX63N
* Tool-Chain   : RX Family C Compiler
* H/W Platform : RX63N
* Description  : Flash programming for the RX63N Group
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 22.05.2012 1.00    First Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#ifndef GRSAKURA
/* Intrinsic functions of MCU */
#include <machine.h>

/* Type define */
#include "r_usbc_cTypedef.h"
#else
/* Intrinsic functions of MCU */
/* Type define */
#include "r_flash_api_rx600.h"
#endif

/* Get board and MCU definitions. */
#include "rx64m/iodefine.h"

/******************************************************************************
Macro definitions
******************************************************************************/
/* System clock speed in Hz. */
#define ICLK_HZ					(96000000)
/* Peripheral clock speed in Hz. */
#define PCLK_HZ					(48000000)
/* External bus clock speed in Hz. */
#define BCLK_HZ					(12000000)
/* FlashIF clock speed in Hz. */
#define FCLK_HZ					(48000000)

/*  Max Programming Time for 128 bytes (ROM) is 12ms. */
#define WAIT_MAX_ROM_WRITE      ((int32_t)(  12000 * (50.0 / (FCLK_HZ / 1000000))) * (ICLK_HZ / 1000000))
/*  Max Programming Time for 2 bytes (ROM) is 2ms. */
#define WAIT_MAX_DF_WRITE      ((int32_t)(  2000 * (50.0 / (FCLK_HZ / 1000000))) * (ICLK_HZ / 1000000))
/*  Max timeout value when using the peripheral clock notification command is 60us. */
#define	WAIT_MAX_FCU_CLOCK		((int32_t)(     60 * (50.0 / (FCLK_HZ / 1000000))) * (ICLK_HZ / 1000000))
/*  Max Erasure Time for a 64kB block is around 1152ms. */
#define WAIT_MAX_ERASE			((int32_t)(1152000 * (50.0 / (FCLK_HZ / 1000000))) * (ICLK_HZ / 1000000))
/*  The number of ICLK ticks needed for 35us delay are calculated below */
#define	WAIT_TRESW				(35 * (ICLK_HZ / 1000000))
/*  The number of ICLK ticks needed for 10us delay are calculated below */
#define	WAIT_T10USEC			(10 * (ICLK_HZ / 1000000))
/* The number of loops to wait for FENTRYR timeout. */
#define FLASH_FENTRYR_TIMEOUT   (4)


/* Memory specifics for the RX610 group */
/* Defines the start program/erase address for the different flash areas */
#define	ROM_AREA_OVER			(0x01000000)
#define ROM_AREA_0				(0x00F80000)
#define ROM_AREA_1				(0x00F00000)
#define ROM_AREA_2				(0x00E80000)
#define ROM_AREA_3				(0x00E00000)

/* Programming size for ROM in bytes */
#define ROM_PROGRAM_SIZE		128


/******************************************************************************
Typedef definitions
******************************************************************************/
/* These typedefs are used for guaranteeing correct accesses to memory. When 
   working with the FCU sometimes byte or word accesses are required. */
typedef volatile uint8_t  * FCU_BYTE_PTR;
typedef volatile uint16_t * FCU_WORD_PTR;
typedef volatile uint32_t * FCU_LONG_PTR;
/***********************************************************************************************************************
 Macro definitions
 ***********************************************************************************************************************/
#define FCU_COMMAND_AREA          (0x007E0000)
#define FCU_FIRMWARE_STORAGE_AREA (0xFEFFF000)
#define FCU_RAM_AREA              (0x007F8000)
#define FCU_RAM_SIZE              (4096)

#define USER_LOCKING_TYPE bsp_lock_t

/*FACI Commands*/
#define FLASH_FACI_CMD_PROGRAM              0xE8
#define FLASH_FACI_CMD_PROGRAM_CF           0x80
#define FLASH_FACI_CMD_PROGRAM_DF           0x02
#define FLASH_FACI_CMD_BLOCK_ERASE          0x20
#define FLASH_FACI_CMD_PE_SUSPEND           0xB0
#define FLASH_FACI_CMD_PE_RESUME            0xD0
#define FLASH_FACI_CMD_STATUS_CLEAR         0x50
#define FLASH_FACI_CMD_FORCED_STOP          0xB3
#define FLASH_FACI_CMD_BLANK_CHECK          0x71
#define FLASH_FACI_CMD_CONFIG_SET_1         0x40
#define FLASH_FACI_CMD_CONFIG_SET_2         0x08
#define FLASH_FACI_CMD_LOCK_BIT_PGM         0x77
#define FLASH_FACI_CMD_LOCK_BIT_READ        0x71
#define FLASH_FACI_CMD_FINAL                0xD0


/* The number of loops to wait for FENTRYR timeout. */
#define FLASH_FENTRYR_TIMEOUT   (4)

/* The maximum timeout for commands is 100usec when FCLK is 16 MHz i.e. 1600 FCLK cycles.
 * Assuming worst case of ICLK at 120 MHz and FCLK at 4 MHz, and optimization set to max such that
 * each count decrement loop takes only 5 cycles, then ((120/4)*1600)/5 = 9600 */
#define FLASH_FRDY_CMD_TIMEOUT          (9600)

/*Time that it would take for the Data Buffer to be empty (DBFULL Flag) is 90 FCLK cycles.
 * Assuming worst case of ICLK at 120 MHz and FCLK at 4 MHz, and optimization set to max such that
 * each count decrement loop takes only 5 cycles, then ((120/4)*90)/5 = 540 */
#define FLASH_DBFULL_TIMEOUT (540)

#define FLASH_MIN_PGM_SIZE_DF (4)

/******************************************************************************
Exported global variables 
******************************************************************************/

/******************************************************************************
Private global variables and functions
******************************************************************************/
FCU_BYTE_PTR g_pfcu_cmd_area = (uint8_t*) FCU_COMMAND_AREA;

/* Enter PE mode function prototype */
static uint8_t      dflash_enter_pe_mode();
/* Exit PE mode function prototype */
static void			flash_exit_pe_mode();
static flash_err_t flash_fcuram_codecopy();



/******************************************************************************
* Function Name	:	flash_Initialize
* Description	:	Initializes the FCU peripheral block.
*					NOTE:This function does not have to execute from in RAM.
* Arguments		:	none
* Return Value	:	FLASH_SUCCESS	= Operation Successful
*					FLASH_FAILURE	= Operation Failed
******************************************************************************/
uint8_t flash_Initialize( void )
{
    /*Allow Access to the Flash registers*/
    FLASH.FWEPROR.BYTE = 0x01;
    /*Set the Clock*/
    FLASH.FPCKAR.WORD = ((0x1E00) + ((FCLK_HZ / 1000000)));

#if 1
    FLASH.FAEINT.BYTE = 0x00;
    FLASH.FRDYIE.BYTE = 0x00;
    /*Clear any pending Flash Ready Interrupt Request*/
    IR(FCU,FRDYI) = 0;
    /*Clear any pending Flash Error Interrupt Request*/
    IR(FCU,FIFERR) = 0;
#endif
    /*Copy the FCU firmware to FCU RAM*/
	return flash_fcuram_codecopy();
}
/******************************************************************************
End of function  flash_Initialize
******************************************************************************/


#ifndef GRSAKURA
#pragma section FRAM
#else
#endif //GRSAKURA

/******************************************************************************
* Function Name	:	flash_coderom_EraseBlock
* Description	:	Erases an entire flash block.
* Arguments		:	fcubpOpe		= Operation address to erase
* Return Value	:	FLASH_SUCCESS	= Operation Successful
*					FLASH_FAILURE	= Operation Failed
******************************************************************************/
uint8_t flash_coderom_EraseBlock( const uint32_t addr )
{
    // no implement
    return false;
}
/******************************************************************************
End of function  flash_coderom_EraseBlock
******************************************************************************/
/******************************************************************************
* Function Name :   flash_datarom_EraseBlock
* Description   :   Erases an entire flash block.
* Arguments     :   fcubpOpe        = Operation address to erase
* Return Value  :   FLASH_SUCCESS   = Operation Successful
*                   FLASH_FAILURE   = Operation Failed
******************************************************************************/
uint8_t flash_datarom_EraseBlock( const uint32_t addr )
{
    uint8_t result;

#ifndef GRSAKURA
    const uint32_t  pswSaved = get_psw();
    set_psw( pswSaved & ~0x10000 ); // Disable interrupt
#else
    bool di = isNoInterrupts();
    noInterrupts();
#endif //GRSAKURA

    /* Enter PE mode, check if operation is successful */
    result = dflash_enter_pe_mode();
    if( result == FLASH_SUCCESS ){
        /*Set Erasure Priority Mode*/
        FLASH.FCPSR.WORD = 0x0001;

        /*Set block start address*/
        FLASH.FSADDR.LONG = addr;

        /*Issue two part Block Erase commands*/
        *g_pfcu_cmd_area = (uint8_t) FLASH_FACI_CMD_BLOCK_ERASE;
        *g_pfcu_cmd_area = (uint8_t) FLASH_FACI_CMD_FINAL;

        /* Read FRDY bit until it has been set to 1 indicating that the current
         * operation is complete.*/
        while (1 != FLASH.FSTATR.BIT.FRDY);

        /*Check if there were any errors
         * Check if Command Lock bit is set*/
        if (0 != FLASH.FASTAT.BIT.CMDLK)
        {
            return FLASH_ERR_FAILURE;
        }

    } else {
        return FLASH_ERR_FAILURE;
    }

    /* Leave Program/Erase Mode */
    flash_exit_pe_mode();

#ifndef GRSAKURA
    set_psw( pswSaved );
#else
    if (!di) {
      interrupts();
    }
#endif
    return FLASH_SUCCESS;
}
/******************************************************************************
End of function  flash_datarom_EraseBlock
******************************************************************************/

/******************************************************************************
* Function Name	:	flash_coderom_WriteData
* Description	:	Program data into code flash.
* Arguments		:	fcubpOpe		= Operation address to programing.
*					pData			= Data buffer to write.
*					nSize			= Data size to write.
* Return Value	:	FLASH_SUCCESS	= Operation Successful
*					FLASH_FAILURE	= Operation Failed
******************************************************************************/
uint8_t flash_coderom_WriteData( const uint32_t addr, void* pData, const uint16_t nDataSize )
{
    //no implement
	return FLASH_ERR_FAILURE;
}
/******************************************************************************
End of function  flash_coderom_WriteData
******************************************************************************/

/******************************************************************************
* Function Name :   flash_datarom_WriteData
* Description   :   Program data into data flash.
* Arguments     :   fcubpOpe        = Operation address to programming.(4bytes align)
*                   data           = Data buffer to write.
*                   size           = Data size to write.
* Return Value  :   FLASH_SUCCESS   = Operation Successful
*                   FLASH_FAILURE   = Operation Failed
******************************************************************************/
uint8_t flash_datarom_WriteData( const uint32_t addr, void* pData, const uint16_t nDataSize )
{
    uint8_t         result;

#ifndef GRSAKURA
    const uint32_t  pswSaved = get_psw();
    set_psw( pswSaved & ~0x10000 ); // Disable interrupt
#else
    bool di = isNoInterrupts();
    noInterrupts();
#endif //GRSAKURA

    /* Enter PE mode, check if operation is successful */
    result = dflash_enter_pe_mode();
    if( result == FLASH_SUCCESS )
    {
        uint32_t src_addr = (uint32_t)pData;  // Source address for operation in progress
        uint32_t dest_addr = addr;  // Destination address for operation in progress
        uint32_t total_count = nDataSize >> 1;  // total number of bytes to write/erase
        uint32_t current_count = 0;     // bytes of total completed

        /* Iterate through the number of data bytes */
        while (total_count > 0)
        {
          /*Set block start address*/
          FLASH.FSADDR.LONG = dest_addr;
          /*Issue two part Write commands*/
          *g_pfcu_cmd_area = (uint8_t) FLASH_FACI_CMD_PROGRAM;
          *g_pfcu_cmd_area = (uint8_t) 0x2;
          /*Write one line (4 bytes for DF, 256 bytes for CF)*/
          while (current_count++ < 2)
          {
            /* Copy data from source address to destination area */
            *(FCU_WORD_PTR) g_pfcu_cmd_area = *(uint16_t *) src_addr;

            while (FLASH.FSTATR.BIT.DBFULL == 1);
            src_addr += 2;
            dest_addr += 2;
            total_count--;
          }
          /*Reset line count*/
          current_count = 0;
          /*Issue write end command*/
          *g_pfcu_cmd_area = (uint8_t) FLASH_FACI_CMD_FINAL;
          /* Read FRDY bit until it has been set to 1 indicating that the current
           * operation is complete.*/
          while (1 != FLASH.FSTATR.BIT.FRDY);

          /*Check if there were any errors
           * Check if Command Lock bit is set*/
          if (0 != FLASH.FASTAT.BIT.CMDLK)
          {
              PORTC.PODR.BIT.B1 = 1;
              /* Leave Program/Erase Mode */
              flash_exit_pe_mode();
              if (!di) {
                interrupts();
              }
              return FLASH_ERR_FAILURE;
          }
        }

    }

    /* Leave Program/Erase Mode */
    flash_exit_pe_mode();

#ifndef GRSAKURA
    set_psw( pswSaved );
#else
    if (!di) {
      interrupts();
    }

#endif
    return FLASH_SUCCESS;

}
/******************************************************************************
End of function  flash_datarom_WriteData
******************************************************************************/
/******************************************************************************
* Function Name :   flash_datarom_blankcheck
* Description   :   Blank check data flash.
* Arguments     :   addr        = Operation address to check.
* Return Value  :   0   = blank
*                   1   = not blank
******************************************************************************/
bool flash_datarom_blankcheck( const uint32_t addr )
{
    uint8_t         result;

#ifndef GRSAKURA
    const uint32_t  pswSaved = get_psw();
    set_psw( pswSaved & ~0x10000 ); // Disable interrupt
#else
    bool di = isNoInterrupts();
    noInterrupts();
#endif //GRSAKURA

    /* Enter PE mode, check if operation is successful */
    result = dflash_enter_pe_mode();
    if( result == FLASH_SUCCESS )
    {
        /*Set the mode to incremental*/
        FLASH.FBCCNT.BYTE = 0x00;
        /*Set the start address for blank checking*/
        FLASH.FSADDR.LONG = addr;
        /*Set the end address for blank checking*/
        FLASH.FEADDR.LONG = addr; // 1 bytes
        /*Issue two part Blank Check command*/
        *g_pfcu_cmd_area = (uint8_t) FLASH_FACI_CMD_BLANK_CHECK;
        *g_pfcu_cmd_area = (uint8_t) FLASH_FACI_CMD_FINAL;

        /* Read FRDY bit until it has been set to 1 indicating that the current
         * operation is complete.*/
        while (1 != FLASH.FSTATR.BIT.FRDY);

        if (FLASH.FBCSTAT.BYTE == 0x01) {
          result = 1;
        } else {
          result = 0;
        }

    }


    /* Leave Program/Erase Mode */
    flash_exit_pe_mode();

#ifndef GRSAKURA
    set_psw( pswSaved );
#else
    if (!di) {
      interrupts();
    }
#endif

    /* Return FLASH_SUCCESS, operation success */
    return (bool)result;
}
/******************************************************************************
End of function  flash_datarom_WriteData
******************************************************************************/


/******************************************************************************
* Function Name :   dflash_enter_pe_mode
* Description   :   Puts the FCU into program/erase mode.
* Arguments     :   fcubpOpe        = Operation address to program/erase
* Return Value  :   FLASH_SUCCESS   = Operation Successful
*                   FLASH_FAILURE   = Operation Failed
******************************************************************************/
static uint8_t dflash_enter_pe_mode()
{
    FLASH.FENTRYR.WORD = 0xAA80;  //Transition to DF P/E mode
    if (FLASH.FENTRYR.WORD == 0x0080)
    {
        /* Return FLASH_SUCCESS, operation successful */
        return FLASH_SUCCESS;
    }

    return FLASH_ERR_FAILURE;
}
/******************************************************************************
End of function flash_enter_pe_mode
******************************************************************************/

/******************************************************************************
* Function Name	:	flash_exit_pe_mode
* Description	:	Takes the FCU out of program/erase mode.
* Return Value  :   FLASH_SUCCESS   = Operation Successful
*                   FLASH_FAILURE   = Operation Failed
******************************************************************************/
static void flash_exit_pe_mode()
{
    /* Read FRDY bit until it has been set to 1 indicating that the current
     * operation is complete.*/
    while (1 != FLASH.FSTATR.BIT.FRDY);
    /*Transition to Read mode*/
    FLASH.FENTRYR.WORD = 0xAA00;


}

/***********************************************************************************************************************
 * Function Name: flash_stop
 * Description  : Function issue the STOP command and check the state of the Command Lock bit.
 * Arguments    :
 * Return Value : FLASH_SUCCESS -
 *                Stop issued successfully.
 *                FLASH_ERR_TIMEOUT
 *                Timed out
 *                FLASH_ERR_LOCKED
 *                Peripheral in locked state
 ***********************************************************************************************************************/
flash_err_t flash_stop()
{
  /* Timeout counter. */
  volatile uint32_t wait_cnt = FLASH_FRDY_CMD_TIMEOUT;

  /*Issue stop command to flash command area*/
  *g_pfcu_cmd_area = (uint8_t) FLASH_FACI_CMD_FORCED_STOP;

  /* Read FRDY bit until it has been set to 1 indicating that the current
   * operation is complete.*/
  while (1 != FLASH.FSTATR.BIT.FRDY)
  {
    /* Wait until FRDY is 1 unless timeout occurs. */
    if (wait_cnt-- <= 0)
    {
      /* This should not happen normally.
       * FRDY should get set in 15-20 ICLK cycles on STOP command*/
      return FLASH_ERR_TIMEOUT;
    }
  }
  /*Check that Command Lock bit is cleared*/
  if (0 != FLASH.FASTAT.BIT.CMDLK)
  {
    return FLASH_ERR_CMD_LOCKED;
  }

  return FLASH_SUCCESS;
}
/***********************************************************************************************************************
 * Function Name: flash_fcuram_codecopy
 * Description  : Function will copy FCU firmware to the FCURAM.
 * Arguments    :
 * Return Value : FLASH_SUCCESS -
 *                    Code copied successfully.
 *                FLASH_ERR_TIMEOUT
 *                  Timed out while attempting to switch to P/E mode or while trying to issue a STOP or
 *                   an ongoing flash operation timed out.
 *                FLASH_ERR_LOCKED
 *                 Switch to Read mode timed out and STOP was attempted to recover. Stop operation failed.
 *                 Peripheral in locked state.
 *                FLASH_ERR_FAILURE
 *                 Unable to enter PE mode
 ***********************************************************************************************************************/

flash_err_t flash_fcuram_codecopy()
{
  flash_err_t err = FLASH_SUCCESS;
  uint16_t copy_cnt = 0;     // Code Copy Counter
  uint32_t * p_src;
  uint32_t * p_dest;
  volatile int32_t wait_cnt;      // Timeout counter.

  p_src = (uint32_t*) FCU_FIRMWARE_STORAGE_AREA;
  p_dest = (uint32_t*) FCU_RAM_AREA;

  if (FLASH.FENTRYR.WORD != 0x0000)
  {
    /*Enter Read Mode*/
    FLASH.FENTRYR.WORD = 0xAA00;
    /* Initialize timeout for FENTRYR being written. */
    wait_cnt = FLASH_FENTRYR_TIMEOUT;

    /* Read FENTRYR to ensure it has been set to 0. Note that the top byte
     of the FENTRYR register is not retained and is read as 0x00. */
    while (0x0000 != FLASH.FENTRYR.WORD) {
      /* Wait until FENTRYR is 0 unless timeout occurs. */
      if (wait_cnt-- <= 0)
      {
        /* This should not happen when copying code over to FCURAM.
         * FENTRYR getting written to 0 should  only take 2-4 PCLK cycles.  */
        return FLASH_ERR_TIMEOUT;
      }
    }
  }

  FLASH.FCURAME.WORD = 0xC411;   // 11: HighSpeed Write Only mode to FCURAM

  /* Copy over the 4K from storage area to FCURAM */
  for (copy_cnt = 0; copy_cnt < (FCU_RAM_SIZE / 4); copy_cnt++)
  {
    *p_dest = *p_src;    // 32 bit copy
    p_dest++;
    p_src++;
  }

  FLASH.FCURAME.WORD = 0xC400;   // 00: Disable Write to FCURAM

  /*Transition to DF P/E mode to issue STOP command.
   * CF P/E mode can also be used instead but DF P/E mode is
   * used so that there is no need to place this function in RAM
   * if CF non-BGO mode is being used*/
  if (FLASH_SUCCESS != dflash_enter_pe_mode())
  {
    return FLASH_ERR_FAILURE;
  }

  /*Issue a forced STOP command */
  err = flash_stop();
  if (err != FLASH_SUCCESS)
  {
    return err;
  }

  /*Transition to read mode*/
  flash_exit_pe_mode();

  return FLASH_SUCCESS;
}

/******************************************************************************
End of function  flash_exit_pe_mode
******************************************************************************/


