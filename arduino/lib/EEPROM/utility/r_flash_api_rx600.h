/*
 * r_flash_api_rx600.h
 *
 *  Created on: Apr 17, 2015
 *      Author: a5034000
 */

#ifndef R_FLASH_API_RX600_H_
#define R_FLASH_API_RX600_H_

#include "Arduino.h"

/**** Function Return Values ****/
/* Operation was successful */
#define DF_ADDRESS              0x00100000
/* Used for getting DF block */
#define DF_MASK                 0xFFFFF800
/* Used for getting DF block */
#define DF_BLOCK_MASK           0xFFFFFFC0
/* Used for getting erase boundary in DF block when doing blank checking */
#define DF_ERASE_BLOCK_SIZE     0x00000040
/* Used for programming/blank check DF align */
#define DF_ALIGN                (4)

/* Flash API error codes */
typedef enum _flash_err
{
    FLASH_SUCCESS = 0,
    FLASH_ERR_BUSY,             // Peripheral Busy
    FLASH_ERR_ACCESSW,          // Access window error
    FLASH_ERR_FAILURE,          // Operation failure, Programming or erasing error due to something other than lock bit
    FLASH_ERR_CMD_LOCKED,       // RX64M - Peripheral in command locked state
    FLASH_ERR_LOCKBIT_SET,      // RX64M - Pgm/Erase error due to lock bit.
    FLASH_ERR_FREQUENCY,        // RX64M - Illegal Frequency value attempted (4-60Mhz)
    FLASH_ERR_ALIGNED,          // RX600/RX200 - The address that was supplied was not on aligned correctly for ROM or DF
    FLASH_ERR_BOUNDARY,         // RX600/RX200 - Writes cannot cross the 1MB boundary on some parts
    FLASH_ERR_OVERFLOW,         // RX600/RX200 - Address + number of bytes' for this operation went past the end of this memory area.
    FLASH_ERR_BYTES,            // Invalid number of bytes passed
    FLASH_ERR_ADDRESS,          // Invalid address or address not on a programming boundary
    FLASH_ERR_BLOCKS,           // The "number of blocks" argument is invalid
    FLASH_ERR_PARAM,            // Illegal parameter
    FLASH_ERR_NULL_PTR,         // received null ptr; missing required argument
    FLASH_ERR_TIMEOUT,          // Timeout Condition
} flash_err_t;

#ifdef __cplusplus
extern "C" {
#endif

uint8_t flash_Initialize( void );
uint8_t flash_coderom_EraseBlock( const uint32_t addr );
uint8_t flash_datarom_EraseBlock( const uint32_t addr );
uint8_t flash_coderom_WriteData( const uint32_t addr, void* pData, const uint16_t nDataSize );
uint8_t flash_datarom_WriteData( const uint32_t addr, void* pData, const uint16_t nDataSize );
bool flash_datarom_blankcheck( const uint32_t addr );


#ifdef __cplusplus
}
#endif


#endif /* R_FLASH_API_RX600_H_ */
